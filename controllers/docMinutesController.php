<?php
	require_once("../Classes/DocMinutes.php"); 
	session_start();
	class DocMinutesController{
		function __construct(){
			// print_r($_POST);
			// exit();
			switch (trim($_POST["mode"])) {
				// for insert
				case 'insert':
					if (!empty($_POST["documentReceivedId"]) || !empty($_POST["addMinuteDetail"]) || !empty($_POST["addMinuteLock_log"])) {
						try {
							$objDocMinutes = new DocMinutes;
							$objDocMinutes->set_documentReceivedId($objDocMinutes->CleanData($_POST["documentReceivedId"]));
							$objDocMinutes->set_addMinuteLock($objDocMinutes->CleanData($_POST["addMinuteLock_log"]));
							$objDocMinutes->set_addMinuteDetail($objDocMinutes->CleanData($_POST["addMinuteDetail"]));
							if ($objDocMinutes->insert()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
					
				break;
			// for update
				case 'update':
					if (!empty($_POST["data_id"]) || !empty($_POST["documentReceivedId"]) || !empty($_POST["addMinuteDetail"]) || !empty($_POST["addMinuteLock_log"])) {
						try {
							$objDocMinutes = new DocMinutes;
							$objDocMinutes->set_documentReceivedId($objDocMinutes->CleanData($_POST["documentReceivedId"]));
							$objDocMinutes->set_addMinuteLock($objDocMinutes->CleanData($_POST["addMinuteLock_log"]));
							$objDocMinutes->set_addMinuteDetail($objDocMinutes->CleanData($_POST["addMinuteDetail"]));
							$objDocMinutes->set_id($objDocMinutes->CleanData($_POST["data_id"]));
							if ($objDocMinutes->update()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
				break;
			// for delete
				case 'delete':
					if(!empty($_POST["data_id"])){
						try {
						  $objDocMinutes = new DocMinutes;
						  $objDocMinutes->set_recordHide("YES");
					      $objDocMinutes->set_id($objDocMinutes->CleanData($_POST["data_id"]));
					      if ($objDocMinutes->delete()) {
					      	echo "success";
					      }
					      else{
					      	echo "error";
					      }
					    } catch (PDOException $e){echo $e;}
					 }else{
					 	echo "error";
					}
				break;
				// geting details of a member with id
				case 'updateModal':
					try {
						if(!empty($_POST["data_id"])){
						  $objDocMinutes = new DocMinutes;  
					      $objDocMinutes->set_id($objDocMinutes->CleanData($_POST["data_id"]));
					      $details = $objDocMinutes->get_doc_minute_by_id();
					      print_r($details);  
						 }else{
						 	echo "error";
						 }
					} catch (PDOException $e){echo $e;}
				break;
				// get all
				case 'getAll':
					if (!empty($_POST["documentReceivedId"])) {
						$objDocMinutes = new DocMinutes;
						$objDocMinutes->set_documentReceivedId($objDocMinutes->CleanData($_POST["documentReceivedId"]));
						print_r($objDocMinutes->get_document_minutes_list());
					}
					else{
						echo "error";
					}
					

				break;

				default:
					echo "error";
				break;
			}

		}
	}

	$objDocMinutesController = new DocMinutesController;
 ?>