<?php
	require_once("../Classes/Company.php"); 
	session_start();
	class CompanyControlle{
		function __construct(){
			// print_r($_POST);
			// exit();
			switch (trim($_POST["mode"])) {
				// for insert
				case 'insert':
					if (!empty($_POST["companyName"]) || !empty($_POST["companyRegion"]) || !empty($_POST["companyCountry"]) ) {
						try {
							$objCompany = new Company;
							$objCompany->set_companyName($objCompany->CleanData($_POST["companyName"]));
							$objCompany->set_companyTelNo($objCompany->CleanData($_POST["companyTelNo"]));
							$objCompany->set_companyEmail($objCompany->CleanData($_POST["companyEmail"]));
							$objCompany->set_companyAddress($_POST["companyAddress"]);
							$objCompany->set_companyRegion($objCompany->CleanData($_POST["companyRegion"]));
							$objCompany->set_companyCountry($objCompany->CleanData($_POST["companyCountry"]));
							$objCompany->set_companyNotes($_POST["companyNotes"]);

							if ($objCompany->insert()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
					
				break;
			// for update
				case 'update':
					if (!empty($_POST["companyName"]) || !empty($_POST["companyRegion"]) || !empty($_POST["companyCountry"]) ) {
						try {
							$objCompany = new Company;
							$objCompany->set_companyName($objCompany->CleanData($_POST["companyName"]));
							$objCompany->set_companyTelNo($objCompany->CleanData($_POST["companyTelNo"]));
							$objCompany->set_companyEmail($objCompany->CleanData($_POST["companyEmail"]));
							$objCompany->set_companyAddress($_POST["companyAddress"]);
							$objCompany->set_companyRegion($objCompany->CleanData($_POST["companyRegion"]));
							$objCompany->set_companyCountry($objCompany->CleanData($_POST["companyCountry"]));
							$objCompany->set_companyNotes($_POST["companyNotes"]);
							$objCompany->set_id($objCompany->CleanData($_POST["data_id"]));
							if ($objCompany->update()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
				break;
			// for delete
				case 'delete':
					if(!empty($_POST["data_id"])){
						try {
						  $objCompany = new Company;
						  $objCompany->set_recordHide("YES");
					      $objCompany->set_id($objCompany->CleanData($_POST["data_id"]));
					      if ($objCompany->delete()) {
					      	echo "success";
					      }
					      else{
					      	echo "error";
					      }
					    } catch (PDOException $e){echo $e;}
					 }else{
					 	echo "error";
					}
				break;
				// geting details of a member with id
				case 'updateModal':
					try {
						if(!empty($_POST["data_id"])){
						  $objCompany = new Company;  
					      $objCompany->set_id($objCompany->CleanData($_POST["data_id"]));
					      $details = $objCompany->get_company_by_id();
					      print_r($details);  
						 }else{
						 	echo "error";
						 }
					} catch (PDOException $e){echo $e;}
				break;
				// get all
				case 'getAll':
					$objCompany = new Company;
					print_r(json_encode($objCompany->get_companys(),true));

				break;
				
				default:
					die();
					break;
			}

		}
	}

	$objCompanyControlle = new CompanyControlle;
 ?>