<?php
	require_once("../Classes/Members.php"); 
	session_start();
	class membersController{
		function __construct(){
			
				$returnData='';
					$folderPath = "../uploads/members_images/";
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							if (empty($_POST["memFullName"]) || empty($_POST["memGender"]) || empty($_POST["memMaritalStatus"]) || empty($_POST["kinFullname"]) || empty($_POST["memPhoneNum1"]) || empty($_POST["memStatus"]) || empty($_POST["memLiceNnum"])) {

								echo "error";
								exit();
							}
							else{
								// ///////////////////////NO IMAGE ADDED////////////////////////////////////
									if ($_FILES['file']['size'] == 0) {
										// print_r($_POST);
										$memberPic = "NONE";
										try {
											$objMembers = new Members;
											$objMembers->set_memFullName($objMembers->CleanData($_POST["memFullName"]));
											$objMembers->set_memDob($objMembers->CleanData($_POST["memDob"]));
											$objMembers->set_memHomeTown($objMembers->CleanData($_POST["memHomeTown"]));
											$objMembers->set_memCardNumber($objMembers->CleanData($_POST["memCardNumber"]));
											$objMembers->set_memGhisDipNo($objMembers->CleanData($_POST["memGhisDipNo"]));
											$objMembers->set_kinFullname($objMembers->CleanData($_POST["kinFullname"]));
											$objMembers->set_kinPhonenum($objMembers->CleanData($_POST["kinPhonenum"]));
											$objMembers->set_memLiceNnum($objMembers->CleanData($_POST["memLiceNnum"]));
											$objMembers->set_memInductionYear($objMembers->CleanData($_POST["memInductionYear"]));
											$objMembers->set_memPhoneNum1($objMembers->CleanData($_POST["memPhoneNum1"]));
											$objMembers->set_memPhoneNum2($objMembers->CleanData($_POST["memPhoneNum2"]));
											$objMembers->set_mememail($objMembers->CleanData($_POST["mememail"]));
											$objMembers->set_memecity($objMembers->CleanData($_POST["memecity"]));
											$objMembers->set_memBussinessAddress($objMembers->CleanData($_POST["memBussinessAddress"]));
											$objMembers->set_memResidentAddress($objMembers->CleanData($_POST["memResidentAddress"]));
											$objMembers->set_memStatus($objMembers->CleanData($_POST["memStatus"]));
											$objMembers->set_memBankName($objMembers->CleanData($_POST["memBankName"]));
											$objMembers->set_memBankAccname($objMembers->CleanData($_POST["memBankAccname"]));
											$objMembers->set_memBankAccnum($objMembers->CleanData($_POST["memBankAccnum"]));
											$objMembers->set_memBankBranch($objMembers->CleanData($_POST["memBankBranch"]));
											$objMembers->set_memGender($objMembers->CleanData($_POST["memGender"]));
											$objMembers->set_memMaritalStatus($objMembers->CleanData($_POST["memMaritalStatus"]));
											$objMembers->set_memberType($objMembers->CleanData($_POST["memType_log"]));
											$objMembers->set_memberPic($memberPic);
											// $printReturn = $objMembers->insert();
											// print_r($printReturn);
											if ($objMembers->insert()) {
												echo "success";
											}
											else{
												echo "error";
											}

										} catch (PDOException $e){echo "error";}

									}
									// //////////////////////IMAGE ADDED//////////////////////////////////////
									else if ($_FILES['file']['size'] != 0) {
										try {
											// declaring a new variable forthe gloabl 
											$files=$_FILES['file'];
											$allowed=array('png','jpeg','jpg');

											$filename = $_FILES['file']['name'];
											$file_tmp =$_FILES['file']['tmp_name'];
											$file_size =$_FILES['file']['size'];
											$file_error = $_FILES['file']['error'];
											$file_ext=explode('.',$filename);
											$filename= current($file_ext);
											$file_ext = strtolower(end($file_ext));

											if (!in_array($file_ext,$allowed)) {
												// if file being uploaded is not in array then prompt
												echo "Unsupported file";
											}
											else if (in_array($file_ext,$allowed)) {
												// check if ther is any errors from the uploaded file
												if ($file_error === 0) {
													// check if file uploaded has ther righ size
													if ($file_size < 100000000000000000) {
														// creating unique ids for uploads instead of names of files, number is in microsecond
														$filenewname=$filename.".".$file_ext;			
														$filedestination = $folderPath.$filenewname;
														// moving file from temporal location to permanent storage
														$fileup=move_uploaded_file($file_tmp,$filedestination);
													}
														// end of if to check file size
												}
														// end of file erroe
														else{
															echo "error";
															die();
														}
											}
											// end of if file is allowed

											// if file is uploaded successfully then save details in database
											if ($fileup) {
												$objMembers = new Members;
												$objMembers->set_memFullName($objMembers->CleanData($_POST["memFullName"]));
												$objMembers->set_memDob($objMembers->CleanData($_POST["memDob"]));
												$objMembers->set_memHomeTown($objMembers->CleanData($_POST["memHomeTown"]));
												$objMembers->set_memCardNumber($objMembers->CleanData($_POST["memCardNumber"]));
												$objMembers->set_memGhisDipNo($objMembers->CleanData($_POST["memGhisDipNo"]));
												$objMembers->set_kinFullname($objMembers->CleanData($_POST["kinFullname"]));
												$objMembers->set_kinPhonenum($objMembers->CleanData($_POST["kinPhonenum"]));
												$objMembers->set_memLiceNnum($objMembers->CleanData($_POST["memLiceNnum"]));
												$objMembers->set_memInductionYear($objMembers->CleanData($_POST["memInductionYear"]));
												$objMembers->set_memPhoneNum1($objMembers->CleanData($_POST["memPhoneNum1"]));
												$objMembers->set_memPhoneNum2($objMembers->CleanData($_POST["memPhoneNum2"]));
												$objMembers->set_mememail($objMembers->CleanData($_POST["mememail"]));
												$objMembers->set_memecity($objMembers->CleanData($_POST["memecity"]));
												$objMembers->set_memBussinessAddress($objMembers->CleanData($_POST["memBussinessAddress"]));
												$objMembers->set_memResidentAddress($objMembers->CleanData($_POST["memResidentAddress"]));
												$objMembers->set_memStatus($objMembers->CleanData($_POST["memStatus"]));
												$objMembers->set_memBankName($objMembers->CleanData($_POST["memBankName"]));
												$objMembers->set_memBankAccname($objMembers->CleanData($_POST["memBankAccname"]));
												$objMembers->set_memBankAccnum($objMembers->CleanData($_POST["memBankAccnum"]));
												$objMembers->set_memBankBranch($objMembers->CleanData($_POST["memBankBranch"]));
												$objMembers->set_memGender($objMembers->CleanData($_POST["memGender"]));
												$objMembers->set_memMaritalStatus($objMembers->CleanData($_POST["memMaritalStatus"]));
												$objMembers->set_memberType($objMembers->CleanData($_POST["memType_log"]));
												$objMembers->set_memberPic($filenewname);
											
												if ($objMembers->insert()) {
													echo "success";
												}
												else{
													echo "error";
												}
											}
											else{
												echo "error";
											}

										} catch (PDOException $e){echo "error";}
									}
							}
									
						break;
					// for update
						case 'update':
							if (empty($_POST["memFullName"]) || empty($_POST["memGender"]) || empty($_POST["memMaritalStatus"]) || empty($_POST["kinFullname"]) || empty($_POST["memPhoneNum1"]) || empty($_POST["memStatus"]) || empty($_POST["memLiceNnum"]) || empty($_POST["data_id"])) {

								echo "error";
								exit();
							}
							else{
								// ///////////////////////NO IMAGE ADDED////////////////////////////////////
									$memberPic = trim($_POST["returnedImg"]);
									if ($_FILES['file']['size'] == 0) {
										try {
											$objMembers = new Members;
											$objMembers->set_memFullName($objMembers->CleanData($_POST["memFullName"]));
											$objMembers->set_memDob($objMembers->CleanData($_POST["memDob"]));
											$objMembers->set_memHomeTown($objMembers->CleanData($_POST["memHomeTown"]));
											$objMembers->set_memCardNumber($objMembers->CleanData($_POST["memCardNumber"]));
											$objMembers->set_memGhisDipNo($objMembers->CleanData($_POST["memGhisDipNo"]));
											$objMembers->set_kinFullname($objMembers->CleanData($_POST["kinFullname"]));
											$objMembers->set_kinPhonenum($objMembers->CleanData($_POST["kinPhonenum"]));
											$objMembers->set_memLiceNnum($objMembers->CleanData($_POST["memLiceNnum"]));
											$objMembers->set_memInductionYear($objMembers->CleanData($_POST["memInductionYear"]));
											$objMembers->set_memPhoneNum1($objMembers->CleanData($_POST["memPhoneNum1"]));
											$objMembers->set_memPhoneNum2($objMembers->CleanData($_POST["memPhoneNum2"]));
											$objMembers->set_mememail($objMembers->CleanData($_POST["mememail"]));
											$objMembers->set_memecity($objMembers->CleanData($_POST["memecity"]));
											$objMembers->set_memBussinessAddress($objMembers->CleanData($_POST["memBussinessAddress"]));
											$objMembers->set_memResidentAddress($objMembers->CleanData($_POST["memResidentAddress"]));
											$objMembers->set_memStatus($objMembers->CleanData($_POST["memStatus"]));
											$objMembers->set_memBankName($objMembers->CleanData($_POST["memBankName"]));
											$objMembers->set_memBankAccname($objMembers->CleanData($_POST["memBankAccname"]));
											$objMembers->set_memBankAccnum($objMembers->CleanData($_POST["memBankAccnum"]));
											$objMembers->set_memBankBranch($objMembers->CleanData($_POST["memBankBranch"]));
											$objMembers->set_memGender($objMembers->CleanData($_POST["memGender"]));
											$objMembers->set_memMaritalStatus($objMembers->CleanData($_POST["memMaritalStatus"]));
											$objMembers->set_memberPic($memberPic);
											$objMembers->set_memberType($objMembers->CleanData($_POST["memType_log"]));
											$objMembers->set_id($objMembers->CleanData($_POST["data_id"]));

											if ($objMembers->update()) {
												echo "success";
											}
											else{
												echo "error1";
											}

										} catch (PDOException $e){echo $e;}

									}
									// //////////////////////IMAGE ADDED//////////////////////////////////////
									else if ($_FILES['file']['size'] != 0) {
										
										try {
											// declaring a new variable forthe gloabl 
											$files=$_FILES['file'];
											$allowed=array('png','jpeg','jpg');

											$filename = $_FILES['file']['name'];
											$file_tmp =$_FILES['file']['tmp_name'];
											$file_size =$_FILES['file']['size'];
											$file_error = $_FILES['file']['error'];
											$file_ext=explode('.',$filename);
											$filename= current($file_ext);
											$file_ext = strtolower(end($file_ext));

											if (!in_array($file_ext,$allowed)) {
												// if file being uploaded is not in array then prompt
												echo "Unsupported file";
											}
											else if (in_array($file_ext,$allowed)) {
												// check if ther is any errors from the uploaded file
												if ($file_error === 0) {
													// check if file uploaded has ther righ size
													if ($file_size < 100000000000000000) {
														// creating unique ids for uploads instead of names of files, number is in microsecond
														$filenewname=$filename.".".$file_ext;			
														$filedestination = $folderPath.$filenewname;
														// //////////////////////Remove file before uploadin new ones
														if (($memberPic != "NONE" ) || (file_exists($folderPath.$memberPic))) {
															unlink($folderPath.$memberPic);
														}
														// ////////////////////////////////////////////////////

														// moving file from temporal location to permanent storage
														$fileup=move_uploaded_file($file_tmp,$filedestination);
													}
														// end of if to check file size
												}
														// end of file erroe
														else{
															echo "error";
															die();
														}
											}
											// end of if file is allowed

											// if file is uploaded successfully then save details in database
											if ($fileup) {
												$objMembers = new Members;
												$objMembers->set_memFullName($objMembers->CleanData($_POST["memFullName"]));
												$objMembers->set_memDob($objMembers->CleanData($_POST["memDob"]));
												$objMembers->set_memHomeTown($objMembers->CleanData($_POST["memHomeTown"]));
												$objMembers->set_memCardNumber($objMembers->CleanData($_POST["memCardNumber"]));
												$objMembers->set_memGhisDipNo($objMembers->CleanData($_POST["memGhisDipNo"]));
												$objMembers->set_kinFullname($objMembers->CleanData($_POST["kinFullname"]));
												$objMembers->set_kinPhonenum($objMembers->CleanData($_POST["kinPhonenum"]));
												$objMembers->set_memLiceNnum($objMembers->CleanData($_POST["memLiceNnum"]));
												$objMembers->set_memInductionYear($objMembers->CleanData($_POST["memInductionYear"]));
												$objMembers->set_memPhoneNum1($objMembers->CleanData($_POST["memPhoneNum1"]));
												$objMembers->set_memPhoneNum2($objMembers->CleanData($_POST["memPhoneNum2"]));
												$objMembers->set_mememail($objMembers->CleanData($_POST["mememail"]));
												$objMembers->set_memecity($objMembers->CleanData($_POST["memecity"]));
												$objMembers->set_memBussinessAddress($objMembers->CleanData($_POST["memBussinessAddress"]));
												$objMembers->set_memResidentAddress($objMembers->CleanData($_POST["memResidentAddress"]));
												$objMembers->set_memStatus($objMembers->CleanData($_POST["memStatus"]));
												$objMembers->set_memBankName($objMembers->CleanData($_POST["memBankName"]));
												$objMembers->set_memBankAccname($objMembers->CleanData($_POST["memBankAccname"]));
												$objMembers->set_memBankAccnum($objMembers->CleanData($_POST["memBankAccnum"]));
												$objMembers->set_memBankBranch($objMembers->CleanData($_POST["memBankBranch"]));
												$objMembers->set_memGender($objMembers->CleanData($_POST["memGender"]));
												$objMembers->set_memMaritalStatus($objMembers->CleanData($_POST["memMaritalStatus"]));
												$objMembers->set_memberPic($filenewname);
												$objMembers->set_memberType($objMembers->CleanData($_POST["memType_log"]));

												$objMembers->set_id($objMembers->CleanData($_POST["data_id"]));
												if ($objMembers->update()) {
													echo "success";
												}
												else{
													echo "error";
												}
											}
											else{
												echo "error";
											}

										} catch (PDOException $e){echo "error";}
									}
							}
						break;
					// for delete
						case 'delete':
							if(!empty($_POST["data_id"])){
									 $objMembers = new Members;
									  $objMembers->set_recordHide("YES");
								      $objMembers->set_id($objMembers->CleanData($_POST["data_id"]));
								      if ($objMembers->delete()) {
								      	echo "success";
								      }
								      else{
								      	echo "error";
								      }
								     
								 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
								try {
									 $objMembers = new Members;  
								      $objMembers->set_id($objMembers->CleanData($_POST["data_id"]));
								      $details = $objMembers->get_member_by_id();
								      print_r($details);  
								} catch (PDOException $e){echo "error";}
							 }else{
							 	echo "error";
							 }
						break;
						// get all
						case 'getAll':
							
							$objMembers = new Members;
							$members = $objMembers->get_members();
							foreach ($members as $member) {
		                        $returnData .= '
		                            <tr>
		                              <td>'.trim(strtoupper($member["member_fullname"])).'</td>
		                              <td>'.trim(strtoupper($member["member_licensenum"])).'</td>
		                              <td>'.trim(strtoupper($member["member_gender"])).'</td>
		                              <td>'.trim(strtoupper($member["member_tel_num"])).'</td>
		                              <td>'.trim(strtoupper($member["member_email"])).'</td>
		                              <td>'.trim(strtoupper($member["added"])).'</td>
		                              <td>
		                                <button class="btn-primary update_data" id="'.$member["member_id"].'"><i class="fa fa-pencil"></i> UPDATE</button> 
		                                <button class="btn-danger del_data" id="'.$member["member_id"].'"><i class="fa fa-trash"></i> DELETE</button>
		                              </td>
		                            </tr>';
		                    }

		                  	print_r($returnData);
						break;
						default:
							die();
							break;
					}

				}
			}

	$objmembersController = new membersController;
 ?>