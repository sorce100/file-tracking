<?php
	require_once("../Classes/Users.php");
	require_once("../Classes/PagesGroup.php");
	require_once("../Classes/SessionLogs.php");
	session_start();
	class usersController{
		private $userpassword;
		private $resetpassword;
		private $accStatus;
		function __construct(){
			switch (trim($_POST["mode"])) {
						// for login
						case 'login':
							try{
								if ((!empty($_POST["userName"])) || (!empty($_POST["userPassword"]))) {
								
									$objUsers = new Users();
									$objUsers->set_userName(strtolower($objUsers->CleanData($_POST["userName"])));
									$objUsers->set_accStatus($objUsers->CleanData("active"));
									$users = $objUsers->login();

									foreach ($users as $user) {
										$this->userpassword = $objUsers->CleanData($user["user_password"]);
										$this->resetpassword = $objUsers->CleanData($user["password_reset"]);
									}
									if (password_verify (strtolower($objUsers->CleanData($_POST["userPassword"])) ,  $this->userpassword)) {
										
										// check if password change is required
										switch ($this->resetpassword) {
											case 'reset':
												// if password reset, return username and password entered
												echo ($_POST["userName"]."-".$_POST["userPassword"]);
											break;
											case 'login':
												$_SESSION['user_id'] = $objUsers->CleanData($user['user_id']); // Initializing Session
												$_SESSION['user_name'] = $objUsers->CleanData($user['user_name']);
												$_SESSION['staff_id'] = $objUsers->CleanData($user['staff_id']);
												$_SESSION['group_id'] = $objUsers->CleanData($user['group_id']);
												$_SESSION['account_type'] = $objUsers->CleanData($user['account_type']);
												// when successful set session for department id and unit id
												$objUsers->set_staff_details_into_session(trim($user['staff_id']));
												// if login successfull then update user online
												$objUsers->session_status_update('ONLINE');
												// save in session log table
												$objSessionLogs = new SessionLogs();
												$objSessionLogs->session_log_start();
												
												echo $user['account_type'];

											break;
											
										}

									}
									else{echo "error";}
								}
								else{
									// if empty aa
									echo "error";
								}
							}catch(PDOException $e){echo $e;}
						break;
						// for insert
						case 'insert':
							try{
								if (!empty($_POST["accountSelect"]) || !empty($_POST["userName"]) || !empty($_POST["accGroup"]) || !empty($_POST["accPasswdReset_log"])  || !empty($_POST["accStatus_log"]) ) {
									// check if password reset or account status has being set
									$objUsers = new Users();
									$this->userpassword = strtolower($objUsers->CleanData($_POST["userPassword"]));

									$objUsers->set_accountSelect($objUsers->CleanData($_POST["accountSelect"]));
									$objUsers->set_userName($objUsers->CleanData($_POST["userName"]));
									$objUsers->set_userPassword(password_hash($this->userpassword, PASSWORD_DEFAULT));
									$objUsers->set_accPasswdReset($objUsers->CleanData($_POST["accPasswdReset_log"]));
									$objUsers->set_accGroup($objUsers->CleanData($_POST["accGroup"]));
									$objUsers->set_accStatus($objUsers->CleanData($_POST["accStatus_log"]));

									$returnedMemberDetails = $objUsers->insert();
									if ($returnedMemberDetails) {
										
										echo "success";
									}
									else{
										echo "error";
									}
								}
								else{
									echo "error";
								}

							}catch(PDOException $e){echo $e;}

						break;
						// for update
						case 'update':
								try{
									$objUsers = new Users();
									$objUsers->set_id($objUsers->CleanData($_POST["data_id"]));
									// check if password reset or account status has being set
									if (!empty($_POST["userPassword"])) {
										$this->userpassword = strtolower($objUsers->CleanData($_POST["userPassword"]));
										$objUsers->set_userPassword($objUsers->CleanData(password_hash($this->userpassword, PASSWORD_DEFAULT)));
									}
									elseif (empty($_POST["userPassword"])) {
										$objUsers->set_userPassword($objUsers->get_password());
									}
									
									$objUsers->set_userName($objUsers->CleanData($_POST["userName"]));
									$objUsers->set_accPasswdReset($objUsers->CleanData($_POST["accPasswdReset_log"]));
									$objUsers->set_accGroup($objUsers->CleanData($_POST["accGroup"]));
									$objUsers->set_accStatus($objUsers->CleanData($_POST["accStatus_log"]));

									if ($objUsers->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								}catch(PDOException $e){echo $e;}
								
						break;
						// for delete
						case 'delete':
							try{
								if(isset($_POST["data_id"])){
									  $objUsers = new Users();    
								      $objUsers->set_id($objUsers->CleanData($_POST["data_id"]));
								      $objUsers->set_recordHide("YES");
								      if($objUsers->delete()){
								      	echo "success";
								      }
								      else{
								      	echo "error";
								      }
								      
								 }else{die();}
							}catch(PDOException $e){echo $e;}
						break;
						// for update modal
						case 'updateModal':
							try{
								if(isset($_POST["data_id"])){
									  $objUsers = new Users();    
								      $objUsers->set_id($objUsers->CleanData($_POST["data_id"]));
								      $userdetails = $objUsers->get_user_by_id();
								      echo $userdetails;  
								 }else{die();}
							}catch(PDOException $e){echo $e;}
						break;

						case 'changePass':
							try{
								$passwdReset="login";
								$chPassUserName = $_POST["chPassUserName"];
								$chPassUserPassword = $_POST["chPassUserPassword"];
								$newPassword = $_POST["newPassword"];
								$newRetypePassword = $_POST["newRetypePassword"];

								if((!empty($chPassUserName)) || (!empty($chPassUserPassword)) || (!empty($newPassword)) || (!empty($newRetypePassword))){
									// check if passwords typed is the same
										if (($newPassword === $newRetypePassword) && ($chPassUserPassword != $newPassword)) {
											// check for string length, password should not be less than four characters
											if (strlen($newPassword) >= 4) {
												$objUsers = new Users();
												$this->userpassword = strtolower($objUsers->CleanData($_POST["newRetypePassword"]));
												$objUsers->set_userPassword(password_hash($this->userpassword, PASSWORD_DEFAULT));
												$objUsers->set_accPasswdReset($passwdReset);
												$objUsers->set_userName($objUsers->CleanData($_POST["chPassUserName"]));
												if ($objUsers->change_password()) {
													echo "success";
												}
												else{
													echo "error";
												}
											}
											else{
												// if password is less than 4 characters
												echo "error";
											}
										}
										else{
											// if passwords not the same and the old password is same as new password
											echo "error";
										}
								 }else{
								 	// if its empty
								 	echo "error";
								 }
							}catch(PDOException $e){echo $e;}

						break;
						// get all
						case 'getAll':
							try{
								$objUsers = new Users;
								$userAccountList = $objUsers->get_all_staff_list();
								if (!empty($userAccountList)) {
									print_r($userAccountList);
								}

							}catch(PDOException $e){echo $e;}	
						break;
						// user change password
						case 'userChangePassword':
							$passwdReset="login";
							$newPasswd = $_POST["newPasswd"];
							$retypeNewPasswd = $_POST["retypeNewPasswd"];
							try{
								if( (!empty($newPasswd)) || (!empty($retypeNewPasswd)) ){
									
										// check if passwords typed is the same
										if ($newPasswd === $retypeNewPasswd) {
											// check for string length, password should not be less than four characters
											if (strlen($newPasswd) >= 4) {
												$objUsers = new Users();
												$this->userpassword = strtolower($objUsers->CleanData($_POST["newPasswd"]));
												$objUsers->set_userPassword(password_hash($this->userpassword, PASSWORD_DEFAULT));
												$objUsers->set_accPasswdReset($passwdReset);
												$objUsers->set_userName($_SESSION['user_name']);
												if ($objUsers->change_password()) {
													echo "success";
												}
												else{
													echo "error";
												}
											}
											else{
												// if password is less than 4 characters
												echo "error";
											}
										}
										else{
											// if passwords not the same and the old password is same as new password
											echo "error";
										}

								 }else{
								 	// if its empty
								 	echo "error";
								 }
							}catch(PDOException $e){echo $e;}
						break;

						// for creating profile administator account
						case 'admin_account_insert':
							
							if ($_POST["profileSelect"] != "" || $_POST["profileUserName"] != "" || $_POST["profileUserPassword"] != "" || $_POST["profileuserAccGroup"] != "") {
								try{
									// check if password reset or account status has being set
									$objUsers = new Users();
									$this->userpassword = strtolower($objUsers->CleanData($_POST["profileUserPassword"]));

									$objUsers->set_accountSelect($objUsers->CleanData($_POST["profileSelect"]));
									$objUsers->set_userName($objUsers->CleanData($_POST["profileUserName"]));
									$objUsers->set_userPassword(password_hash($this->userpassword, PASSWORD_DEFAULT));
									$objUsers->set_accPasswdReset($objUsers->CleanData($_POST["profileuserAccPasswdReset_log"]));
									$objUsers->set_accGroup($objUsers->CleanData($_POST["profileuserAccGroup"]));
									$objUsers->set_accStatus($objUsers->CleanData($_POST["profileuserAccStatus_log"]));
									$objUsers->set_accountType("administator");
									$objUsers->set_accountProfile($objUsers->CleanData($_POST["profileSelect"]));

									if ($objUsers->insert_admin_account()) {

										echo "success";
									}
									else{
										echo "error";
									}
								}catch(PDOException $e){echo $e;}
							}
							else{
								echo "error";
							}

								

						break;
						// for admin account update
						// for update
						case 'admin_account_update':
							if ($_POST["profileSelect"] != "" || $_POST["profileUserName"] != "" || $_POST["profileuserAccGroup"] != "") {
								try{
									$objUsers = new Users();
									$objUsers->set_id($objUsers->CleanData($_POST["data_id"]));
									// check if password reset or account status has being set
									if (!empty($_POST["profileUserPassword"])) {
										$this->userpassword = strtolower($objUsers->CleanData($_POST["profileUserPassword"]));
										$objUsers->set_userPassword($objUsers->CleanData(password_hash($this->userpassword, PASSWORD_DEFAULT)));
									}
									elseif (empty($_POST["profileUserPassword"])) {
										$objUsers->set_userPassword($objUsers->get_password());
									}
									
									$objUsers->set_userName($objUsers->CleanData($_POST["profileUserName"]));
									$objUsers->set_accPasswdReset($objUsers->CleanData($_POST["profileuserAccPasswdReset_log"]));
									$objUsers->set_accGroup($objUsers->CleanData($_POST["profileuserAccGroup"]));
									$objUsers->set_accStatus($objUsers->CleanData($_POST["profileuserAccStatus_log"]));

									if ($objUsers->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								}catch(PDOException $e){echo $e;}
							}
							else{
								echo "error";
							}
								
						break;
						// get all
						case 'getAdministratorAll':
							try{
								$objUsers = new Users;
								print_r(json_encode($objUsers->get_administator_users(),true));
							}catch(PDOException $e){echo $e;}	
						break;
						default:
							echo "There was a problem";
							break;
					
				}
			}
		}
	$objusersController = new usersController();
 ?>