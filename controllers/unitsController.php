<?php
	require_once("../Classes/Units.php"); 
	session_start();
	class unitsController{
		function __construct(){
			// print_r($_POST);
			// exit();
			switch (trim($_POST["mode"])) {
				// for insert
				case 'insert':
					if (!empty($_POST["unitName"]) || !empty($_POST["unitAlias"]) || !empty($_POST["unitDepartment"])) {
						try {
							$objUnits = new Units;
							$objUnits->set_unitName($objUnits->CleanData($_POST["unitName"]));
							$objUnits->set_unitAlias($objUnits->CleanData($_POST["unitAlias"]));
							$objUnits->set_unitDepartment($objUnits->CleanData($_POST["unitDepartment"]));
							$objUnits->set_unitHead($objUnits->CleanData($_POST["unitHead"]));
							$objUnits->set_unitSecretary(trim($_POST["unitSecretary"]));
							$objUnits->set_unitNotes(trim($_POST["unitNotes"]));
							if ($objUnits->insert()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
					
				break;
			// for update
				case 'update':
					if (!empty($_POST["unitName"]) || !empty($_POST["unitAlias"]) || !empty($_POST["unitDepartment"])) {
						try {
							$objUnits = new Units;
							$objUnits->set_unitName($objUnits->CleanData($_POST["unitName"]));
							$objUnits->set_unitAlias($objUnits->CleanData($_POST["unitAlias"]));
							$objUnits->set_unitDepartment($objUnits->CleanData($_POST["unitDepartment"]));
							$objUnits->set_unitHead($objUnits->CleanData($_POST["unitHead"]));
							$objUnits->set_unitSecretary(trim($_POST["unitSecretary"]));
							$objUnits->set_unitNotes(trim($_POST["unitNotes"]));
							$objUnits->set_id($objUnits->CleanData($_POST["data_id"]));
							if ($objUnits->update()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
				break;
			// for delete
				case 'delete':
					if(!empty($_POST["data_id"])){
						try {
						  $objUnits = new Units;
						  $objUnits->set_recordHide("YES");
					      $objUnits->set_id($objUnits->CleanData($_POST["data_id"]));
					      if ($objUnits->delete()) {
					      	echo "success";
					      }
					      else{
					      	echo "error";
					      }
					    } catch (PDOException $e){echo $e;}
					 }else{
					 	echo "error";
					}
				break;
				// geting details of a member with id
				case 'updateModal':
					try {
						if(!empty($_POST["data_id"])){
						  $objUnits = new Units;  
					      $objUnits->set_id($objUnits->CleanData($_POST["data_id"]));
					      $details = $objUnits->get_unit_by_id();
					      print_r($details);  
						 }else{
						 	echo "error";
						 }
					} catch (PDOException $e){echo $e;}
				break;
				// get all
				case 'getAll':
					$objUnits = new Units;
					print_r(json_encode($objUnits->get_units(),true));

				break;
				// get all units by department select
				case 'get_department_units':
					try {
						$results='';
						if(!empty($_POST["departmentId"])){
						  $objUnits = new Units;  
					      $objUnits->set_unitDepartment($objUnits->CleanData($_POST["departmentId"]));
					      $details = $objUnits->get_all_department_units();
					      if ($details) {
					      	foreach ($details as $detail) {
					      		$results .='<option value="'.$detail["unit_id"].'">'.$detail["unit_name"].'</option>';
					      	}
					      }
					      print_r($results);  
					      
						 }else{
						 	echo "error";
						 }
					} catch (PDOException $e){echo $e;}
				break;
				// get all units
				case 'getAllReceiverUnits':
					try {
						$returnRecords='';
						$objUnits = new Units;
						$records = $objUnits->get_units();
						if (!empty($records)) {
							$returnRecords .='<div class="col-md-2">
									                <label for="title" class="col-form-label"> Select Units <span class="asterick">*</span></label>
									            </div>
									            <div class="col-md-10">
									                <div class="form-group">
									                <select class="form-control" id="receiverUnitIdSelect" name="receiverUnitIdSelect" required>
									                	<option selected disabled>Please Select</option>'; 

														foreach ($records as $record) {
													      	$returnRecords.= '<option value="'.trim($record["unit_id"]).'">'.trim($record["unit_name"]).'</option>';
													    } 
						}

							$returnRecords .= 		'</select>
													<input type="hidden" name="receiverDepartmentIdSelect" value="0">
								                </div>
								            </div>';

						print_r($returnRecords);

					} catch (PDOException $e){echo $e;}

				break;

				default:
					die();
					break;
			}

		}
	}

	$objunitsController = new unitsController;
 ?>