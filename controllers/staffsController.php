<?php
	require_once("../Classes/Staffs.php"); 
	session_start();
	class StaffsControlle{
		function __construct(){
			// print_r($_POST);
			// exit();
			switch (trim($_POST["mode"])) {
				// for insert
				case 'insert':
					if (!empty($_POST["staffFirstName"]) || !empty($_POST["staffLastName"]) || !empty($_POST["staffTelNo"]) || !empty($_POST["staffEmployeeType"]) || !empty($_POST["staffDepartmentId"]) || !empty($_POST["staffUnitId"])) {
						try {
							$objStaffs = new Staffs;
							$objStaffs->set_staffFirstName($objStaffs->CleanData($_POST["staffFirstName"]));
							$objStaffs->set_staffLastName($objStaffs->CleanData($_POST["staffLastName"]));
							$objStaffs->set_staffTelNo($objStaffs->CleanData($_POST["staffTelNo"]));
							$objStaffs->set_staffEmail($objStaffs->CleanData($_POST["staffEmail"]));
							$objStaffs->set_staffEmployeeNum($objStaffs->CleanData($_POST["staffEmployeeNum"]));
							$objStaffs->set_staffEmployeeType($objStaffs->CleanData($_POST["staffEmployeeType"]));
							$objStaffs->set_staffDepartmentId($objStaffs->CleanData($_POST["staffDepartmentId"]));
							$objStaffs->set_staffUnitId($objStaffs->CleanData($_POST["staffUnitId"]));
							$objStaffs->set_staffNotes($objStaffs->CleanData($_POST["staffNotes"]));
							if ($objStaffs->insert()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
					
				break;
			// for update
				case 'update':
					if (!empty($_POST["staffFirstName"]) || !empty($_POST["staffLastName"]) || !empty($_POST["staffTelNo"]) || !empty($_POST["staffEmployeeType"]) || !empty($_POST["staffDepartmentId"]) || !empty($_POST["staffUnitId"])) {
						try {
							$objStaffs = new Staffs;
							$objStaffs->set_staffFirstName($objStaffs->CleanData($_POST["staffFirstName"]));
							$objStaffs->set_staffLastName($objStaffs->CleanData($_POST["staffLastName"]));
							$objStaffs->set_staffTelNo($objStaffs->CleanData($_POST["staffTelNo"]));
							$objStaffs->set_staffEmail($objStaffs->CleanData($_POST["staffEmail"]));
							$objStaffs->set_staffEmployeeNum($objStaffs->CleanData($_POST["staffEmployeeNum"]));
							$objStaffs->set_staffEmployeeType($objStaffs->CleanData($_POST["staffEmployeeType"]));
							$objStaffs->set_staffDepartmentId($objStaffs->CleanData($_POST["staffDepartmentId"]));
							$objStaffs->set_staffUnitId($objStaffs->CleanData($_POST["staffUnitId"]));
							$objStaffs->set_staffNotes($objStaffs->CleanData($_POST["staffNotes"]));
							$objStaffs->set_id($objStaffs->CleanData($_POST["data_id"]));
							if ($objStaffs->update()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
				break;
			// for delete
				case 'delete':
					if(!empty($_POST["data_id"])){
						try {
						  $objStaffs = new Staffs;
						  $objStaffs->set_recordHide("YES");
					      $objStaffs->set_id($objStaffs->CleanData($_POST["data_id"]));
					      if ($objStaffs->delete()) {
					      	echo "success";
					      }
					      else{
					      	echo "error";
					      }
					    } catch (PDOException $e){echo $e;}
					 }else{
					 	echo "error";
					}
				break;
				// geting details of a member with id
				case 'updateModal':
				// print_r($_POST["data_id"]);
				// exit();
					try {
						// if(!empty($_POST["data_id"])){
						  $objStaffs = new Staffs;  
					      $objStaffs->set_id($objStaffs->CleanData($_SESSION['staff_id']));
					      $details = $objStaffs->get_staff_by_id();
					      print_r($details);  
						 // }else{
						 // 	echo "error";
						 // }
					} catch (PDOException $e){echo $e;}
				break;
				// get all
				case 'getAll':
					$objStaffs = new Staffs;
					print_r(json_encode($objStaffs->get_units(),true));

				break;
				// get all department staff
				case 'get_department_staffs':
					try {
						$results='';
						if(!empty($_POST["departmentId"])){
						  $objStaffs = new Staffs; 
					      $objStaffs->set_staffDepartmentId($objStaffs->CleanData($_POST["departmentId"]));
					      $details = $objStaffs->get_all_department_staff();
					      if ($details) {
					      	foreach ($details as $detail) {
					      		$results .='<option value="'.$detail["staff_id"].'">'.$detail["staff_first_name"].' '.$detail["staff_last_name"].'</option>';
					      	}
					      }
					      print_r($results);  
					      
						 }else{
						 	echo "error";
						 }
					} catch (PDOException $e){echo $e;}
				break;
				// get department selected staff ids
				case 'get_receiver_department_staffs':
					try {
						if(!empty($_POST["departmentId"])){
						  $returnRecords='';
						  $objStaffs = new Staffs; 
					      $objStaffs->set_staffDepartmentId($objStaffs->CleanData($_POST["departmentId"]));
					      $details = $objStaffs->get_all_department_staff();

					      if ($details) {
								      	$returnRecords.='
								      					<div class="col-md-2">
											                <label for="title" class="col-form-label"> Select Staffs </label>
											            </div>
											            <div class="col-md-10">

									      					<div class="table-responsive">
													          <table class="table table-striped jambo_table tableList">
													            <thead>
													              <th width="10%">Select</th>
													              <th width="30%">Staff Name</th>
													              <th width="20%">Staff Type</th>
													              <th width="40%">Staff Notes</th>
													            </thead>
													            <tbody>';
					      	foreach ($details as $detail) {
										      	$returnRecords.='<tr>
										      						<td><input type="checkbox" value="'.trim($detail["staff_id"]).'" class="selectedReceiverStaff" name="selectedReceiverStaff[]"></td>
										      						<td>'.trim($detail["staff_first_name"]).' '.trim($detail["staff_last_name"]).'</td>
										      						<td>'.trim($detail["staff_employee_type"]).'</td>
										      						<td>'.trim($detail["staff_notes"]).'</td>
										      					</tr>';

					      	}
								      	$returnRecords.= '		</tbody>
												         	 </table>
												        	</div>
														</div>';
					      }

					      print_r($returnRecords);  
					      
						 }else{
						 	echo "error";
						 }
					} catch (PDOException $e){echo $e;}
				break;
				// get all units staff
				case 'get_receiver_unit_staffs':
					try {
						if(!empty($_POST["unitId"])){
						  $returnRecords='';

						  $objStaffs = new Staffs; 
					      $objStaffs->set_staffUnitId($objStaffs->CleanData($_POST["unitId"]));
					      $details = $objStaffs->get_all_unit_staff();

					      if ($details) {
								      	$returnRecords.='
								      					<div class="col-md-2">
											                <label for="title" class="col-form-label"> Select Staffs </label>
											            </div>
											            <div class="col-md-10">

									      					<div class="table-responsive">
													          <table class="table table-striped jambo_table tableList">
													            <thead>
													              <th width="10%">Select</th>
													              <th width="30%">Staff Name</th>
													              <th width="20%">Staff Type</th>
													              <th width="40%">Staff Notes</th>
													            </thead>
													            <tbody>';
					      	foreach ($details as $detail) {
										      	$returnRecords.='<tr>
										      						<td><input type="checkbox" value="'.trim($detail["staff_id"]).'" class="selectedReceiverStaff" name="selectedReceiverStaff[]"></td>
										      						<td>'.trim($detail["staff_first_name"]).' '.trim($detail["staff_last_name"]).'</td>
										      						<td>'.trim($detail["staff_employee_type"]).'</td>
										      						<td>'.trim($detail["staff_notes"]).'</td>
										      					</tr>';
					      	}
								      	$returnRecords.= '		</tbody>
												         	 </table>
												        	</div>
														</div>';
					      }

					      print_r($returnRecords);  
					      
						 }else{
						 	echo "error";
						 }
					} catch (PDOException $e){echo $e;}
				break;

				// get all staff
				case 'get_all_staff':
					try {
						  $returnRecords='';

						  $objStaffs = new Staffs; 
					      $details = $objStaffs->get_staffs();

					      if ($details) {
								      	$returnRecords.='
								      					<input type="hidden" name="receiverDepartmentIdSelect" value="0">
								      					<input type="hidden" name="receiverUnitIdSelect" value="0">
								      					
								      					<div class="col-md-2">
											                <label for="title" class="col-form-label"> Select Staffs </label>
											            </div>
											            <div class="col-md-10">

									      					<div class="table-responsive">
													          <table class="table table-striped jambo_table tableList">
													            <thead>
													              <th width="10%">Select</th>
													              <th width="25%">Staff Name</th>
													              <th width="10%">Staff Type</th>
													              <th width="15%">Staff Department</th>
													              <th width="10%">Staff Unit</th>
													              <th width="30%">Staff Notes</th>
													            </thead>
													            <tbody>';

					      	foreach ($details as $detail) {

										      	$returnRecords.='<tr>
										      						<td><input type="checkbox" value="'.trim($detail["staff_id"]).'" class="selectedReceiverStaff" name="selectedReceiverStaff[]"></td>
										      						<td>'.trim($detail["staff_first_name"]).' '.trim($detail["staff_last_name"]).'</td>
										      						<td>'.trim($detail["staff_employee_type"]).'</td>
										      						<td>'.trim($detail["department_name"]).'</td>
										      						<td>'.trim($detail["unit_name"]).'</td>
										      						<td>'.trim($detail["staff_notes"]).'</td>
										      					</tr>';

					      	}

								      	$returnRecords.= '		</tbody>
												         	 </table>
												        	</div>
														</div>';
					      }

					      print_r($returnRecords);  
					      
					} catch (PDOException $e){echo $e;}
				break;
				default:
					die();
					break;
			}

		}
	}

	$objStaffsController = new StaffsControlle;
 ?>