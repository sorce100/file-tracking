<?php
	require_once("../Classes/DocTransfer.php"); 
	session_start();
	class docTransferController{
		function __construct(){
			// print_r($_POST);
			// exit();
			switch (trim($_POST["mode"])) {
				// for insert
				case 'insert':
				// print_r($_POST);
				// exit();
					if (!empty($_POST["selectDocReceiverCategory"]) || !empty($_POST["receiverDepartmentIdSelect"]) || !empty($_POST["receiverUnitIdSelect"]) || !empty($_POST["selectedDocsIds"])) {
						try {
								$selectedReceiverStaffArray = [];
								// check if selected receiver staff is not empty then loop through and use the id as index then set the status to unread
								if (!empty($_POST["selectedReceiverStaff"])) {

									foreach ($_POST["selectedReceiverStaff"] as $selectedStaffId) {
										$selectedReceiverStaffArray[$selectedStaffId] = "UNREAD";
									}
								}
								else{
										$selectedReceiverStaffArray[0] = "NONE";
								}
								// ///////////////////////////////////////////////////////////////////
								$insertStatus = "";
								$objDocTransfer = new DocTransfer;
								// get the size of array of the selected records to loop through and save individually
								$returnedSelectedDocumentsIds = json_decode($_POST["selectedDocsIds"],true);

								foreach ($returnedSelectedDocumentsIds as $documentId) {
									$objDocTransfer->set_docReceivedId($documentId);
									$objDocTransfer->set_docTransferReceiverDepartmentId($objDocTransfer->CleanData($_POST["receiverDepartmentIdSelect"]));
									$objDocTransfer->set_docTransferReceiverUnitId($objDocTransfer->CleanData($_POST["receiverUnitIdSelect"]));
									$objDocTransfer->set_docTransferReceiverStaffIds(json_encode($selectedReceiverStaffArray,true));
									$objDocTransfer->set_docTransferReceiverCategory($objDocTransfer->CleanData($_POST["selectDocReceiverCategory"]));
									// check if particular file has already being transfered by particular staff
									// above not done yet
									
									$insertStatus = $objDocTransfer->insert();
								}
								

								if ($insertStatus) {
									echo "success";
								}
								else{
									echo "error";
								}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
					
				break;
			// for update
				case 'update':
					if (!empty($_POST["departmentName"]) || !empty($_POST["departmentHead"]) || !empty($_POST["departmentSecretary"])) {
						try {
							$objDocTransfer = new DocTransfer;
							$objDocTransfer->set_departmentName($objDocTransfer->CleanData($_POST["departmentName"]));
							$objDocTransfer->set_departmentHead($objDocTransfer->CleanData($_POST["departmentHead"]));
							$objDocTransfer->set_departmentSecretary($objDocTransfer->CleanData($_POST["departmentSecretary"]));
							$objDocTransfer->set_departmentNotes(trim($_POST["departmentNotes"]));
							$objDocTransfer->set_id($objDocTransfer->CleanData($_POST["data_id"]));
							if ($objDocTransfer->update()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
				break;
			// for delete
				case 'delete':
					if(!empty($_POST["data_id"])){
						try {
						  $objDocTransfer = new DocTransfer;
						  $objDocTransfer->set_recordHide("YES");
					      $objDocTransfer->set_id($objDocTransfer->CleanData($_POST["data_id"]));
					      if ($objDocTransfer->delete()) {
					      	echo "success";
					      }
					      else{
					      	echo "error";
					      }
					    } catch (PDOException $e){echo $e;}
					 }else{
					 	echo "error";
					}
				break;
				// geting details of a member with id
				case 'updateModal':
					try {
						if(!empty($_POST["data_id"])){
						  $objDocTransfer = new DocTransfer;  
					      $objDocTransfer->set_id($objDocTransfer->CleanData($_POST["data_id"]));
					      $details = $objDocTransfer->get_department_by_id();
					      print_r($details);  
						 }else{
						 	echo "error";
						 }
					} catch (PDOException $e){echo $e;}
				break;
				// get all
				case 'getAllReceiverDepartments':
					try {
						$returnRecords='';
						$objDocTransfer = new DocTransfer;
						$records = $objDocTransfer->get_departments();
						if (!empty($records)) {
							$returnRecords .='<div class="col-md-2">
									                <label for="title" class="col-form-label"> Select Department <span class="asterick">*</span></label>
									            </div>
									            <div class="col-md-10">
									                <div class="form-group">
									                <select class="form-control" id="receiverDepartmentIdSelect" name="receiverDepartmentIdSelect" required>
									                	<option selected disabled>Please Select</option>'; 

														foreach ($records as $record) {
													      	$returnRecords.= '<option value="'.trim($record["department_id"]).'">'.trim($record["department_name"]).'</option>';
													    } 
						}

							$returnRecords .= 		'</select>
													<input type="hidden" name="receiverUnitIdSelect" value="0">
								                </div>
								            </div>';

						print_r($returnRecords);

					} catch (PDOException $e){echo $e;}

				break;
				// get all document transfer history
				case 'getAllDocumentTransferHistory':
				$objDocTransfer = new DocTransfer;
				$records = $objDocTransfer->get_all_document_transfer_history();
				if (!empty($records)) {
					print_r($records);
				}

				break;

				// for staff accepted receipt of file
				case 'staffAcceptedTranfer';
				if (!empty($_POST['docTransferId'])) {
					$objDocTransfer = new DocTransfer;
					$objDocTransfer->set_id($objDocTransfer->CleanData($_POST["docTransferId"]));
					$records = $objDocTransfer->staff_accepted_document_tranfer();
					if (!empty($records)) {
						print_r($records);
					}
				} else {
					echo "error";
				}
				
				break;
				default:
					echo "error";
				break;
			}

		}
	}

	$objdocTransferController = new docTransferController;
 ?>