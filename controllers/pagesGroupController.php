<?php
	require_once("../Classes/PagesGroup.php"); 
	session_start();
	class PagesGroupController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							if ($_POST["pagesGroupName"] != "") {
								$objPagesGroup = new PagesGroup;
								$objPagesGroup->set_pagesGroupName($objPagesGroup->CleanData($_POST["pagesGroupName"]));
								$objPagesGroup->set_pagesId(json_encode($_POST["pagesId"]));
								if ($objPagesGroup->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							$objPagesGroup = new PagesGroup;
							$objPagesGroup->set_pagesGroupName($objPagesGroup->CleanData($_POST["pagesGroupName"]));
							$objPagesGroup->set_pagesId(json_encode($_POST["pagesId"]));
							$objPagesGroup->set_id($objPagesGroup->CleanData($_POST["data_id"]));
							if ($objPagesGroup->update()) {
									echo "success";
								}
								else{
									echo "error";
								}
						break;
					// for delete
						case 'delete':
							if(isset($_POST["data_id"])){
									 $objPagesGroup = new PagesGroup;
									  $objPagesGroup->set_recordHide("YES");
								      $objPagesGroup->set_id($objPagesGroup->CleanData($_POST["data_id"]));
								      if ($objPagesGroup->delete()) {
								      	echo "success";
								      }
								      else{
								      	echo "error";
								      }
								     
								 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if($_POST["data_id"] != ""){
									 $objPagesGroup = new PagesGroup;  
								      $objPagesGroup->set_id($objPagesGroup->CleanData($_POST["data_id"]));
								      $pages_details = $objPagesGroup->get_group_by_id();
								      print_r($pages_details);  
								 }else{
								 	echo "error";
								 }
						break;
						// get all
						case 'getAll':
							$objPagesGroup = new PagesGroup;
							print_r(json_encode($objPagesGroup->get_pages_groups_all(),true));

						break;
						default:
							die();
							break;
					}

				}
			}

	$objPagesGroupController = new PagesGroupController;
 ?>