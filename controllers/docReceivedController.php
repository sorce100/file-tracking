<?php
	require_once("../Classes/DocReceived.php"); 
	session_start();
	class docReceivedController{
		function __construct(){
			
				$returnData='';
					$folderPath = "../uploads/received_documents/";
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							if (empty($_POST["docReceivedFirstName"]) || empty($_POST["docReceivedLastName"]) || empty($_POST["docReceivedCompany"]) || empty($_POST["docReceivedType"]) || empty($_POST["docReceivedDocCopies"]) || empty($_POST["docReceivedSubject"])) {

								echo "error";
								exit();
							}
							else{
								// ///////////////////////NO IMAGE ADDED////////////////////////////////////
									if ($_FILES['file']['size'] == 0) {
										// print_r($_POST);
										try {
											$objDocReceived = new DocReceived;
											$objDocReceived->set_docReceivedSource($objDocReceived->CleanData($_POST["docReceivedSource"]));
											$objDocReceived->set_docReceivedFirstName($objDocReceived->CleanData($_POST["docReceivedFirstName"]));
											$objDocReceived->set_docReceivedLastName($objDocReceived->CleanData($_POST["docReceivedLastName"]));
											$objDocReceived->set_docReceivedTelNum1($objDocReceived->CleanData($_POST["docReceivedTelNum1"]));
											$objDocReceived->set_docReceivedTelNum2($objDocReceived->CleanData($_POST["docReceivedTelNum2"]));
											$objDocReceived->set_docReceivedEmail($objDocReceived->CleanData($_POST["docReceivedEmail"]));
											$objDocReceived->set_docReceivedCompany($objDocReceived->CleanData($_POST["docReceivedCompany"]));
											$objDocReceived->set_docReceivedType($objDocReceived->CleanData($_POST["docReceivedType"]));
											$objDocReceived->set_docReceivedDocCopies($objDocReceived->CleanData($_POST["docReceivedDocCopies"]));
											$objDocReceived->set_docReceivedSubject($_POST["docReceivedSubject"]);
											$objDocReceived->set_docReceivedNotes($_POST["docReceivedNotes"]);
											$objDocReceived->set_folderName("NONE");
											$objDocReceived->set_scannedImage("NONE");
										
											// $printReturn = $objDocReceived->insert();
											// print_r($printReturn);
											if ($objDocReceived->insert()) {
												echo "success";
											}
											else{
												echo "error";
											}

										} catch (PDOException $e){echo $e;}

									}
									// //////////////////////IMAGE ADDED//////////////////////////////////////
									else if ($_FILES['file']['size'] != 0) {
										try {
											$foldername=trim(date("m-d-Yis")).$_SESSION['staff_id'];
											$foldercreate=$folderPath.$foldername."/";
											// error for when folder could not created to contain images uploaded
											if(!mkdir($foldercreate,0777,true)) {
												echo "error";
											}

											// declaring a new variable forthe gloabl 
											$files=$_FILES['file'];
											$allowed=array('pdf');

											$filename = $_FILES['file']['name'];
											$file_tmp =$_FILES['file']['tmp_name'];
											$file_size =$_FILES['file']['size'];
											$file_error = $_FILES['file']['error'];
											$file_ext=explode('.',$filename);
											$filename= current($file_ext);
											$file_ext = strtolower(end($file_ext));

											if (!in_array($file_ext,$allowed)) {
												// if file being uploaded is not in array then prompt
												echo "Unsupported file";
											}
											else if (in_array($file_ext,$allowed)) {
												// check if ther is any errors from the uploaded file
												if ($file_error === 0) {
													// check if file uploaded has ther righ size
													if ($file_size < 100000000000000000) {
														// creating unique ids for uploads instead of names of files, number is in microsecond
														$filenewname=$filename.".".$file_ext;			
														$filedestination = $foldercreate.$filenewname;
														// moving file from temporal location to permanent storage
														$fileup=move_uploaded_file($file_tmp,$filedestination);
													}
														// end of if to check file size
												}
												// end of file erroe
												else{
													echo "error";
													die();
												}
											}
											// end of if file is allowed

											// if file is uploaded successfully then save details in database
											if ($fileup) {
												$objDocReceived = new DocReceived;
												$objDocReceived->set_docReceivedSource($objDocReceived->CleanData($_POST["docReceivedSource"]));
												$objDocReceived->set_docReceivedFirstName($objDocReceived->CleanData($_POST["docReceivedFirstName"]));
												$objDocReceived->set_docReceivedLastName($objDocReceived->CleanData($_POST["docReceivedLastName"]));
												$objDocReceived->set_docReceivedTelNum1($objDocReceived->CleanData($_POST["docReceivedTelNum1"]));
												$objDocReceived->set_docReceivedTelNum2($objDocReceived->CleanData($_POST["docReceivedTelNum2"]));
												$objDocReceived->set_docReceivedEmail($objDocReceived->CleanData($_POST["docReceivedEmail"]));
												$objDocReceived->set_docReceivedCompany($objDocReceived->CleanData($_POST["docReceivedCompany"]));
												$objDocReceived->set_docReceivedType($objDocReceived->CleanData($_POST["docReceivedType"]));
												$objDocReceived->set_docReceivedDocCopies($objDocReceived->CleanData($_POST["docReceivedDocCopies"]));
												$objDocReceived->set_docReceivedSubject($_POST["docReceivedSubject"]);
												$objDocReceived->set_docReceivedNotes($_POST["docReceivedNotes"]);
												$objDocReceived->set_folderName($foldername);
												$objDocReceived->set_scannedImage($filenewname);
											
												if ($objDocReceived->insert()) {
													echo "success";
												}
												else{
													echo "error";
												}
											}
											else{
												echo "error";
											}

										} catch (PDOException $e){echo $e;}
									}
							}
									
						break;
					// for update
						case 'update':
							if (empty($_POST["docReceivedFirstName"]) || empty($_POST["docReceivedLastName"]) || empty($_POST["docReceivedCompany"]) || empty($_POST["docReceivedType"]) || empty($_POST["docReceivedDocCopies"]) || empty($_POST["docReceivedSubject"])) {

								echo "error";
								exit();
							}
							else{
								// ///////////////////////NO IMAGE ADDED////////////////////////////////////
									$scannedImage = trim($_POST["returnedImg"]);
									if ($_FILES['file']['size'] == 0) {
										try {
											$objDocReceived = new DocReceived;
											$objDocReceived->set_memFullName($objDocReceived->CleanData($_POST["memFullName"]));
											$objDocReceived->set_memDob($objDocReceived->CleanData($_POST["memDob"]));
											$objDocReceived->set_memHomeTown($objDocReceived->CleanData($_POST["memHomeTown"]));
											$objDocReceived->set_memCardNumber($objDocReceived->CleanData($_POST["memCardNumber"]));
											$objDocReceived->set_memGhisDipNo($objDocReceived->CleanData($_POST["memGhisDipNo"]));
											$objDocReceived->set_kinFullname($objDocReceived->CleanData($_POST["kinFullname"]));
											$objDocReceived->set_kinPhonenum($objDocReceived->CleanData($_POST["kinPhonenum"]));
											$objDocReceived->set_memLiceNnum($objDocReceived->CleanData($_POST["memLiceNnum"]));
											$objDocReceived->set_memInductionYear($objDocReceived->CleanData($_POST["memInductionYear"]));
											$objDocReceived->set_memPhoneNum1($objDocReceived->CleanData($_POST["memPhoneNum1"]));
											$objDocReceived->set_memPhoneNum2($objDocReceived->CleanData($_POST["memPhoneNum2"]));
											$objDocReceived->set_mememail($objDocReceived->CleanData($_POST["mememail"]));
											$objDocReceived->set_memecity($objDocReceived->CleanData($_POST["memecity"]));
											$objDocReceived->set_memBussinessAddress($objDocReceived->CleanData($_POST["memBussinessAddress"]));
											$objDocReceived->set_memResidentAddress($objDocReceived->CleanData($_POST["memResidentAddress"]));
											$objDocReceived->set_memStatus($objDocReceived->CleanData($_POST["memStatus"]));
											$objDocReceived->set_memBankName($objDocReceived->CleanData($_POST["memBankName"]));
											$objDocReceived->set_memBankAccname($objDocReceived->CleanData($_POST["memBankAccname"]));
											$objDocReceived->set_memBankAccnum($objDocReceived->CleanData($_POST["memBankAccnum"]));
											$objDocReceived->set_memBankBranch($objDocReceived->CleanData($_POST["memBankBranch"]));
											$objDocReceived->set_memGender($objDocReceived->CleanData($_POST["memGender"]));
											$objDocReceived->set_memMaritalStatus($objDocReceived->CleanData($_POST["memMaritalStatus"]));
											$objDocReceived->set_scannedImage($scannedImage);
											$objDocReceived->set_memberType($objDocReceived->CleanData($_POST["memType_log"]));
											$objDocReceived->set_id($objDocReceived->CleanData($_POST["data_id"]));

											if ($objDocReceived->update()) {
												echo "success";
											}
											else{
												echo "error1";
											}

										} catch (PDOException $e){echo $e;}

									}
									// //////////////////////IMAGE ADDED//////////////////////////////////////
									else if ($_FILES['file']['size'] != 0) {
										
										try {
											// declaring a new variable forthe gloabl 
											$files=$_FILES['file'];
											$allowed=array('png','jpeg','jpg');

											$filename = $_FILES['file']['name'];
											$file_tmp =$_FILES['file']['tmp_name'];
											$file_size =$_FILES['file']['size'];
											$file_error = $_FILES['file']['error'];
											$file_ext=explode('.',$filename);
											$filename= current($file_ext);
											$file_ext = strtolower(end($file_ext));

											if (!in_array($file_ext,$allowed)) {
												// if file being uploaded is not in array then prompt
												echo "Unsupported file";
											}
											else if (in_array($file_ext,$allowed)) {
												// check if ther is any errors from the uploaded file
												if ($file_error === 0) {
													// check if file uploaded has ther righ size
													if ($file_size < 100000000000000000) {
														// creating unique ids for uploads instead of names of files, number is in microsecond
														$filenewname=$filename.".".$file_ext;			
														$filedestination = $foldercreate.$filenewname;
														// //////////////////////Remove file before uploadin new ones
														if (($scannedImage != "NONE" ) || (file_exists($folderPath.$scannedImage))) {
															unlink($folderPath.$scannedImage);
														}
														// ////////////////////////////////////////////////////

														// moving file from temporal location to permanent storage
														$fileup=move_uploaded_file($file_tmp,$filedestination);
													}
														// end of if to check file size
												}
														// end of file erroe
														else{
															echo "error";
															die();
														}
											}
											// end of if file is allowed

											// if file is uploaded successfully then save details in database
											if ($fileup) {
												$objDocReceived = new DocReceived;
												$objDocReceived->set_memFullName($objDocReceived->CleanData($_POST["memFullName"]));
												$objDocReceived->set_memDob($objDocReceived->CleanData($_POST["memDob"]));
												$objDocReceived->set_memHomeTown($objDocReceived->CleanData($_POST["memHomeTown"]));
												$objDocReceived->set_memCardNumber($objDocReceived->CleanData($_POST["memCardNumber"]));
												$objDocReceived->set_memGhisDipNo($objDocReceived->CleanData($_POST["memGhisDipNo"]));
												$objDocReceived->set_kinFullname($objDocReceived->CleanData($_POST["kinFullname"]));
												$objDocReceived->set_kinPhonenum($objDocReceived->CleanData($_POST["kinPhonenum"]));
												$objDocReceived->set_memLiceNnum($objDocReceived->CleanData($_POST["memLiceNnum"]));
												$objDocReceived->set_memInductionYear($objDocReceived->CleanData($_POST["memInductionYear"]));
												$objDocReceived->set_memPhoneNum1($objDocReceived->CleanData($_POST["memPhoneNum1"]));
												$objDocReceived->set_memPhoneNum2($objDocReceived->CleanData($_POST["memPhoneNum2"]));
												$objDocReceived->set_mememail($objDocReceived->CleanData($_POST["mememail"]));
												$objDocReceived->set_memecity($objDocReceived->CleanData($_POST["memecity"]));
												$objDocReceived->set_memBussinessAddress($objDocReceived->CleanData($_POST["memBussinessAddress"]));
												$objDocReceived->set_memResidentAddress($objDocReceived->CleanData($_POST["memResidentAddress"]));
												$objDocReceived->set_memStatus($objDocReceived->CleanData($_POST["memStatus"]));
												$objDocReceived->set_memBankName($objDocReceived->CleanData($_POST["memBankName"]));
												$objDocReceived->set_memBankAccname($objDocReceived->CleanData($_POST["memBankAccname"]));
												$objDocReceived->set_memBankAccnum($objDocReceived->CleanData($_POST["memBankAccnum"]));
												$objDocReceived->set_memBankBranch($objDocReceived->CleanData($_POST["memBankBranch"]));
												$objDocReceived->set_memGender($objDocReceived->CleanData($_POST["memGender"]));
												$objDocReceived->set_memMaritalStatus($objDocReceived->CleanData($_POST["memMaritalStatus"]));
												$objDocReceived->set_scannedImage($filenewname);
												$objDocReceived->set_memberType($objDocReceived->CleanData($_POST["memType_log"]));

												$objDocReceived->set_id($objDocReceived->CleanData($_POST["data_id"]));
												if ($objDocReceived->update()) {
													echo "success";
												}
												else{
													echo "error";
												}
											}
											else{
												echo "error";
											}

										} catch (PDOException $e){echo "error";}
									}
							}
						break;
					// for delete
						case 'delete':
							if(!empty($_POST["data_id"])){
									 $objDocReceived = new DocReceived;
									  $objDocReceived->set_recordHide("YES");
								      $objDocReceived->set_id($objDocReceived->CleanData($_POST["data_id"]));
								      if ($objDocReceived->delete()) {
								      	echo "success";
								      }
								      else{
								      	echo "error";
								      }
								     
								 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
								try {
									 $objDocReceived = new DocReceived;  
								      $objDocReceived->set_id($objDocReceived->CleanData($_POST["data_id"]));
								      $details = $objDocReceived->get_received_docs_by_id();
								      print_r($details);  
								} catch (PDOException $e){echo "error";}
							 }else{
							 	echo "error";
							 }
						break;
						// get details of selected documents
						case 'getSelectedDocuments':
							if(!empty($_POST["returnedArrays"])){
								$returnRecords="";
								try {
								  $objDocReceived = new DocReceived;  
							      $records = $objDocReceived->get_selected_documents($_POST["returnedArrays"]);
							      foreach ($records as $record) {
							      	$returnRecords.= "<tr><b>
											              <td>".trim($record['document_received_source'])."</td>
											              <td>".trim($record['document_received_type'])."</td>
											              <td>".trim($record['document_received_copies_num'])."</td>
											              <td>".trim($record['document_received_subject'])."</td>
											              <td>".trim($record['document_received_file_name'])."</td>
											              <td>".trim($record["docreceivedfromname"])."</td>
											              <td>".trim($record['company_name'])."</td>
											              <td>".trim($record['added'])."</td>
											            </b>
											         </tr>";
							      } 
							      	//add the selected docs ids to to the dom
							      	$returnRecords.="<input type='hidden' name='selectedDocsIds' value='".$_POST["returnedArrays"]."'>";

							      print_r($returnRecords);
								} catch (PDOException $e){echo $e;}
							 }else{
							 	echo "error";
							 }
						break;
						// get document details for transfer history
						case 'get_document_details_and_history':
							if(!empty($_POST["documentReceivedId"])){
								try {
									 $objDocReceived = new DocReceived;  
								      $objDocReceived->set_id($objDocReceived->CleanData($_POST["documentReceivedId"]));
								      $details = $objDocReceived->get_document_detail_and_history_by_doc_id();
								      print_r($details);  
								} catch (PDOException $e){echo $e;}
							 }else{
							 	echo "error";
							 }
						break;

						default:
							die();
							break;
					}

				}
			}

	$objdocReceivedController = new docReceivedController;
 ?>