<?php
	require_once("../Classes/Departments.php"); 
	session_start();
	class departmentsController{
		function __construct(){
			// print_r($_POST);
			// exit();
			switch (trim($_POST["mode"])) {
				// for insert
				case 'insert':
					if (!empty($_POST["departmentName"]) || !empty($_POST["departmentHead"]) || !empty($_POST["departmentSecretary"])) {
						try {
							$objDepartments = new Departments;
							$objDepartments->set_departmentName($objDepartments->CleanData($_POST["departmentName"]));
							$objDepartments->set_departmentHead($objDepartments->CleanData($_POST["departmentHead"]));
							$objDepartments->set_departmentSecretary($objDepartments->CleanData($_POST["departmentSecretary"]));
							$objDepartments->set_departmentNotes(trim($_POST["departmentNotes"]));
							if ($objDepartments->insert()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
					
				break;
			// for update
				case 'update':
					if (!empty($_POST["departmentName"]) || !empty($_POST["departmentHead"]) || !empty($_POST["departmentSecretary"])) {
						try {
							$objDepartments = new Departments;
							$objDepartments->set_departmentName($objDepartments->CleanData($_POST["departmentName"]));
							$objDepartments->set_departmentHead($objDepartments->CleanData($_POST["departmentHead"]));
							$objDepartments->set_departmentSecretary($objDepartments->CleanData($_POST["departmentSecretary"]));
							$objDepartments->set_departmentNotes(trim($_POST["departmentNotes"]));
							$objDepartments->set_id($objDepartments->CleanData($_POST["data_id"]));
							if ($objDepartments->update()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
				break;
			// for delete
				case 'delete':
					if(!empty($_POST["data_id"])){
						try {
						  $objDepartments = new Departments;
						  $objDepartments->set_recordHide("YES");
					      $objDepartments->set_id($objDepartments->CleanData($_POST["data_id"]));
					      if ($objDepartments->delete()) {
					      	echo "success";
					      }
					      else{
					      	echo "error";
					      }
					    } catch (PDOException $e){echo $e;}
					 }else{
					 	echo "error";
					}
				break;
				// geting details of a member with id
				case 'updateModal':
					try {
						if(!empty($_POST["data_id"])){
						  $objDepartments = new Departments;  
					      $objDepartments->set_id($objDepartments->CleanData($_POST["data_id"]));
					      $details = $objDepartments->get_department_by_id();
					      print_r($details);  
						 }else{
						 	echo "error";
						 }
					} catch (PDOException $e){echo $e;}
				break;
				// get all
				case 'getAllReceiverDepartments':
					try {
						$returnRecords='';
						$objDepartments = new Departments;
						$records = $objDepartments->get_departments();
						if (!empty($records)) {
							$returnRecords .='<div class="col-md-2">
									                <label for="title" class="col-form-label"> Select Department <span class="asterick">*</span></label>
									            </div>
									            <div class="col-md-10">
									                <div class="form-group">
									                <select class="form-control" id="receiverDepartmentIdSelect" name="receiverDepartmentIdSelect" required>
									                	<option selected disabled>Please Select</option>'; 

														foreach ($records as $record) {
													      	$returnRecords.= '<option value="'.trim($record["department_id"]).'">'.trim($record["department_name"]).'</option>';
													    } 
						}

							$returnRecords .= 		'</select>
													<input type="hidden" name="receiverUnitId" value="0">
								                </div>
								            </div>';

						print_r($returnRecords);

					} catch (PDOException $e){echo $e;}

				break;
				default:
					die();
					break;
			}

		}
	}

	$objdepartmentsController = new departmentsController;
 ?>