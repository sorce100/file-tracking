<?php 
  require_once('../includes/header.php');
  include_once('../Classes/Counts.php'); 
 ?>

<div class="row">
  <!-- fisrt col -->
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <!--  -->
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-unlock"></i> Profile Details</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <th></th>
              <th></th>
            </thead>
            <tbody>
              <?php 
                $profileDetails = $objStaffs->get_staff_profile_details();
                if (!empty($profileDetails)) {
                  print_r($profileDetails);
                }
               ?>
            </tbody>
          </table>
        </div>

        <button class="bg-dark-blue btn-block" data-toggle="modal" data-target="#sendComplaintModal" data-backdrop="static" data-keyboard="false" >Contact Admin <i class="fa fa-phone"></i></button>

      </div>
    </div>
    <!--  -->
  </div>
  <!-- second col -->
  <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">

    <div class="row top_tiles">
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="documents_transfer.php">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-plus-square-o"></i></div>
            <div class="count"><?php echo $objDocTransfer->count_get_doc_transfer_received_by_staff();?></div>
            <h3>New Document Transfer</h3>
            <!-- <p>Lorem ipsum psdea itgum rixt.</p> -->
          </div>
        </a>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="general_staff_unit.php">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-home"></i></div>
            <div class="count">5</div>
            <h3>Unit</h3>
            <!-- <p>Lorem ipsum psdea itgum rixt.</p> -->
          </div>
        </a>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="general_staff_department.php">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-university"></i></div>
            <div class="count">5</div>
            <h3>Departments</h3>
            <!-- <p>Lorem ipsum psdea itgum rixt.</p> -->
          </div>
        </a>
      </div>
    </div>

    <hr>
    <!-- ssb -->
    <div class="row">
        <!-- //////////////////////////////////Recent Activities////////////////////////////////// -->
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Recent activities</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="col-md-12 col-sm-12 col-xs-12">
             <!-- <div id='calendar'></div> -->
            </div>  
          </div>
        </div>
      </div>
    </div>

 </div>
</div>


<div class="modal fade" id="sendComplaintModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title">Send Complaint To Administrator</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="addMinute_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                      <div class="col-md-3">
                        <label for="title" class="col-form-label"> Complaint No <span class="asterick"> *</span></label>
                      </div>
                      <div class="col-md-9">
                          <div class="form-group">
                            <input type="text" name="complaintNumber" id="complaintNumber" class="form-control" readonly value="<?php echo date('ymd').mt_rand(0,200)?>">
                          </div>
                      </div>
                    </div>

                    <hr>
                    <!-- 2 -->
                    <div class="row">
                      <div class="col-md-3">
                        <label for="title" class="col-form-label"> Complaint Details <span class="asterick"> *</span></label>
                      </div>
                      <div class="col-md-9">
                          <div class="form-group">
                            <textarea rows="15" name="complaintDetail" id="complaintDetail" class="form-control" placeholder="Please give a clear description of complaint &hellip;" required autocomplete="off"></textarea>
                          </div>
                      </div>
                    </div>
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="complaintMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="complaint_btn">Send Complaint <i class="fa fa-send"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<?php require_once('../includes/footer.php'); ?>

