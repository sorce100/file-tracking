<?php 
	require_once('../includes/header.php');
  include_once('../Classes/PagesGroup.php'); 
  include_once('../Classes/Pages.php'); 
 ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-globe"></i> Users Groups Setup</h2>
        <!-- add new button -->
        <div class="pull-right"><button class="btn btn-danger" data-toggle="modal" data-target="#pagesGroupModal">Add New <i class="fa fa-plus"></i></button></div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
         <div class="table-responsive">
            <table class="table table-striped jambo_table tableList">
              <thead>
                  <tr>
                      <th>Name</th>
                      <th>Added</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody id="resultsDisplay">
                <?php
                    $obPagesGroup = new PagesGroup;
                    $groups = $obPagesGroup->get_pages_groups_all(); 
                    foreach ($groups as $group) {
                          echo '
                              <tr>
                                <td>'.trim($group["pages_group_name"]).'</td>
                                <td>'.trim($group["added"]).'</td>
                                <td><button class="btn-primary update_data" id="'.$group["pages_group_id"].'"><i class="fa fa-pencil"></i> UPDATE</button> 
                                <button class="btn-danger del_data" id="'.$group["pages_group_id"].'"><i class="fa fa-trash"></i> DELETE</button></td>
                              </tr>
                            ';
                        }
                   ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- for modal -->
<div class="modal fade" id="pagesGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title" id="subject">Add New Practice</h4>
      </div>
      <div class="modal-body" id="bg">
        <form id="insert_form" method="POST">
          <!-- 1 -->
          <div class="row">
              <div class="col-md-2">
                  <label for="title" class="col-form-label">Group Name <span class="asterick">*</span></label>
              </div>
              <div class="col-md-10">
                  <div class="form-group">
                     <input type="text" name="pagesGroupName" id="pagesGroupName" class="form-control" placeholder="User Group Name &hellip;" autocomplete="off" required>
                  </div>
              </div>
          </div>
          <!-- <hr> -->
          <hr>
          <!-- 3 -->
          <div class="row">
              <div class="col-md-2">
                  <label for="title" class="col-form-label">Add Pages <span class="asterick">*</span></label>
              </div>
              <div class="col-md-10">
                 <div class="table-responsive">
                    <table class="table table-hover tableList"> 
                        <thead>
                            <tr>
                                <th width="10%">Select</th>
                                <th width="90%">Page Name</th>
                            </tr>
                        </thead>
                        <tbody id="resultsDisplay">
                            <?php
                                $objPages = new Pages;
                                $pages = $objPages->get_pages(); 
                                foreach ($pages as $page) {
                                      echo '
                                          <tr>
                                            <td><input type="checkbox" class="input-md" name="pagesId[]" id="pagesCheckBox" value="'.trim($page["pages_id"]).'"></td>
                                            <td>'.$page["pages_name"].'</td>
                                          </tr>
                                        ';
                                    }
                               ?>
                        </tbody>
                    </table>
                </div>
              </div>
          </div>
            <!-- for inserting the page id -->
            <input type="hidden" name="data_id" id="groupdata_id" value="">
            <!-- for insert query -->
            <input type="hidden" name="mode" id="groupmode" value="insert">

             <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
              <button type="submit" class="btn btn-primary" id="groupsave_btn">Save Group <i class="fa fa-floppy-o"></i></button>
           </div>
      </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<?php require_once('../includes/footer.php'); ?>
<script src="js/pageScript/page_groups.js"></script>
