<?php 
	require_once('../includes/header.php');
  require_once('../Classes/PagesGroup.php');
  require_once('../Classes/Staffs.php');
  require_once('../Classes/Users.php');
  require_once('../Classes/Departments.php');
  require_once('../Classes/Units.php');
  require_once('../Classes/Users.php');

 ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-users"></i> Users Setup</h2>
        <!-- add new button -->
        <div class="pull-right"><button class="btn btn-danger" data-toggle="modal" data-target="#usersModal">Add New <i class="fa fa-plus"></i></button></div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
         <div class="table-responsive">
              <table class="table table-striped jambo_table tableList">
                  <thead>
                      <tr>
                          <th>Name</th>
                          <th>Username</th>
                          <th>Group</th>
                          <th>Password Status</th>
                          <th>Account Status</th>
                          <th>Online / Offline</th>
                          <th>Added</th>
                          <th>Last Updated</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody id="userAccountDiv">
                     <?php 
                      $objUsers = new Users;
                      $staffUserList = $objUsers->get_all_staff_list();
                      if (!empty($staffUserList)) {
                        print_r($staffUserList);
                      }
                      ?>
                  </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- for modal -->
<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title userTitle">Add New User</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="users_form">
            <div class="row">
                <div class="col-md-12">
                  <!-- select department -->
                  <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Select Department <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <select class="form-control departmentSelect2" style="width: 100%;" name="departmentSelect" id="departmentSelect" >
                                  <option disabled selected>Select Department</option>
                                  <?php 
                                      $objDepartment = new Departments;
                                      $departments = $objDepartment->get_departments(); 
                                      foreach ($departments as $department) {

                                        echo '<option value="'.trim($department["department_id"]).'" >'.trim($department["department_name"]).'</option>';
                                      }
                                  ?>
                               </select>
                            </div>
                        </div>
                    </div>
                  <!-- select Unit -->
                  <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Select Unit <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <select class="form-control unitSelect2" style="width: 100%;" name="unitSelect" id="unitSelect" >
                                  <option disabled selected>Select Unit</option>
                                  <?php 
                                      $objUnit = new Units;
                                      $units = $objUnit->get_units(); 
                                      foreach ($units as $unit) {

                                        echo '<option value="'.trim($unit["unit_id"]).'" >'.trim($unit["unit_name"]).'</option>';
                                      }
                                  ?>
                               </select>
                            </div>
                        </div>
                    </div>
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Select Member <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <select class="form-control userSelect2" style="width: 100%;" name="accountSelect" id="accountSelect" >
                                  <option disabled selected>Select Staff</option>
                                  <?php 
                                      $objStaff = new Staffs;
                                      $staffs = $objStaff->get_staff_account_options(); 
                                      print_r($staffs);
                                      
                                 ?>
                               </select>
                            </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Username <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="text" name="userName" id="userName" class="form-control" readonly required>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Password <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                               <input type="Password" name="userPassword" id="userPassword" class="form-control" placeholder="User Account Password">
                            </div>
                        </div>
                        <!-- for password reset -->
                        <div class="col-md-3">
                          <input type="checkbox" id="accPasswdReset" data-width="120"/>
                          <input type="hidden" name="accPasswdReset_log" id="accPasswdReset_log" value="reset" />

                        </div>
                    </div>
                    <!-- 4 -->
                    <div class="row">
                      <div class="col-md-2">
                        <label for="title" class="col-form-label">Group <span class="asterick">*</span></label>
                      </div>
                      <div class="col-md-7">
                        <div class="form-group">
                             <select class="form-control" id="accGroup" name="accGroup" required>
                              <option value="" selected="selected">Select Group</option>
                              <?php 
                                  $objPagesGroup = new PagesGroup;
                                  $Groups = $objPagesGroup->get_pages_groups(); 
                                  foreach ($Groups as $Group) {
                                      echo '<option value="'.trim($Group["pages_group_id"]).'">'.$Group["pages_group_name"].'</option>';
                                  }
                             ?>
                            </select>
                        </div>
                      </div>
                    </div>
                    <!-- 5 -->
                    <div class="row">
                      <div class="col-md-2">
                        <label for="title" class="col-form-label">Account Status</label>
                      </div>
                      <div class="col-md-5">
                        <input type="checkbox" id="accStatus" data-width="100"/>
                        <input type="hidden" name="accStatus_log" id="accStatus_log" value="active" />

                      </div>
                    </div>
                    
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="usersdata_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="usersmode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="userssave_btn">Add User <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php require_once('../includes/footer.php'); ?>
<script src="js/pageScript/users.js"></script>
