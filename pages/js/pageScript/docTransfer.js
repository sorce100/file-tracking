$(document).ready(function(){ 

    $('#docTransferHistoryModal').on('hidden.bs.modal', function () {
      
    });

    // get general info and details of document

    $(document).on('click', '.accept_received_doc_btn', function(){
      var btnClicked = $(this);
      if (confirm("ARE YOU SURE YOU WANT TO ACCEPT AND ACCESS DOCUMENT?")) {
      	var docTransferId = $(this).prop("id");  
       	let mode= "staffAcceptedTranfer";  
           $.ajax({  
            url:"../controllers/docTransferController.php",  
            method:"POST",  
            data:{docTransferId:docTransferId,mode:mode},  
            success:function(results){
              // console.log(results);
              switch(results) {
                case 'error':
                  toastr.error('There was an error');
                  break;
                default:
                  toastr.success('Successful');
                  btnClicked.replaceWith(results);
                  
              }
            }  
        	});

      }else{
        return false;
      }  
     
    });


    

// transfer_doc_btn

});
