$(document).ready(function(){
  /// triggring the check
 

  $('#memType').bootstrapToggle({
    on: 'MEMBER',
    off: 'NON MEMBER',
    onstyle: 'success',
    offstyle: 'danger'
  });

  $('#memType').bootstrapToggle('on');

  $('#memType').change(function(){
    if($(this).prop('checked')){
     $('#memType_log').val('member');
    }else{
     $('#memType_log').val('non_member');
    }
  });

  // for reset modal when close
  $('#addMemberModal').on('hidden.bs.modal', function () {
      $(".modal-title").html("Add Members");
      // $(this).find('input:text').css({"border-color":"#808080"});
      $(".multistep-form")[0].reset();
      $("#saveBtn").text("Save");
      $("#member_mode").val("insert");
      $("#picDisplayDiv").html('');
      // reset progress bar
      $('#progressBar').prop('aria-valuemax',0).css('width',0 + '%').text(0 + '%');
  });
  //for inserting 
  $(".multistep-form").submit(function(e){
      e.preventDefault();
      // fields validation
        $.ajax({
        // for progress bar
        xhr:function(){
          let xhr = new XMLHttpRequest();
          xhr.upload.addEventListener('progress',function(e){
            // check if upload length is true or false
            if (e.lengthComputable) {
              let uploadPercent = Math.round((e.loaded/e.total)*100);
              // updating progress bar pecentage
              $('#progressBar').prop('aria-valuemax',uploadPercent).css('width',uploadPercent + '%').text(uploadPercent + '%');
            }
          });
          return xhr;
        },
        url:"../controllers/membersController.php",
        method:"POST",
        enctype: 'multipart/form-data',
        data:new FormData(this),  
        contentType:false,  
        processData:false,
        beforeSend:function(){  
          $('#saveBtn').text("Loading ...").prop("disabled",true); 
        },
        success:function(results){
        // $.bootstrapGrowl("Login Successfull",{ type: results,align: 'center',allow_dismiss: false,width: 'auto' }); 
           // console.log(results);
           $('#saveBtn').text("Save").prop("disabled",false);
           $("#addMemberModal").modal("hide");
           switch(results) {
              case 'success':
                get_all();
                toastr.success('Saved Successfully');
                break;
              case 'error':
                toastr.error('There was an error');
                $("#addMemberModal").modal("hide");
                $(".multistep-form")[0].reset(); 
                break;
              default:
                toastr.error('There was an error');
            }
        } 

        });  
    });
    // for update
    $('.tableList').on('click', '.update_data', function(){
       var mode= "updateModal"; 
       var data_id = $(this).prop("id");  
       $.ajax({  
            url:"../controllers/membersController.php",
            method:"POST",  
            data:{data_id:data_id,mode:mode},  
            success:function(results){
              var jsonObj = JSON.parse(results); 
              // console.log(results); 
               // changing modal title
              $(".modal-title").html("Update Member");
              $("#memFullName").val($.trim(jsonObj.member_fullname));
              $("#memDob").val($.trim(jsonObj.member_dob));
              $("#memHomeTown").val($.trim(jsonObj.member_hometown));
              $("#memCardNumber").val($.trim(jsonObj.member_card_number));
              $("#memGhisDipNo").val($.trim(jsonObj.ghislsd_dipnum));
              $("#kinFullname").val($.trim(jsonObj.member_nxtofkin));
              $("#kinPhonenum").val($.trim(jsonObj.member_nxtofkin_telnum));
              $("#memLiceNnum").val($.trim(jsonObj.member_licensenum));
              $("#memInductionYear").val($.trim(jsonObj.member_year_inducted));
              $("#memPhoneNum1").val($.trim(jsonObj.member_tel_num));
              $("#memPhoneNum2").val($.trim(jsonObj.member_tel_num1));
              $("#mememail").val($.trim(jsonObj.member_email));
              $("#memecity").val($.trim(jsonObj.member_city));
              $("#memBussinessAddress").val($.trim(jsonObj.member_buss_address));
              $("#memResidentAddress").val($.trim(jsonObj.member_res_address));
              $("#memStatus option[value="+$.trim(jsonObj.member_active_status)+"]").attr('selected', 'selected');
              $("#memBankName").val($.trim(jsonObj.member_bank_name));
              $("#memBankAccname").val($.trim(jsonObj.member_bankacc_name));
              $("#memBankAccnum").val($.trim(jsonObj.member_bankacc_num));
              $("#memBankBranch").val($.trim(jsonObj.member_bank_branch));

              switch($.trim(jsonObj.member_type)) {
                case 'member':
                  $('#memType').bootstrapToggle('on');
                  break;
                case 'non_member':
                  $('#memType').bootstrapToggle('off');
                  break;
              }
              // display image selected
              
              // //////////////////////////////////////////////////////////////////////////////////////////////////////
              switch($.trim(jsonObj.member_pic)) {
                case 'NONE':
                  $("#picDisplayDiv").html('<img src="../pages/images/user.png" width="320px" height="255px">');
                  break;
                default:
                  $("#picDisplayDiv").html('<img src="../uploads/members_images/'+$.trim(jsonObj.member_pic)+'" width="320px" height="255px">');
              }
              // /////////////////////////////////////////////////////////////////////////////////////////////////////
              $("#memGender option[value="+jsonObj.member_gender+"]").attr('selected', 'selected');
              $("#memMaritalStatus option[value="+jsonObj.member_marital_status+"]").attr('selected', 'selected');
              $("#saveBtn").text("Update Member");
              $("#member_mode").val("update");
              $("#member_data_id").val($.trim(jsonObj.member_id));
              $("#returnedImg").val($.trim(jsonObj.member_pic));
              $("#addMemberModal").modal("show");
            }  
           });  
        });

        // for delete
        $('.tableList').on('click', '.del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 var mode= "delete"; 
                 var data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/membersController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        switch(results) {
                          case 'success':
                            get_all();
                            toastr.success('Deleted Successfully');
                            break;
                          case 'error':
                            toastr.error('There was an error');
                            break;
                          default:
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
    // get all data 
         function get_all(){
            var mode= "getAll";
            $.ajax({  
              url:"../controllers/membersController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                $('#resultsDisplay').html(results);
              }  
            }); 
     }
});  