$(document).ready(function(){ 

    $('#docTransferHistoryModal').on('hidden.bs.modal', function () {
      $("#documentSourceDiv,#documentTypeDiv,#documentSubjectDiv,#documentPagesDiv,#documentAttachedDiv,#submitterNameDiv,#submitterCompanyDiv,#dateAddedDiv,#lastUpdatedDiv,#addedStaffNameDiv,#addedStaffUnitDiv,#addedStaffDepartmentDiv,#transferLogDiv,#docMinutesDiv").html('');
    });

    // get general info and details of document

    $(document).on('click', '.doc_history_btn', function(){
      var documentReceivedId = $(this).prop('id');
      var mode = "get_document_details_and_history";
      $.ajax({  
          url:"../controllers/docReceivedController.php",
          method:"POST",  
          data:{documentReceivedId:documentReceivedId,mode:mode},  
          success:function(results){
              // console.log(results);
              if(results){
                  let mainJsonObj = JSON.parse(results); 

                  let jsonObj = mainJsonObj.allResults ; 
                  $("#documentSourceDiv").html(jsonObj.document_received_source);
                  $("#documentTypeDiv").html(jsonObj.document_received_type);
                  $("#documentSubjectDiv").html(jsonObj.document_received_subject);
                  $("#documentPagesDiv").html(jsonObj.document_received_copies_num);
                  // $("#documentAttachedDiv").html(jsonObj.document_received_file_name);
                  switch($.trim(jsonObj.document_received_file_name)){
                    case 'NONE':
                      $("#documentAttachedDiv").html('NONE');
                    break;
                    default:
                      $("#documentAttachedDiv").html('<a class="readPdf" id="'+$.trim(jsonObj.document_received_folder_name)+'/'+$.trim(jsonObj.document_received_file_name)+'" > <i class="fa-3x fa fa-file-pdf-o"> </i> '+$.trim(jsonObj.document_received_file_name)+'</a>');
                  }
                  $("#submitterNameDiv").html(jsonObj.submitterfullname);
                  $("#submitterCompanyDiv").html(jsonObj.company_name);
                  $("#dateAddedDiv").html(jsonObj.added);
                  $("#lastUpdatedDiv").html(jsonObj.last_updated);
                  $("#addedStaffNameDiv").html(jsonObj.stafffullname);
                  $("#addedStaffUnitDiv").html(jsonObj.unit_name);
                  $("#addedStaffDepartmentDiv ").html(jsonObj.department_name);

                  // load all document trafer history
                  ///////////////////////////////////////////////////////////////////////////////
                  $('#transferLogDiv').html(mainJsonObj.transferHistory);
                  ///////////////////////////////////////////////////////////////////////////////
                  // get all document minutes
                  $('#docMinutesDiv').html(mainJsonObj.documentMinutes);
                  // console.log(documentMinutes);
                  // ///////////////////////////////////////////////////////////////////////////
                  $('#docTransferHistoryModal').modal('show');
              }
              else{
                toastr.error('Sorry !!! There was an error');
              }
          }   

        });

      });  
     

    // get details for minutes


    // get details for tranfer logs
    function get_all_document_transfer_logs(documentId){
        var mode= "getAllDocumentTransferHistory";
        $.ajax({  
          url:"../controllers/docTransferController.php",  
          method:"POST",  
          data:{mode:mode},  
          success:function(results){
            console.log(results);
          }  
        }); 
     }





});