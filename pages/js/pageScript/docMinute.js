$(document).ready(function(){
	$('#addMinuteLock').bootstrapToggle({
      on: 'OPEN MINUTE',
      off: 'LOCK MINUTE',
      onstyle: 'success',
      offstyle: 'danger'
    });
	$('#addMinuteLock').change(function(){
	  if($(this).prop('checked')){
	   $('#addMinuteLock_log').val('OPEN');
	  }else {
	   $('#addMinuteLock_log').val('LOCK');
	  }
	});

	// //////////////////////////////////////////////////////////////////////////////////////////////////////

	// parsley
   $('#addMinute_form').parsley();
   
    // for reset modal when close
  	$('#addMinuteModal').on('hidden.bs.modal', function () {
      $(".addMinuteTitle").html("Add New Minute");
      $("#addMinute_form")[0].reset();
      $('#addMinuteModal_btn').replaceWith('<button type="submit" class="btn btn-primary" id="addMinuteModal_btn">Save Minute<i class="fa fa-save"></i></button>'); 
      $("#addMinuteModalMode").val("insert");
      $('#addMinute_form').parsley().reset();
      $('#viewMinutesDiv').html('');
    });

  	//for inserting 
    $("#addMinute_form").submit(function(e){
        e.preventDefault();
        // fields validation
          $.ajax({
          url:"../controllers/docMinutesController.php",
          method:"POST",
          data:$("#addMinute_form").serialize(),
          beforeSend:function(){  
            $('#addMinuteModal_btn').prop('disabled','disabled').text('Loading...');  
          },
          success:function(results){ 
              // console.log(results);
               $('#addMinuteModal_btn').replaceWith('<button type="submit" class="btn btn-primary" id="addMinuteModal_btn">Save Minute<i class="fa fa-save"></i></button>'); 
               $("#addMinuteModal").modal("hide");
               switch(results) {
                  case 'success':
                    get_all();
                    toastr.success('Saved Successfully');
                    break;
                  case 'error':
                    toastr.error('There was an error');
                    $("#addMinuteModal").modal("hide");
                    $("#addMinute_form")[0].reset();  
                    break;
                  default:
                    // code block
                }
          } 

          });  
      });


    // for update
      $(document).on('click', '.updateMinute', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
	          url:"../controllers/docMinutesController.php",
	          method:"POST",  
	          data:{data_id:data_id,mode:mode},  
	          success:function(results){
	          	// console.log(results);
	               let jsonObj = JSON.parse(results);  
	               // changing modal title
	              	$(".addMinuteTitle").html("Update Minute");
	              	////////////////////////////////////////////////////////////////////////////////////////////
	                switch($.trim(jsonObj.doc_minute_lock)) {
	                  case 'OPEN':
	                    $('#addMinuteLock').bootstrapToggle('on');
	                    break;
	                  case 'LOCK':
	                    $('#addMinuteLock').bootstrapToggle('off');
	                    break;
	                }
	                //////////////////////////////////////////////////////////////////////////////////////////
					$("#addMinuteDetail").val($.trim(jsonObj.doc_minute_note));
	                $("#addMinuteModal_data_id").val($.trim(jsonObj.doc_minute_id));
	                // insert document id into update
	                $("#documentReceivedId").val($.trim(jsonObj.document_received_id));

	                $("#addMinuteModal_btn").text("Update Minute");
	                $("#addMinuteModalMode").val("update");
	                $("#addMinuteModal").modal("show");
	          }  
	         });  
        });

      // for delete
        $(document).on('click', '.deleteMinute', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
               
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/docMinutesController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                      	switch(results) {
	                      case 'success':
	                        get_all();
                            toastr.success('Deleted Successfully');
	                        break;
	                      case 'error':
	                        toastr.error('There was an error');
	                        break;
	                      default:
	                        toastr.error('There was an error');
	                    }
                      }  
                     }); 

             }else{
              return false;
            }  
        });

       // get all data 
	     function get_all(){
	        let mode = "getAll";
	        var documentReceivedId = $('#documentReceivedId').val();
	        $.ajax({  
	          url:"../controllers/docMinutesController.php",  
	          method:"POST",  
	          data:{documentReceivedId:documentReceivedId,mode:mode},  
	          success:function(results){
	            $('#viewAllMinutesDiv').html(results);
	          }  
	        }); 
	     }

	// displaying modal for minutes log 
	$(document).on('click', '.add_minute_btn', function(){
		let mode= "getAll";
    	var documentReceivedId = $(this).prop("id");
    	// insert document id into add minute form
    	$('#documentReceivedId').val(documentReceivedId);
    	// 
    	$.ajax({  
          url:"../controllers/docMinutesController.php",  
          method:"POST",  
          data:{documentReceivedId:documentReceivedId,mode:mode},  
          success:function(results){
          	switch(results) {
              case 'error':
                toastr.error('There was an error');
                break;
              default:
                $('#viewAllMinutesDiv').html(results);
            }
          }  
         }); 
    	$('#minuteLogsModal').modal('show'); 

    });



});