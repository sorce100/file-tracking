$(document).ready(function(){
  // parsley
  $('#staff_form').parsley();
   
    // for reset modal when close
  $('#staffModal').on('hidden.bs.modal', function () {
      $(".unitTitle").html("Add Staff");
      $("#staff_form")[0].reset();
      $('#staffSave_btn').replaceWith('<button type="submit" class="btn btn-primary" id="staffSave_btn">Add Staff <i class="fa fa-save"></i></button>'); 
      $("#unitMode").val("insert");
      $('#staff_form').parsley().reset();
      // $('#staffDepartmentId').find('option').not(':first').remove();
      $('#staffUnitId').find('option').not(':first').remove();
      $("#staffNotes").html('');
    });

  //for inserting 
    $("#staff_form").submit(function(e){
        e.preventDefault();
        // fields validation
          $.ajax({
          url:"../controllers/staffsController.php",
          method:"POST",
          data:$("#staff_form").serialize(),
          beforeSend:function(){  
            $('#staffSave_btn').replaceWith('<button disabled class="btn btn-primary" >Loading  <i class="fa fa-spinner" aria-hidden="true"></i></button>'); 
          },
          success:function(results){ 
              // console.log(results);
               $('#staffSave_btn').replaceWith('<button type="submit" class="btn btn-primary" id="staffSave_btn">Add Staff <i class="fa fa-save"></i></button>'); 
               $("#staffModal").modal("hide");
               switch(results) {
                  case 'success':
                    get_all();
                    toastr.success('Successfully');
                    break;
                  case 'error':
                    toastr.error('There was an error');
                    $("#staffModal").modal("hide");
                    $("#staff_form")[0].reset();  
                    break;
                  default:
                    // code block
                }
          } 

        });  
    });
  // for update
  $('.tableList').on('click', '.update_staff', function(){
     var mode= "updateModal"; 
     var data_id = $(this).prop("id");  
     $.ajax({  
          url:"../controllers/staffsController.php",
          method:"POST",  
          data:{data_id:data_id,mode:mode},  
          success:function(results){
              var jsonObj = JSON.parse(results);  
               // changing modal title
              $(".staffTitle").html("Update Staff Details");
              $("#staffFirstName").val($.trim(jsonObj.staff_first_name));
              $("#staffLastName").val($.trim(jsonObj.staff_last_name));
              $("#staffTelNo").val($.trim(jsonObj.staff_tel_num));
              $("#staffEmail").val($.trim(jsonObj.staff_email));
              $("#staffEmployeeNum").val($.trim(jsonObj.staff_employee_num));
              $("#staffEmployeeType").val($.trim(jsonObj.staff_employee_type));
              $("#staffDepartmentId").val($.trim(jsonObj.staff_department_id)).trigger('change');
              $("#staffUnitId").val($.trim(jsonObj.staff_unit_id)).change();
              $("#staffNotes").html($.trim(jsonObj.staff_notes));
              $("#staff_data_id").val($.trim(jsonObj.staff_id));
              $("#staffSave_btn").text("Update Staff");
              $("#staffMode").val("update");
              $("#staffModal").modal("show");
          }  
         });  
    });
// for delete
  $('.tableList').on('click', '.del_staff', function(){
     if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
           var mode= "delete"; 
           var data_id = $(this).prop("id");  
           $.ajax({  
                url:"../controllers/staffsController.php",  
                method:"POST",  
                data:{data_id:data_id,mode:mode},  
                success:function(results){
                  switch(results) {
                    case 'success':
                      get_all();
                      toastr.success('Deleted Successfully');
                      break;
                    case 'error':
                      toastr.error('There was an error');
                      break;
                    default:
                      toastr.error('There was an error');
                  }
                }  
               }); 

       }else{
        return false;
      }  
  });
// get all data 
   function get_all(){
      var mode= "getAll";
      $.ajax({  
        url:"../controllers/staffsController.php",  
        method:"POST",  
        data:{mode:mode},  
        success:function(results){
          $('#resultsDisplay').html('');
          var jsonObj = JSON.parse(results);
          for (var i = 0; i < jsonObj.length; i++) {
              $('#resultsDisplay').append('<tr><td>'+jsonObj[i].pages_name+'</td><td>'+jsonObj[i].pages_url+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary update_data" id="'+jsonObj[i].pages_id+'"><i class="fa fa-pencil"></i> UPDATE</button> <button class="btn-danger del_data" id="'+jsonObj[i].pages_id+'"><i class="fa fa-trash"></i> DELETE</button></td></tr>');
          }
        }  
      }); 
   }

// get all units based on department select
    $("#staffDepartmentId").change(function(){
       let departmentId = $('option:selected', this).val();
       var mode= "get_department_units"; 
           var data_id = $(this).prop("id");  
           $.ajax({  
            url:"../controllers/unitsController.php",  
            method:"POST",  
            data:{departmentId:departmentId,mode:mode},  
            success:function(results){
              // console.log(results);
              switch(results) {
                case 'error':
                  // toastr.error('There was an error');
                  break;
                default:
                  $('#staffUnitId').append(results);
                  
              }
            }  
           });
    });

});  