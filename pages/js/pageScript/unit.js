$(document).ready(function(){
  // parsley
  $('#unit_form').parsley();
   
    // for reset modal when close
  $('#unitModal').on('hidden.bs.modal', function () {
      $(".unitTitle").html("Add Pages");
      $("#unit_form")[0].reset();
      $('#unitSave_btn').replaceWith('<button type="submit" class="btn btn-primary" id="unitSave_btn">Add Unit <i class="fa fa-save"></i></button>'); 
      $("#unitMode").val("insert");
      $('#unit_form').parsley().reset();
      $('#unitHead').find('option').not(':first').remove();
      $('#unitSecretary').find('option').not(':first').remove();
    });

  //for inserting 
    $("#unit_form").submit(function(e){
        e.preventDefault();
        // fields validation
          $.ajax({
          url:"../controllers/unitsController.php",
          method:"POST",
          data:$("#unit_form").serialize(),
          beforeSend:function(){  
            $('#unitSave_btn').replaceWith('<button disabled class="btn btn-primary" >Loading  <i class="fa fa-spinner" aria-hidden="true"></i></button>'); 
          },
          success:function(results){ 
              // console.log(results);
               $('#unitSave_btn').replaceWith('<button type="submit" class="btn btn-primary" id="unitSave_btn">Add Unit <i class="fa fa-save"></i></button>'); 
               $("#unitModal").modal("hide");
               switch(results) {
                  case 'success':
                    get_all();
                    toastr.success('Successfully');
                    break;
                  case 'error':
                    toastr.error('There was an error');
                    $("#unitModal").modal("hide");
                    $("#unit_form")[0].reset();  
                    break;
                  default:
                    // code block
                }
          } 

        });  
    });
  // for update
  $('.tableList').on('click', '.update_unit', function(){
     var mode= "updateModal"; 
     var data_id = $(this).prop("id");  
     $.ajax({  
          url:"../controllers/unitsController.php",
          method:"POST",  
          data:{data_id:data_id,mode:mode},  
          success:function(results){
              var jsonObj = JSON.parse(results); 
              console.log(results); 
               // changing modal title
              $(".unitTitle").html("Update Unit");
              $("#unitName").html($.trim(jsonObj.unit_name));
              $("#unitAlias").val($.trim(jsonObj.unit_alias));
              $("#unitDepartment").val($.trim(jsonObj.unit_department)).trigger('change');
              $("#unitHead").val($.trim(jsonObj.unit_head)).trigger('change');
              $("#unitSecretary").val($.trim(jsonObj.unit_secretaries)).change();
              $("#unitNotes").html($.trim(jsonObj.unit_notes));
              $("#unit_data_id").val($.trim(jsonObj.unit_id));
              $("#unitSave_btn").text("Update Unit");
              $("#unitMode").val("update");
              $("#unitModal").modal("show");
          }  
         });  
    });
// for delete
  $('.tableList').on('click', '.del_unit', function(){
     if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
           var mode= "delete"; 
           var data_id = $(this).prop("id");  
           $.ajax({  
                url:"../controllers/unitsController.php",  
                method:"POST",  
                data:{data_id:data_id,mode:mode},  
                success:function(results){
                  switch(results) {
                    case 'success':
                      get_all();
                      toastr.success('Deleted Successfully');
                      break;
                    case 'error':
                      toastr.error('There was an error');
                      break;
                    default:
                      toastr.error('There was an error');
                  }
                }  
               }); 

       }else{
        return false;
      }  
  });
// get all data 
   function get_all(){
      var mode= "getAll";
      $.ajax({  
        url:"../controllers/unitsController.php",  
        method:"POST",  
        data:{mode:mode},  
        success:function(results){
          $('#resultsDisplay').html('');
          var jsonObj = JSON.parse(results);
          for (var i = 0; i < jsonObj.length; i++) {
              $('#resultsDisplay').append('<tr><td>'+jsonObj[i].pages_name+'</td><td>'+jsonObj[i].pages_url+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary update_data" id="'+jsonObj[i].pages_id+'"><i class="fa fa-pencil"></i> UPDATE</button> <button class="btn-danger del_data" id="'+jsonObj[i].pages_id+'"><i class="fa fa-trash"></i> DELETE</button></td></tr>');
          }
        }  
      }); 
   }

   // load staff by department selected
   $("#unitDepartment").change(function(){
       let departmentId = $('option:selected', this).val();
       var mode= "get_department_staffs"; 
           var data_id = $(this).prop("id");  
           $.ajax({  
            url:"../controllers/staffsController.php",  
            method:"POST",  
            data:{departmentId:departmentId,mode:mode},  
            success:function(results){
              // console.log(results);
              switch(results) {
                case 'error':
                  toastr.error('There was an error');
                  break;
                default:
                  $('#unitHead,#unitSecretary').append(results);
                  
              }
            }  
           });
    });
});  