$(document).ready(function(){
      // parsley
       $('#pages_form').parsley();
       
        // for reset modal when close
      $('#pagesModal').on('hidden.bs.modal', function () {
          $(".modal-title").html("Add Pages");
          $("#pages_form")[0].reset();
          $('#pagesSave_btn').replaceWith('<button type="submit" class="btn btn-primary" id="pagesSave_btn">Add Page <i class="fa fa-save"></i></button>'); 
          $("#pagesMode").val("insert");
          $('#pages_form').parsley().reset();
        });

      //for inserting 
        $("#pages_form").submit(function(e){
            e.preventDefault();
            // fields validation
              $.ajax({
              url:"../controllers/pagesController.php",
              method:"POST",
              data:$("#pages_form").serialize(),
              beforeSend:function(){  
                $('#pagesSave_btn').replaceWith('<button disabled class="btn btn-primary" >Loading  <i class="fa fa-spinner" aria-hidden="true"></i></button>'); 
              },
              success:function(results){ 
                  // console.log(results);
                   $('#pagesSave_btn').replaceWith('<button type="submit" class="btn btn-primary" id="pagesSave_btn">Add Page <i class="fa fa-save"></i></button>'); 
                   $("#pagesModal").modal("hide");
                   switch(results) {
                      case 'success':
                        get_all();
                        toastr.success('Saved Successfully');
                        break;
                      case 'error':
                        toastr.error('There was an error');
                        $("#pagesModal").modal("hide");
                        $("#pages_form")[0].reset();  
                        break;
                      default:
                        // code block
                    }
              } 

              });  
          });
      // for update
      $('.tableList').on('click', '.update_data', function(){
         var mode= "updateModal"; 
         var data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/pagesController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                  var jsonObj = JSON.parse(results);  
                   // changing modal title
                  $(".modal-title").html("Update Page Details");
                  $("#pageName").val($.trim(jsonObj[0].pages_name));
                  $("#pageUrl").val($.trim(jsonObj[0].pages_url));
                  $("#pageFileName").val($.trim(jsonObj[0].page_file_name));
                  $("#pages_data_id").val($.trim(jsonObj[0].pages_id));
                  $("#pagesSave_btn").text("UPDATE PAGE");
                  $("#pagesMode").val("update");
                  $("#pagesModal").modal("show");
              }  
             });  
        });
// for delete
        $('.tableList').on('click', '.del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 var mode= "delete"; 
                 var data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/pagesController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        switch(results) {
                          case 'success':
                            get_all();
                            toastr.success('Deleted Successfully');
                            break;
                          case 'error':
                            toastr.error('There was an error');
                            break;
                          default:
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
// get all data 
     function get_all(){
        var mode= "getAll";
        $.ajax({  
          url:"../controllers/pagesController.php",  
          method:"POST",  
          data:{mode:mode},  
          success:function(results){
            $('#resultsDisplay').html('');
            var jsonObj = JSON.parse(results);
            for (var i = 0; i < jsonObj.length; i++) {
                $('#resultsDisplay').append('<tr><td>'+jsonObj[i].pages_name+'</td><td>'+jsonObj[i].pages_url+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary update_data" id="'+jsonObj[i].pages_id+'"><i class="fa fa-pencil"></i> UPDATE</button> <button class="btn-danger del_data" id="'+jsonObj[i].pages_id+'"><i class="fa fa-trash"></i> DELETE</button></td></tr>');
            }
          }  
        }); 
     }
});  