$(document).ready(function(){
  // for reset modal when close
  $('#pagesGroupModal').on('hidden.bs.modal', function () {
      $("#subject").html("Add New Practice");
      $(this).find('input:text').css({"border-color":"#808080"});
      $("#insert_form")[0].reset();
      $("#groupmode").val("insert");
      $("#groupdata_id").val();
      $("#groupsave_btn").text("Save Group");
    });

  //for inserting 
    $("#insert_form").submit(function(e){
        $('.tableList').DataTable().destroy();
        e.preventDefault();
        // fields validation

          $.ajax({
          url:"../controllers/pagesGroupController.php",
          method:"POST",
          data:$("#insert_form").serialize(),
          beforeSend:function(){  
                   $('#groupsave_btn').prop('disabled','disabled').text('Loading...');  
               },
          success:function(results){ 
               $("#pagesGroupModal").modal("hide");
               switch(results) {
                  case 'success':
                    $('.tableList').dataTable({ordering: false,});
                    get_all();
                    toastr.success('Successful');
                    $("#insert_form")[0].reset();
                    $('#groupsave_btn').prop('disabled',false);
                    $('#resultsDisplay').prepend(results);
                    break;
                  case 'error':
                    toastr.error('There was an error');
                    $("#pagesGroupModal").modal("hide");
                    $("#insert_form")[0].reset(); 
                    break;
                  default:
                    // code block
                }
          } 

          });  
      });

      // for update
        $('.tableList').on('click', '.update_data', function(){
             var mode= "updateModal"; 
             var data_id = $(this).prop("id");  
             $.ajax({  
                  url:"../controllers/pagesGroupController.php",
                  method:"POST",  
                  data:{data_id:data_id,mode:mode},  
                  success:function(data){
                    $('.tableList').DataTable().destroy();
                        // passing data from server for particular id selected
                       var jsonObj = JSON.parse(data);
                       // passing the group pages array stored in database
                       var groupPagesArray = JSON.parse(jsonObj[0].pages_id);
                       // console.log(grouppagesArray);
                         //looping through all input id with the checkbox id 
                         var checkbox = $('input[id = "pagesCheckBox"]').each(function(){ 
                                  // grabbing the checkboxes values
                                  var PagesId = $(this).val(); 
                                  // looping througth the array to get the ids
                                  if (groupPagesArray != null) {
                                      for (var i = 0; i < groupPagesArray.length; ++i) {
                                      // for comparing if returned array is contained in the input id's values
                                      if (groupPagesArray[i] == PagesId) {
                                        // select the checkbox if the id's meet
                                            $(this).prop('checked',true);
                                          }
                                      }
                                  }
                                
                               });
                         $('.tableList').dataTable({ordering: false,});
                         // changing modal title
                         // console.log($.trim(jsonObj[0].profile));
                        $("#subject").html("UPDATE GROUP PAGES");
                        $("#groupdata_id").val($.trim(jsonObj[0].pages_group_id));
                        $("#pagesGroupName").val($.trim(jsonObj[0].pages_group_name));
                        $("#pagesGroupProfile").val($.trim(jsonObj[0].profile)).change();
                        $("#groupsave_btn").text("Update Group");
                        $("#groupmode").val("update");
                        $("#pagesGroupModal").modal("show");
                  }  
                 });  
          });

      
// for delete
        $('.tableList').on('click', '.del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
               
                 var mode= "delete"; 
                 var data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/pagesGroupController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        switch(results) {
                          case 'success':
                            get_all();
                            toastr.success('Deleted Successfully');
                            break;
                          case 'error':
                            toastr.error('There was an error');
                            break;
                          default:
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
        // / get all data 
         function get_all(){
            var mode= "getAll";
            $.ajax({  
              url:"../controllers/pagesGroupController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                $('#resultsDisplay').html('');
                var jsonObj = JSON.parse(results);
                for (var i = 0; i < jsonObj.length; i++) {
                    $('#resultsDisplay').append('<tr><td>'+jsonObj[i].pages_group_name+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary update_data" id="'+jsonObj[i].pages_group_id+'"><i class="fa fa-pencil"></i> UPDATE</button> <button class="btn-danger del_data" id="'+jsonObj[i].pages_group_id+'"><i class="fa fa-trash"></i> DELETE</button></td></tr>');
                }
              }  
            }); 
         }

});  