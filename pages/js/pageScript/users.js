$(document).ready(function(){
		 $('#accPasswdReset').bootstrapToggle({
		      on: 'Direct Login',
		      off: 'Reset Pass',
		      onstyle: 'success',
		      offstyle: 'danger'
		    });
	     $('#accPasswdReset').change(function(){
	      if($(this).prop('checked')){
	       $('#accPasswdReset_log').val('login');
	      }else {
	       $('#accPasswdReset_log').val('reset');
	      }
	     });
	     // end of password reset

	     // eccount status
	    $('#accStatus').bootstrapToggle({
	      on: 'Active',
	      off: 'Inactive',
	      onstyle: 'success',
	      offstyle: 'danger'
	    });

	    // triggring the check
	    $('#accStatus').bootstrapToggle('on');

	     $('#accStatus').change(function(){
	      if($(this).prop('checked')){
	       $('#accStatus_log').val('active');
	      }else{
	       $('#accStatus_log').val('inactive');
	      }
	     });
	    // end of account status

		 $('#users_form').parsley();
      	 $('#users_form').parsley().reset();

	 	 $('#usersModal').on('hidden.bs.modal', function () {
	 	  $("#usersmode").val("");
	 	  $("#usersmode").val("insert");
          $(".userTitle").html("Add New Use");
          $("#users_form")[0].reset();
          $("#userssave_btn").replaceWith('<button type="submit" class="btn btn-primary" id="userssave_btn">Add User <i class="fa fa-save"></i></button>');
          $('#users_form').parsley().reset();
          $("#accountSelect").prop('disabled',false);
          $('.userSelect2').val(null).trigger('change');
          $(".accSelectDiv").show();
        });

	 	 $(".userSelect2").select2({
	      dropdownParent: $("#usersModal")
	    });
	 	 // get username for login
		$("#accountSelect").change(function(){
			 let staffUsername = $('option:selected', this).prop("id");
			 // get username and insert into DOM
			 $("#userName").val(staffUsername);
		});


		// for insert
		$("#users_form").submit(function(e){
	        e.preventDefault();
	        $("#accountSelect").prop('disabled',false);
	          $.ajax({
		          url:"../controllers/usersController.php",
		          method:"POST",
		          data:$("#users_form").serialize(),
		          beforeSend:function(){  
	                   // $('#userssave_btn').prop('disabled','disabled').text('Loading...');  
	               },
		          success:function(results){ 
		          	console.log(results);
		               $("#usersModal").modal("hide");

		               switch(results) {
	                      case 'success':
	                        get_all();
		                    toastr.success('Saved Successfully');
		                    $("#users_form").reset();
		                    $("#userssave_btn").replaceWith('<button type="submit" class="btn btn-primary" id="userssave_btn">Add User <i class="fa fa-save"></i></button>');
	                      break;
	                      case 'error':
	                          toastr.error('There was an error');
			                  $("#usersModal").modal("hide");
			                  $("#users_form").reset();
	                      break;
	                      default:
	                      	  toastr.error('There was an error');
	                    }

		          } 

	          	});  
	    });
	     // for update
	       $(document).on('click', '.update_user', function(){
	         let mode= "updateModal"; 
	         let data_id = $(this).prop("id");  
	         $.ajax({  
	              url:"../controllers/usersController.php",
	              method:"POST",  
	              data:{data_id:data_id,mode:mode},  
	              success:function(results){
	                   let jsonObj = JSON.parse(results);  
	                   // changing modal title
	                  	$(".userTitle").html("Update User Account");
	                  	////////////////////////////////////////////////////////////////////////////////////////////
	                    switch($.trim(jsonObj.password_reset)) {
	                      case 'login':
	                        $('#accPasswdReset').bootstrapToggle('on');
	                        break;
	                      case 'reset':
	                        $('#accStatus').bootstrapToggle('off');
	                        break;
	                    }
	                    ///////////////////////////////////////////////////////////////////////////////////////////
	                    // if account status is off
	                    switch($.trim(jsonObj.acc_status)) {
	                      case 'inactive':
	                        $('#accStatus').bootstrapToggle('off');
	                        break;
	                      case 'active':
	                        $('#accStatus').bootstrapToggle('on');
	                        break;
	                    }
	                    //////////////////////////////////////////////////////////////////////////////////////////
						$("#accountSelect").val($.trim(jsonObj.staff_id)).change().prop("disabled",true);
						$("#userName").val($.trim(jsonObj.user_name));
						$("#accGroup").val($.trim(jsonObj.group_id));
	                    $("#usersdata_id").val($.trim(jsonObj.user_id));
	                    // turn account status button
	                    $("#userssave_btn").text("Update User");
	                    $("#usersmode").val("update");
	                    $("#usersModal").modal("show");
	              }  
	             });  
	        });
	      // for delete
	        $(document).on('click', '.del_user', function(){
	           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
	               
	                 let mode= "delete"; 
	                 let data_id = $(this).prop("id");  
	                 $.ajax({  
	                      url:"../controllers/usersController.php",  
	                      method:"POST",  
	                      data:{data_id:data_id,mode:mode},  
	                      success:function(results){
	                      	switch(results) {
		                      case 'success':
		                        get_all();
	                            toastr.success('Deleted Successfully');
		                        break;
		                      case 'error':
		                        toastr.error('There was an error');
		                        break;
		                      default:
		                        toastr.error('There was an error');
		                    }
	                      }  
	                     }); 

	             }else{
	              return false;
	            }  
	        });

	       // get all data 
		     function get_all(){
		        let mode= "getAll";
		        $.ajax({  
		          url:"../controllers/usersController.php",  
		          method:"POST",  
		          data:{mode:mode},  
		          success:function(results){
		            $('#resultsDisplay').html('');
		            if ($results !="") {
		            	$('#userAccountDiv').html(results);
		            }
		          	
		          }  
		        }); 
		     }
});