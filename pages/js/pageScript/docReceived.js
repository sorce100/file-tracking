$(document).ready(function(){
  // initialize dropify
  $('.dropify').dropify();
  // parsley
  $('#docReceived_form').parsley();
   
    // for reset modal when close
  $('#docReceivedModal').on('hidden.bs.modal', function () {
      $(".docReceivedTitle").html("Add New Received Document");
      $("#docReceived_form")[0].reset();
      $('#docReceived_btn').replaceWith('<button type="submit" class="btn btn-primary" id="docReceived_btn">Add Document <i class="fa fa-save"></i></button>'); 
      $("#docReceivedMode").val("insert");
      $('#docReceived_form').parsley().reset();
      $('#unitHead').find('option').not(':first').remove();
      // dropify clear
      $(".dropify-clear").click();

      $("#uploadedFileDiv").html('');

      $("#docReceivedFirstName,#docReceivedLastName,#docReceivedTelNum1").prop('readonly',false);
    });


    // check if document is internal or external, if internal, load details of staff
    $("#docReceivedSource").change(function(){
       var docSource = $('option:selected', this).val();
       switch(docSource) {
          case 'Internal':
             var mode= "updateModal"; 
             var data_id=0;
             $.ajax({  
                  url:"../controllers/staffsController.php",
                  method:"POST",  
                  data:{data_id:data_id,mode:mode},  
                  success:function(results){
                      var jsonObj = JSON.parse(results); 
                      $("#docReceivedFirstName").val($.trim(jsonObj.staff_first_name)).prop('readonly',true);
                      $("#docReceivedLastName").val($.trim(jsonObj.staff_last_name)).prop('readonly',true);
                      $("#docReceivedTelNum1").val($.trim(jsonObj.staff_tel_num)).prop('readonly',true);
                  }  
                 });  

          break;
          case 'External':
            
          break;
          default:
            // code block
        }
    }); 


  //for inserting 
    $("#docReceived_form").submit(function(e){
        e.preventDefault();
        // fields validation
          $.ajax({
          // for progress bar
          xhr:function(){
            let xhr = new XMLHttpRequest();
            xhr.upload.addEventListener('progress',function(e){
              // check if upload length is true or false
              if (e.lengthComputable) {
                let uploadPercent = Math.round((e.loaded/e.total)*100);
                // updating progress bar pecentage
                $('#progressBar').prop('aria-valuemax',uploadPercent).css('width',uploadPercent + '%').text(uploadPercent + '%');
              }
            });
            return xhr;
          },
          url:"../controllers/docReceivedController.php",
          method:"POST",
          enctype: 'multipart/form-data',
          data:new FormData(this),  
          contentType:false,  
          processData:false,
          beforeSend:function(){  
            $('#docReceived_btn').replaceWith('<button disabled class="btn btn-primary" >Loading  <i class="fa fa-spinner" aria-hidden="true"></i></button>'); 
          },
          success:function(results){ 
              // console.log(results);
             $('#docReceived_btn').replaceWith('<button type="submit" class="btn btn-primary" id="docReceived_btn">Add Document <i class="fa fa-save"></i></button>'); 
             $("#docReceivedModal").modal("hide");
             switch(results) {
                case 'success':
                  get_all();
                  toastr.success('Successfully');
                  break;
                case 'error':
                  toastr.error('There was an error');
                  $("#docReceivedModal").modal("hide");
                  $("#docReceived_form")[0].reset();  
                  break;
                default:
                  // code block
              }
          } 

        });  
    });
  // for update
  $('.tableList').on('click', '.update_docReceived', function(){
     let mode= "updateModal"; 
     let data_id = $(this).prop("id");  
     $.ajax({  
          url:"../controllers/docReceivedController.php",
          method:"POST",  
          data:{data_id:data_id,mode:mode},  
          success:function(results){
              let jsonObj = JSON.parse(results); 
              // console.log(results); 
               // changing modal title
              $(".docReceivedTitle").html("Update Unit");
              $("#docReceivedSource").val($.trim(jsonObj.document_received_source)).trigger('change');
              $("#docReceivedFirstName").val($.trim(jsonObj.document_received_from_first_name));
              $("#docReceivedLastName").val($.trim(jsonObj.document_received_from_last_name));
              $("#docReceivedTelNum1").val($.trim(jsonObj.document_received_from_tel_num));
              $("#docReceivedTelNum2").val($.trim(jsonObj.document_received_from_tel_num_1));
              $("#docReceivedEmail").val($.trim(jsonObj.document_received_from_email));
              $("#docReceivedCompany").val($.trim(jsonObj.document_received_from_company)).trigger('change');
              $("#docReceivedType").val($.trim(jsonObj.document_received_type)).trigger('change');
              $("#docReceivedDocCopies").val($.trim(jsonObj.document_received_copies_num));
              $("#docReceivedSubject").val($.trim(jsonObj.document_received_subject));

              // insert file for read if uploaded
              switch($.trim(jsonObj.document_received_file_name)){
                case 'NONE':
                  $("#uploadedFileDiv").html('');
                break;
                default:
                  $("#uploadedFileDiv").html('<u><h4>Uploaded File</h4></u><a " id="../uploads/received_documents/'+$.trim(jsonObj.document_received_folder_name)+'/'+$.trim(jsonObj.document_received_file_name)+'" > <i class="fa-5x fa fa-file-pdf-o"> </i> '+$.trim(jsonObj.document_received_file_name)+'</a>');
              }

              $("#docReceivedNotes").html($.trim(jsonObj.document_received_notes));
              $("#docReceived_data_id").val($.trim(jsonObj.document_received_id));
              $("#docReceivedMode").val("update");
              $("#docReceived_btn").text("Update Document");
              $("#docReceivedModal").modal("show");
          }  
         });  
    });
// for delete
  $('.tableList').on('click', '.del_unit', function(){
     if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
           let mode= "delete"; 
           let data_id = $(this).prop("id");  
           $.ajax({  
                url:"../controllers/docReceivedController.php",  
                method:"POST",  
                data:{data_id:data_id,mode:mode},  
                success:function(results){
                  switch(results) {
                    case 'success':
                      get_all();
                      toastr.success('Deleted Successfully');
                      break;
                    case 'error':
                      toastr.error('There was an error');
                      break;
                    default:
                      toastr.error('There was an error');
                  }
                }  
               }); 

       }else{
        return false;
      }  
  });
// get all data 
   // function get_all(){
   //    let mode= "getAll";
   //    $.ajax({  
   //      url:"../controllers/docReceivedController.php",  
   //      method:"POST",  
   //      data:{mode:mode},  
   //      success:function(results){
   //        $('#resultsDisplay').html('');
   //        let jsonObj = JSON.parse(results);
   //        for (let i = 0; i < jsonObj.length; i++) {
   //            $('#resultsDisplay').append('<tr><td>'+jsonObj[i].pages_name+'</td><td>'+jsonObj[i].pages_url+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary update_data" id="'+jsonObj[i].pages_id+'"><i class="fa fa-pencil"></i> UPDATE</button> <button class="btn-danger del_data" id="'+jsonObj[i].pages_id+'"><i class="fa fa-trash"></i> DELETE</button></td></tr>');
   //        }
   //      }  
   //    }); 
   // }

   // load staff by department selected
   $("#unitDepartment").change(function(){
       let departmentId = $('option:selected', this).val();
       let mode= "get_department_staffs";  
           $.ajax({  
            url:"../controllers/staffsController.php",  
            method:"POST",  
            data:{departmentId:departmentId,mode:mode},  
            success:function(results){
              // console.log(results);
              switch(results) {
                case 'error':
                  toastr.error('There was an error');
                  break;
                default:
                  $('#unitHead,#unitSecretary').append(results);
                  
              }
            }  
           });
    });

   // getting the selected documents to submit or send
   let selectedIdsArray = new Array();
    $('.docReceivedSelectedIds').change(function(){
      if($(this).prop('checked')){
       // added selected id to modal
       selectedIdsArray.push($(this).prop('name'));
      }else {
        // remove from array if unchecked
        let index = $.inArray($(this).prop('name'), selectedIdsArray);
        if (index != -1) {
            selectedIdsArray.splice(index, 1);
        }
      }
      // insert into dom
       $('.sendSelectedDocsBtn').prop('id',JSON.stringify(selectedIdsArray));
       // 
    });
    // ////////////////////////////////////////////////////////////
    // When selected documents button is clicked
    $(document).on('click', '.sendSelectedDocsBtn', function(){
      let mode= "getSelectedDocuments";
      // let mode= "regionalNumberApiRequest";
      let returnedArrays = $(this).prop('id');
      if (returnedArrays === undefined || returnedArrays.length == 0) {
          toastr.error('Sorry!!! Please Select Document to send');
          return false;
      }
      $.ajax({  
          url:"../controllers/docReceivedController.php",  
          method:"POST",  
          data:{mode:mode,returnedArrays:returnedArrays},  
          success:function(results){
            // console.log(results);
            if (results != "") {
              // toastr.success('Successful');
              $('#selectedDocumentsModal').modal('show');
              $('#returnSelectedDocsDiv').html(results);
            }
            else{
              $('#returnSelectedDocsDiv').html('');
              $('#selectedDocumentsModal').modal('hide');
              toastr.error('Sorry, an error occured, Try again');
            }

           
          }  
        });
    });
    // ////////////////////////////////////////////////////////////
    // select category to load its associated lists [department, unit, staff]
    $("#selectDocReceiverCategory").change(function(){
      $('#categorySelectResultsDiv,#receiverSelectStaffResultsDiv').html('');

       let receiverCategory = $('option:selected', this).val();
       switch(receiverCategory) {

          case 'Department':
            var mode = "getAllReceiverDepartments";
            $.ajax({  
              url:"../controllers/departmentsController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                // console.log(results);
                if (results != "") {
                  $('#categorySelectResultsDiv').html(results);
                }
                else{
                  toastr.error('Sorry, an error occured, Try again');
                }

               
              }  
            });
          break;
          case 'Unit':
            var mode = "getAllReceiverUnits";
            $.ajax({  
              url:"../controllers/unitsController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                // console.log(results);
                if (results != "") {
                  $('#categorySelectResultsDiv').html(results);
                }
                else{
                  toastr.error('Sorry, an error occured, Try again');
                }

               
              }  
            });
          break;
          case 'Staff':
            var mode = "get_all_staff";
            $.ajax({  
              url:"../controllers/staffsController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                // console.log(results);
                if (results != "") {
                  $('#receiverSelectStaffResultsDiv').html(results);
                }
                else{
                  toastr.error('Sorry, an error occured, Try again');
                }

               
              }  
            });
          break;
          default:
           toastr.error('There was an error');
            
        }
    });


// get department staff list
  $(document).on('change', '#receiverDepartmentIdSelect', function(){
     $('#receiverSelectStaffResultsDiv').html('');

     let mode= "get_receiver_department_staffs";
     let departmentId = $('option:selected', this).val();
     // console.log(departmentId);
      $.ajax({  
          url:"../controllers/staffsController.php",  
          method:"POST",  
          data:{departmentId:departmentId,mode:mode},  
          success:function(results){
            // console.log(results);
            switch(results) {
              case 'error':
                toastr.error('There was an error');
                break;
              default:
                $('#receiverSelectStaffResultsDiv').html(results);
                
            }
          }  
        });
    });
  // get all units staff list
  $(document).on('change', '#receiverUnitIdSelect', function(){  
    $('#receiverSelectStaffResultsDiv').html('');

     let mode= "get_receiver_unit_staffs";
     let unitId = $('option:selected', this).val();
     // console.log(unitId);
      $.ajax({  
          url:"../controllers/staffsController.php",  
          method:"POST",  
          data:{unitId:unitId,mode:mode},  
          success:function(results){
            // console.log(results);
            switch(results) {
              case 'error':
                toastr.error('There was an error');
                break;
              default:
                $('#receiverSelectStaffResultsDiv').html(results);
                
            }
          }  
        });
    });

// ///////////////////////////////////
// submitting receivers information
    $("#selectDocReceiver_form").submit(function(e){
        e.preventDefault();
        $.ajax({
          url:"../controllers/doctransferController.php",
          method:"POST",
          data:$("#selectDocReceiver_form").serialize(),
          beforeSend:function(){  
            $('#selectedDocsReceiver_btn').prop('disabled','disabled').text('Loading...');  
          },
          success:function(results){ 
            // console.log(results);
           switch(results) {
              case 'success':
                toastr.success('Document Sent Successful');
                $("#selectDocReceiver_form")[0].reset();
                $('#selectedDocsReceiver_btn').replaceWith('<button type="submit" class="btn btn-primary" id="selectedDocsReceiver_btn">Send Documents <i class="fa fa-send"></i></button>');
                $("#selectedDocumentsModal").modal("hide");
                break;
              case 'error':
                $("#selectDocReceiver_form")[0].reset();
                $('#selectedDocsReceiver_btn').replaceWith('<button type="submit" class="btn btn-primary" id="selectedDocsReceiver_btn">Send Documents <i class="fa fa-send"></i></button>');
                toastr.error('There was an error');; 
                break;
              default:
                // code block
            }
          } 

        });  

    });


















});  