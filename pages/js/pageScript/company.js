$(document).ready(function(){
  // parsley
  $('#company_form').parsley();
   
    // for reset modal when close
  $('#companyModal').on('hidden.bs.modal', function () {
      $(".companyTitle").html("Add New Company");
      $("#company_form")[0].reset();
      $('#companySave_btn').replaceWith('<button type="submit" class="btn btn-primary" id="companySave_btn">Add Company <i class="fa fa-save"></i></button>'); 
      $("#companyMode").val("insert");
      $('#company_form').parsley().reset();
      $('#unitHead').find('option').not(':first').remove();
      $('#unitSecretary').find('option').not(':first').remove();
    });

  //for inserting 
    $("#company_form").submit(function(e){
        e.preventDefault();
        // fields validation
          $.ajax({
          url:"../controllers/companyController.php",
          method:"POST",
          data:$("#company_form").serialize(),
          beforeSend:function(){  
            $('#companySave_btn').replaceWith('<button disabled class="btn btn-primary" >Loading  <i class="fa fa-spinner" aria-hidden="true"></i></button>'); 
          },
          success:function(results){ 
              console.log(results);
               $('#companySave_btn').replaceWith('<button type="submit" class="btn btn-primary" id="companySave_btn">Add Company <i class="fa fa-save"></i></button>'); 
               $("#companyModal").modal("hide");
               switch(results) {
                  case 'success':
                    get_all();
                    toastr.success('Successfully');
                    break;
                  case 'error':
                    toastr.error('There was an error');
                    $("#companyModal").modal("hide");
                    $("#company_form")[0].reset();  
                    break;
                  default:
                    // code block
                }
          } 

        });  
    });
  // for update
  $('.tableList').on('click', '.update_company', function(){
     var mode= "updateModal"; 
     var data_id = $(this).prop("id");  
     $.ajax({  
          url:"../controllers/companyController.php",
          method:"POST",  
          data:{data_id:data_id,mode:mode},  
          success:function(results){
              var jsonObj = JSON.parse(results); 
              // console.log(results); 
               // changing modal title
              $(".companyTitle").html("Update Unit");
              $("#companyName").val($.trim(jsonObj.company_name));
              $("#companyTelNo").val($.trim(jsonObj.company_tel_num));
              $("#companyEmail").val($.trim(jsonObj.company_email));
              $("#companyAddress").text($.trim(jsonObj.company_address));
              $("#companyRegion").val($.trim(jsonObj.company_region)).change();
              $("#companyCountry").val($.trim(jsonObj.company_country)).change();
              $("#companyNotes").html($.trim(jsonObj.company_notes));
              $("#company_data_id").val($.trim(jsonObj.company_id));
              $("#companySave_btn").text("Update Company");
              $("#companyMode").val("update");
              $("#companyModal").modal("show");
          }  
         });  
    });
// for delete
  $('.tableList').on('click', '.del_company', function(){
     if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
           var mode= "delete"; 
           var data_id = $(this).prop("id");  
           $.ajax({  
                url:"../controllers/companyController.php",  
                method:"POST",  
                data:{data_id:data_id,mode:mode},  
                success:function(results){
                  switch(results) {
                    case 'success':
                      get_all();
                      toastr.success('Deleted Successfully');
                      break;
                    case 'error':
                      toastr.error('There was an error');
                      break;
                    default:
                      toastr.error('There was an error');
                  }
                }  
               }); 

       }else{
        return false;
      }  
  });

  // for viewing
   $('.tableList').on('click', '.view_company', function(){
     var mode= "updateModal"; 
     var data_id = $(this).prop("id");  
     $.ajax({  
          url:"../controllers/companyController.php",
          method:"POST",  
          data:{data_id:data_id,mode:mode},  
          success:function(results){
              var jsonObj = JSON.parse(results); 
              // console.log(results); 
               // changing modal title
              $(".companyTitle").html("Update Unit");
              $("#companyName").val($.trim(jsonObj.company_name));
              $("#companyTelNo").val($.trim(jsonObj.company_tel_num));
              $("#companyEmail").val($.trim(jsonObj.company_email));
              $("#companyAddress").text($.trim(jsonObj.company_address));
              $("#companyRegion").val($.trim(jsonObj.company_region)).change();
              $("#companyCountry").val($.trim(jsonObj.company_country)).change();
              $("#companyNotes").html($.trim(jsonObj.company_notes));
              $("#company_data_id").val("");
              $("#companySave_btn").replaceWith("<span id='companySave_btn'><span>");
              $("#companyMode").val("");
              $("#companyModal").modal("show");
          }  
         });  
    });

// get all data 
   function get_all(){
      var mode= "getAll";
      $.ajax({  
        url:"../controllers/companyController.php",  
        method:"POST",  
        data:{mode:mode},  
        success:function(results){
          $('#resultsDisplay').html('');
          var jsonObj = JSON.parse(results);
          for (var i = 0; i < jsonObj.length; i++) {
              $('#resultsDisplay').append('<tr><td>'+jsonObj[i].pages_name+'</td><td>'+jsonObj[i].pages_url+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary update_data" id="'+jsonObj[i].pages_id+'"><i class="fa fa-pencil"></i> UPDATE</button> <button class="btn-danger del_data" id="'+jsonObj[i].pages_id+'"><i class="fa fa-trash"></i> DELETE</button></td></tr>');
          }
        }  
      }); 
   }

company_form
});  