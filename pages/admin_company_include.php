<!-- for modal -->
<div class="modal fade" id="companyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title companyTitle">Add New Company</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="company_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Name <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="text" name="companyName" id="companyName" class="form-control" placeholder="Name of Company" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Phone Number</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="number" name="companyTelNo" id="companyTelNo" class="form-control" placeholder="(xxx) xxx xxxx" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Email</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="email" name="companyEmail" id="companyEmail" class="form-control" placeholder="Eg. abc@domain.com" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <!-- 4 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Address</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <textarea class="form-control" id="companyAddress" name="companyAddress" placeholder="Address Or Location of company"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- 5 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Region / Country <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                              <select class="form-control" name="companyRegion" id="companyRegion" required>
                                <?php require_once('../includes/regions.html');?>
                              </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                              <select class="form-control" name="companyCountry" id="companyCountry" required>
                                <?php require_once('../includes/countries.html');?>
                              </select>
                            </div>
                        </div>
                    </div>
                    <!-- 6 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Notes</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <textarea rows="3" class="form-control" id="companyNotes" name="companyNotes" placeholder="Additional Company Informtaion"></textarea>
                            </div>
                        </div>
                    </div>

                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="company_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="companyMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="companySave_btn">Add Company <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="js/pageScript/company.js"></script>
