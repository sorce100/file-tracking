<?php 
	require_once('../includes/header.php');
  include_once('../Classes/Units.php'); 
  include_once('../Classes/Departments.php'); 

 ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-square"></i> Unit Setup Page</h2>
        <!-- add new button -->
        <div class="pull-right"><button class="btn btn-danger" data-toggle="modal" data-target="#unitModal">Add New <i class="fa fa-plus"></i></button></div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
          <div class="table-responsive">
            <table class="table table-striped jambo_table tableList">
              <thead>
                  <tr>
                      <th>Name</th>
                      <th>Alias</th>
                      <th>Department</th>
                      <th>Head</th>
                      <th>Secretary</th>
                      <th>Added</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody id="unitsDisplay">
                <?php
                  $objUnits = new Units;
                  $units = $objUnits->get_units_list(); 
                  if (!empty($units)) {
                    print_r($units);
                  }
                 ?>
              </tbody>
            </table>
          </div>

      </div>
    </div>
  </div>
</div>


<!-- for modal -->
<div class="modal fade" id="unitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title unitTitle">Unit Setup</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="unit_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Unit Name <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <textarea name="unitName" id="unitName" class="form-control" placeholder="Eg. Information Technology &hellip;" autocomplete="off" required></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Unit Alias <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                              <input type="text" name="unitAlias" id="unitAlias" class="form-control" placeholder="Eg. IT &hellip;" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Department <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <select class="form-control unitSelectDepart" id="unitDepartment" name="unitDepartment">
                                 <option value="0">None</option>
                                 <?php
                                  $objDepartments = new Departments;
                                  $departments = $objDepartments->get_departments(); 
                                  foreach ($departments as $department) {
                                    echo '<option value="'.$department["department_id"].'">'.$department["department_name"].'</option>';
                                  }
                                 ?>
                               </select>
                            </div>
                        </div>
                    </div>
                    <!-- 4 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Select head <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <select class="form-control unitSelectHead" id="unitHead" name="unitHead">
                                 <option value="0">None</option>
                               </select>
                            </div>
                        </div>
                    </div>
                    <!-- 5 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Select Secretary <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <select class="form-control unitSelectSecretary" id="unitSecretary" name="unitSecretary">
                                 <option value="0">None</option>
                               </select>
                            </div>
                        </div>
                    </div>
                    <!-- 6 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Unit Notes <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <textarea rows="4" name="unitNotes" id="unitNotes" class="form-control" placeholder="Additional Information" autocomplete="off"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="unit_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="unitMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="unitSave_btn">Add Unit <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php require_once('../includes/footer.php'); ?>
<script src="js/pageScript/unit.js"></script>
