<?php 
	require_once('../includes/header.php');
  include_once('../Classes/Departments.php'); 
 ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-university"></i> Departments Setup Page</h2>
        <!-- add new button -->
        <div class="pull-right"><button class="btn btn-danger" data-toggle="modal" data-target="#departmentModal">Add New <i class="fa fa-plus"></i></button></div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
          <div class="table-responsive">
            <table class="table table-striped jambo_table tableList">
              <thead>
                  <tr>
                      <th>Department Name</th>
                      <th>Head</th>
                      <th>Secretary</th>
                      <th>Added</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody id="resultsDisplay">
                <?php
                  $objDepartments = new Departments;
                  $departments = $objDepartments->get_departments_list(); 
                  if (!empty($departments)) {
                    print_r($departments);
                  }
                 ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- for modal -->
<div class="modal fade" id="departmentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title departmentTitle">Add Department</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="department_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Name <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <textarea name="departmentName" id="departmentName" class="form-control" placeholder="Eg. Main Department &hellip;" autocomplete="off" required></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->

                    <!-- 4 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Select head <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <select class="form-control departmentSelectHead" style="width: 100%;" id="departmentHead" name="departmentHead">
                                 <option value="0">None</option>
                                  <?php
                                    $staffList = $objStaffs->get_staff_options();
                                    if (!empty($staffList)) {
                                      print_r($staffList);
                                    }
                                  ?>
                               </select>
                            </div>
                        </div>
                    </div>
                    <!-- 5 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Select Secretary <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <select class="form-control departmentSelectSecretary" style="width: 100%;" id="departmentSecretary" name="departmentSecretary">
                                 <option value="0">None</option>
                                  <?php
                                    $staffList = $objStaffs->get_staff_options();
                                    if (!empty($staffList)) {
                                      print_r($staffList);
                                    }
                                  ?>
                               </select>
                            </div>
                        </div>
                    </div>
                    <!-- 6 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Department Notes</label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <textarea rows="4" name="departmentNotes" id="departmentNotes" class="form-control" placeholder="Additional Information" autocomplete="off"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="department_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="departmentMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="departmentSave_btn">Add Department <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php require_once('../includes/footer.php'); ?>
<script src="js/pageScript/department.js"></script>
