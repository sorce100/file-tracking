<!-- modal for display records for sending selected documents -->
<div class="modal fade" id="selectedDocumentsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title selectedDocsTitle">Selected Documents</h4>
      </div>
      <div class="modal-body" >
        <form id="selectDocReceiver_form" method="POST">
            <!-- row 1 -->
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <th width="5%">Source</th>
                  <th width="5%">Type</th>
                  <th width="10%">No Of Pages</th>
                  <th width="20%">Subject / Title</th>
                  <th width="14%">Attached Document</th>
                  <th width="18%">Submitter Name</th>
                  <th width="14%">Submitter Company</th>
                  <th width="14%">Date Added</th>
                </thead>
                <tbody id="returnSelectedDocsDiv" class="bg-dark-blue">
                  
                </tbody>
              </table>
            </div>
            <!-- row 2 -->
            <!-- for send to department of unit select -->
            <legend>Select Receiver (s)</legend>
            <div class="row">
                <div class="col-md-2">
                    <label for="title" class="col-form-label"> Select Receiver Category <span class="asterick">*</span></label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                      <select class="form-control" id="selectDocReceiverCategory" name="selectDocReceiverCategory" required>
                        <option selected disabled> Please Select</option>
                        <option value="Department">Department</option>
                        <option value="Unit">Unit</option>
                        <option value="Staff">Staff</option>
                      </select>
                    </div>
                </div>
            </div>
            <!-- category results div -->
            <div class="row" id="categorySelectResultsDiv" ></div>

            <hr>
            <!-- staff display list based on category or unit select -->
            <div class="row" id="receiverSelectStaffResultsDiv" ></div>
            <!--  -->
            <input type="hidden" name="mode" id="receiverSelectStaffMode" value="insert">
            <!--  -->
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
              <button type="submit" class="btn btn-primary" id="selectedDocsReceiver_btn">Send Documents <i class="fa fa-send"></i></button>
             </div>

       </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="js/pageScript/docReceived.js"></script>