<?php 
  require_once('../includes/header.php');
 ?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-university"></i> Staff Department</h2>
        <!-- end new button -->
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <!-- creating tabs -->
          <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#allDepartmentTab" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">All Departement Staff <i class="fa fa-users"></i></a></li>
              <li role="presentation" class=""><a href="#departmentIncomingDocTab" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Department Incoming Documents <i class="fa fa-inbox"></i></a> </li>
              <li role="presentation" class=""><a href="#departmentOutgoingDocTab" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Department Outgoing Documents <i class="fa fa-inbox"></i></a> </li>
            </ul><br>
            <div id="myTabContent" class="tab-content">
              <div role="tabpanel" class="tab-pane fade active in" id="allDepartmentTab" aria-labelledby="home-tab">
               <!--  -->
               <div class="table-responsive">
                  <table class="table table-striped jambo_table tableList">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Telephone Number</th>
                            <th>Email</th>
                            <th>Employee Type</th>
                            <th>Position</th>
                        </tr>
                    </thead>
                    <tbody id="resultsDisplay">
                      <?php
                        $returnList = $objStaffs->get_all_department_staff_associated(); 
                        if (!empty($returnList)) {
                          print_r($returnList);
                        }
                       ?>
                    </tbody>
                  </table>
                </div>
               <!--  -->
              </div>
              <div role="tabpanel" class="tab-pane fade" id="departmentIncomingDocTab" aria-labelledby="profile-tab">
               <!--  -->
                <div class="table-responsive">
                  <table class="table table-striped jambo_table tableList">
                    <thead>
                       <tr>
                            <th>Date Added</th>
                            <th>Title</th>
                            <th>Source</th>
                            <th>Type</th>
                            <th>Sender Name</th>
                            <th>Sender Department</th>
                            <th>Sender Unit</th>
                            <th>Received Date</th>
                            <th>Receiver Name</th>
                        </tr>
                    </thead>
                    <tbody id="allDepartmentIncomingDoc">
                      <?php
                        $docDetails = $objDocTransfer->get_all_department_incoming_documents(); 
                        if (!empty($docDetails)) {
                            print_r($docDetails);
                        }
                       ?>
                    </tbody>
                  </table>
                </div>
               <!--  -->
              </div>
              <div role="tabpanel" class="tab-pane fade" id="departmentOutgoingDocTab" aria-labelledby="profile-tab">
               <!--  -->
                <div class="table-responsive">
                  <table class="table table-striped jambo_table tableList">
                    <thead>
                        <tr>
                            <th>Date Added</th>
                            <th>Document Title</th>
                            <th>Source</th>
                            <th>Type</th>
                            <th>Sender Name</th>
                            <th>Received Category</th>
                            <th>Received Date</th>
                            <th>Receiver Name</th>
                        </tr>
                    </thead>
                    <tbody id="allDepartmentOutgoingDoc">
                      <?php
                        $docDetails = $objDocTransfer->get_all_department_outgoing_documents(); 
                        if (!empty($docDetails)) {
                            print_r($docDetails);
                        }
                       ?>
                    </tbody>
                  </table>
                </div>
               <!--  -->
              </div>
            </div>
          </div>
          <!-- end of tabs -->
      </div>
    </div>
  </div>
</div>

<?php require_once('../includes/footer.php'); ?>