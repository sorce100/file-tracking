<?php 
  require_once('../includes/header.php');
  include_once('../Classes/Units.php'); 
 ?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-home"></i> Staff Unit</h2>
        <!-- end new button -->
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <!-- creating tabs -->
          <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#allUnitTab" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">All Unit Staff <i class="fa fa-users"></i></a></li>
              <li role="presentation" class=""><a href="#unitIncomingDocTab" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Unit Incoming Document <i class="fa fa-inbox"></i></a></li>
              <li role="presentation" class=""><a href="#unitOutgoingDocTab" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Unit Outgoing Documents <i class="fa fa-send"></i></a></li>
            </ul><br>
            <div id="myTabContent" class="tab-content">
              <div role="tabpanel" class="tab-pane fade active in" id="allUnitTab" aria-labelledby="home-tab">
               <!-- all unit staff -->
               <div class="table-responsive">
                  <table class="table table-striped jambo_table tableList">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Telephone Number</th>
                            <th>Email</th>
                            <th>Employee Type</th>
                            <th>Position</th>
                        </tr>
                    </thead>
                    <tbody id="allUnitStaff">
                      <?php
                        $returnList = $objStaffs->get_all_unit_staff_associated(); 
                        if (!empty($returnList)) {
                          print_r($returnList);
                        }
                       ?>
                    </tbody>
                  </table>
                </div>
               <!--  -->
              </div>
              <div role="tabpanel" class="tab-pane fade" id="unitIncomingDocTab" aria-labelledby="profile-tab">
               <!--  -->
                <div class="table-responsive">
                  <table class="table table-striped jambo_table tableList">
                    <thead>
                        <tr>
                            <th>Date Added</th>
                            <th>Title</th>
                            <th>Source</th>
                            <th>Type</th>
                            <th>Sender Name</th>
                            <th>Sender Department</th>
                            <th>Sender Unit</th>
                            <th>Received Date</th>
                            <th>Receiver Name</th>
                        </tr>
                    </thead>
                    <tbody id="allUnitIncomingDoc">
                      <?php
                        $docDetails = $objDocTransfer->get_all_units_incoming_documents(); 
                        if (!empty($docDetails)) {
                            print_r($docDetails);
                        }
                       ?>
                    </tbody>
                  </table>
                </div>
               <!--  -->
              </div>
              <div role="tabpanel" class="tab-pane fade" id="unitOutgoingDocTab" aria-labelledby="profile-tab">
               <!--  -->
               <div class="table-responsive">
                  <table class="table table-striped jambo_table tableList">
                    <thead>
                        <tr>
                            <th>Date Added</th>
                            <th>Title</th>
                            <th>Source</th>
                            <th>Type</th>
                            <th>Sender Name</th>
                            <th>Received Category</th>
                            <th>Received Date</th>
                            <th>Receiver Name</th>
                        </tr>
                    </thead>
                    <tbody id="allUnitOutgoingDoc">
                      <?php
                        $docDetails = $objDocTransfer->get_all_units_outgoing_documents(); 
                        if (!empty($docDetails)) {
                            print_r($docDetails);
                        }
                       ?>
                    </tbody>
                  </table>
                </div>
               <!--  -->
              </div>
            </div>
          </div>
          <!-- end of tabs -->
      </div>
    </div>
  </div>
</div>

<?php require_once('../includes/footer.php'); ?>