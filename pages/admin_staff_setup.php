<?php 
	require_once('../includes/header.php'); 
  include_once('../Classes/Units.php'); 
  include_once('../Classes/Departments.php'); 

 ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-users"></i> Staff Page</h2>
        <!-- add new button -->
        <div class="pull-right"><button class="btn btn-danger" data-toggle="modal" data-target="#staffModal">Add New <i class="fa fa-plus"></i></button></div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
          <div class="table-responsive">
            <table class="table table-striped jambo_table tableList">
              <thead>
                  <tr>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Phone Num</th>
                      <th>Email</th>
                      <th>Unit</th>
                      <th>Department</th>
                      <th>Added</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody id="resultsDisplay">
                <?php
                  $staffList = $objStaffs->get_staffs_list(); 
                  if (!empty($staffList)) {
                    print_r($staffList);
                  }
                  
                 ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- for modal -->
<div class="modal fade" id="staffModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title staffTitle">Add Staff</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="staff_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Name <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                               <input type="text" name="staffFirstName" id="staffFirstName" class="form-control" placeholder="Enter First Name &hellip;" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                               <input type="text" name="staffLastName" id="staffLastName" class="form-control" placeholder="Enter Last Name &hellip;" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Phone Number <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="text" name="staffTelNo" id="staffTelNo" class="form-control" placeholder="(XXX) XXX XXXX" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Email <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="text" name="staffEmail" id="staffEmail" class="form-control" placeholder="(XXX) XXX XXXX" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!-- 4 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Employee Number</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="text" name="staffEmployeeNum" id="staffEmployeeNum" class="form-control" placeholder="Employee Numbers &hellip;" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <!-- 5 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Employee Type <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                              <select class="form-control" id="staffEmployeeType" name="staffEmployeeType" required>
                                <option selected disabled>Please Select</option>
                                <option value="Staff">Staff</option>
                                <option value="Nsp">National Service Personnel</option>
                                <option value="Internship">Internship / Attachment</option>
                              </select>
                            </div>
                        </div>
                    </div>
                    <!-- 6 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Department / Unit <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                              <select class="form-control" id="staffDepartmentId" name="staffDepartmentId" required>
                                <option selected disabled>Select Department</option>
                                <?php
                                  $objDepartments = new Departments;
                                  $departments = $objDepartments->get_departments(); 
                                  foreach ($departments as $department) {
                                    echo '<option value="'.$department["department_id"].'">'.$department["department_name"].'</option>';
                                  }
                                 ?>
                              </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                              <select class="form-control" id="staffUnitId" name="staffUnitId" required>
                                <option selected disabled>Select Unit</option>
                               
                              </select>
                            </div>
                        </div>
                    </div>
                    <!-- 7 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Staff Notes <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <textarea rows="4" name="staffNotes" id="staffNotes" class="form-control" placeholder="Additional Information" autocomplete="off"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="staff_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="staffMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="staffSave_btn">Add Staff <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php require_once('../includes/footer.php'); ?>
<script src="js/pageScript/staff.js"></script>
