<div class="modal fade" id="docTransferHistoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title docTransferHistoryTitle">Document Transfer History</h4>
      </div>
      <div class="modal-body" id="docTransferHistoryMainDiv">
        <!-- /////////////////////////////////////////////////////////////////// -->
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#docDetailsTab">Document Details <i class="fa fa-list"></i></a></li>
          <li><a data-toggle="tab" href="#minutesTab">Minutes <i class="fa fa-pencil"></i></a></li>
          <li><a data-toggle="tab" href="#transferLogTab">Document Transfer Log <i class="fa fa-hourglass"></i></a></li>
        </ul><br>

        <div class="tab-content">
          <!-- details tab -->
          <div id="docDetailsTab" class="tab-pane fade in active">
            <!--  -->
            <div class="row">
              <!-- first column -->
              <div class="col-md-6 .border-right">
                <h4><center><b>Document General Info</b></center> </h4>
                <div class="table-responsive">
                    <table class="table table-striped ">
                      <tbody id="documentDetailsResultsDiv">
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-file"></i> Document Source</th>
                          <th id="documentSourceDiv"></th>
                        </tr>
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-exchange"></i> Document Type</th>
                          <th id="documentTypeDiv"></th>
                        </tr>
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-pencil"></i> Document Subject</th>
                          <th id="documentSubjectDiv"></th>
                        </tr>
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-file"></i> Document Pages</th>
                          <th id="documentPagesDiv"></th>
                        </tr>
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-link"></i> Document Attached</th>
                          <th id="documentAttachedDiv"></th>
                        </tr>
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-user"></i> Submitter Name</th>
                          <th id="submitterNameDiv"></th>
                        </tr>
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-university"></i> SubmitterCompany</th>
                          <th id="submitterCompanyDiv"></th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
              <!-- second column -->
              <div class="col-md-6">
                <!-- //////////////////////////////////////////////////////////////////////////////////////////// -->
                  <!-- For calculating the number of days it has lasted in the system -->
                 <!--  <div class="well bg-dark-blue" >
                    <span><strong>Number Of Days in The System : </strong></span> <span id="daysAgoDiv"></span>
                  </div>

                  <hr> -->
                <!-- //////////////////////////////////////////////////////////////////////////////////////////// -->

                <h4><center><b>Document Uploader Details</b></center> </h4>
                <div class="table-responsive">
                    <table class="table table-striped ">
                      <tbody id="documentDetailsResultsDiv">
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-calendar"></i> Date Added</th>
                          <th id="dateAddedDiv"></th>
                        </tr>
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-calendar"></i> Last Updated </th>
                          <th id="lastUpdatedDiv"></th>
                        </tr>
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-user"></i> Staff Name</th>
                          <th id="addedStaffNameDiv"></th>
                        </tr>
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-home"></i> Staff Unit</th>
                          <th id="addedStaffUnitDiv"></th>
                        </tr>
                        <tr>
                          <th class="bg-dark-blue" width="30%"><i class="fa fa-university"></i> Staff Department</th>
                          <th id="addedStaffDepartmentDiv"></th>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                  
              </div>
            </div>
            <!--  -->
          </div>
          <!-- minuts tab -->
          <div id="minutesTab" class="tab-pane fade">
            <!--  -->
            <div class="row">
              <div class="col-md-12">
                <ul class="list-unstyled timeline" id="docMinutesDiv">
                  
                </ul>
              </div>
            </div>
            <!--  -->
          </div>
          <!-- transfer log -->
          <div id="transferLogTab" class="tab-pane fade">
              <!--  -->
                <div class="table-responsive">
                    <table class="table table-striped jambo_table">
                      <thead>
                          <tr>
                              <th>Sender Name</th>
                              <th>Sender Unit</th>
                              <th>Sender Department</th>
                              <th>Date Sent</th>
                              <th>Receiver Category</th>
                              <th>Receiver Category Sub</th>
                              <th>Receiver Name</th>
                              <th>Date Accepted</th>
                          </tr>
                      </thead>
                      <tbody id="transferLogDiv">
                        
                      </tbody>
                    </table>
                  </div>
            
            <!--  -->
          </div>
        </div>
        <!-- /////////////////////////////////////////////////////////////////// -->
      </div>
      <!--  -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="js/pageScript/transferHistory.js"></script>

