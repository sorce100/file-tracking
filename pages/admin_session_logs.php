<?php 
  require_once('../includes/header.php');
	require_once('../Classes/SessionLogs.php');
 ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-bars"></i> Session Logs Page</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="table-responsive">
            <table class="table table-striped jambo_table tableList">
              <thead>
                  <tr>
                      <th>User Account</th>
                      <th>Session Start</th>
                      <th>Session End</th>
                  </tr>
              </thead>
              <tbody>
                <?php
                  $objSessionLogs = new SessionLogs;
                  $logs = $objSessionLogs->get_session(); 
                  foreach ($logs as $log) {
                    echo '
                        <tr>
                          <td>'.trim($log["user_name"]).'</td>
                          <td>'.trim($log["session_start"]).'</td>
                          <td>'.trim($log["session_end"]).'</td>
                        </tr>';
                  }
                 ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>

<?php require_once('../includes/footer.php'); ?>