<?php 
	require_once('../includes/header.php');
  include_once('../Classes/Company.php'); 
  include_once('../Classes/DocReceived.php'); 
 ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-files-o"></i> Documents Received Page</h2>
        <!-- add new button -->
        <div class="pull-right"><button class="btn btn-danger" data-toggle="modal" data-target="#docReceivedModal"  data-backdrop="static" data-keyboard="false">Add New Document <i class="fa fa-plus"></i></button></div>
        <!-- end new button -->
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <!-- creating tabs -->
          <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#documentsReceived" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Documents Received <i class="fa fa-files-o"></i></a></li>
              <li role="presentation" class=""><a href="#companySetup" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Company Setup <i class="fa fa-home"></i></a></li>
            </ul><br>
            <div id="myTabContent" class="tab-content">
              <div role="tabpanel" class="tab-pane fade active in" id="documentsReceived" aria-labelledby="home-tab">
               <!-- <hr> -->
                  <div class="table-responsive">
                    <table class="table table-striped jambo_table tableList">
                      <thead>
                          <tr>
                              <th></th>
                              <th>Source</th>
                              <th>Submitter Name</th>
                              <th>Phone Number</th>
                              <th>Company</th>
                              <th>Doc Type</th>
                              <th>Doc Subject / Title</th>
                              <th>Doc Pages</th>
                              <th>Added</th>
                              <th></th>

                          </tr>
                      </thead>
                      <tbody id="companyDisplay">
                        <?php
                            $objDocReceived = new DocReceived;
                            $details = $objDocReceived->get_added_received_docs_list(); 
                            if ($details) {
                                print_r($details);
                            }
                           ?>
                      </tbody>
                      <tfoot>
                        <tr>
                            <td colspan="10"><button type="button" class="btn bg-green sendSelectedDocsBtn" id="">Send Selected Documents <i class="fa fa-send"></i></button></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                <!--  -->
              </div>
              <div role="tabpanel" class="tab-pane fade" id="companySetup" aria-labelledby="profile-tab">
                <!-- for tab 2 -->
                <div class="row">
                  <div class="pull-right">
                    <button class="btn btn-danger" data-toggle="modal" data-target="#companyModal">Add Company <i class="fa fa-plus"></i></button>
                  </div>
                </div>
                <!-- <hr> -->
                  <div class="table-responsive">
                    <table class="table table-striped jambo_table tableList">
                      <thead>
                          <tr>
                              <th>Name</th>
                              <th>Phone Number</th>
                              <th>Region</th>
                              <th>Country</th>
                              <th>NO Of Letters</th>
                              <th>Added</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody id="companyDisplay">
                        <?php
                            $objCompany = new Company;
                            $details = $objCompany->get_companys(); 
                            if ($details) {
                              print_r($details);
                            }
                           ?>
                      </tbody>
                    </table>
                  </div>
                <!--  -->
              </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>


<!-- for modal -->
<div class="modal fade" id="docReceivedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h3 class="modal-title docReceivedTitle">Add New Received Document</h3>
      </div>
      <div class="modal-body" id="bg">
          <form id="docReceived_form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                  <!-- select source of document -->
                    <div class="row">
                      <div class="col-md-2">
                          <label for="title" class="col-form-label"> Document Source <span class="asterick"> *</span></label>
                      </div>
                      <div class="col-md-10">
                        <div class="form-group">
                           <select class="form-control" id="docReceivedSource" name="docReceivedSource" required>
                             <option selected disabled>Please Select</option>
                             <option value="Internal">Internal</option>
                             <option value="External">External</option>>
                           </select>
                        </div>
                      </div>
                    </div>
                  <!-- Documents received from -->
                  <fieldset>
                    <legend><b>Submitter Details</b></legend>
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label"> Name <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                               <input type="text" name="docReceivedFirstName" id="docReceivedFirstName" class="form-control" placeholder="Enter First Name" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                               <input type="text" name="docReceivedLastName" id="docReceivedLastName" class="form-control" placeholder="Enter Last Name" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label"> Phone Number <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                               <input type="number" name="docReceivedTelNum1" id="docReceivedTelNum1" class="form-control" placeholder="Phone number 1" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                               <input type="number" name="docReceivedTelNum2" id="docReceivedTelNum2" class="form-control" placeholder="Phone number 2" autocomplete="off" >
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label"> Email</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="email" name="docReceivedEmail" id="docReceivedEmail" class="form-control" placeholder="Eg. abc@domain.com" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Select Company <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <select class="form-control" id="docReceivedCompany" name="docReceivedCompany" required>
                                 <option selected disabled>Select Company</option>
                                 <?php
                                    $objCompany = new Company;
                                    $details = $objCompany->get_companys_list(); 
                                    if ($details) {
                                      print_r($details);
                                    }
                                   ?>
                               </select>
                            </div>
                        </div>
                        <!-- <div class="col-md-2">
                            <a data-toggle="modal" data-target="#companyModal" class="bg-green"><b> Add New Company</b></a>
                        </div> -->
                    </div>
                    
                    </fieldset>
                    <!-- second fieldset -->
                    <fieldset>
                      <legend><b>Document Details</b></legend>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label"> Document Type <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                             <select class="form-control" id="docReceivedType" name="docReceivedType" required>
                               <option selected disabled>Please Select</option>
                               <option value="LETTER">LETTER</option>
                               <option value="MEMO">MEMO</option>
                               <option value="PUBLICATION">PUBLICATION</option>
                               <option value="OTHER">OTHER</option>
                             </select>
                          </div>
                        </div>
                        <!-- for number of copies -->
                        <div class="col-md-2">
                          <label for="title" class="col-form-label"> Number Of Pages <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-3">
                          <input type="number" name="docReceivedDocCopies" id="docReceivedDocCopies" class="form-control" placeholder="Number Of Pages" autocomplete="off" >
                        </div>
                      </div>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label"> Document Subject <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-10">
                          <div class="form-group">
                             <textarea rows="3" class="form-control" id="docReceivedSubject" name="docReceivedSubject" placeholder="Eg. Enter subject / Title of document" required></textarea>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Add Scanned Copy  <span class="asterick">(pdf Only)</span> </label>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                              <input type="file" name="file" id="docFile" class="dropify" data-allowed-file-extensions="pdf" data-max-file-size="10M">
                          </div>
                        </div>
                        <!-- display Images -->
                        <div class="col-md-4" id="uploadedFileDiv"></div>
                      </div>
                    </fieldset>

                    <hr>

                    <div class="row">
                      <div class="col-md-2">
                          <label for="title" class="col-form-label"> Additional Details</label>
                      </div>
                      <div class="col-md-10">
                        <div class="form-group">
                           <textarea rows="5" class="form-control" id="docReceivedNotes" name="docReceivedNotes" placeholder="Any additional Document details &hellip;"></textarea>
                        </div>
                      </div>
                    </div>

                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="docReceived_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="docReceivedMode" value="insert">
                    <!-- for progress bar -->
                    <!-- progress bar -->
                    <div class="progress">
                      <div id="progressBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">0%</div>
                    </div>

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="docReceived_btn">Add Document <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- document history tab -->





<!-- add company modal -->
<?php require_once('../includes/footer.php'); ?>
<!-- company setup include -->
<?php include_once('admin_company_include.php'); ?>
<!-- including modal for selecting documents and receivers -->
<?php include_once('document_selected_transfer_include.php'); ?>
<!-- documents history and details modal include -->
<?php include_once('documents_transfer_history_include.php'); ?>
<!-- pdf viewer include -->
<?php include_once('pdfViewer.php'); ?>

