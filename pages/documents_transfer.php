<?php 
	require_once('../includes/header.php');
 ?>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-exchange"> Document Transfer </i></h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="col-md-2 leftcol-md2">
        <!-- required for floating -->
        <!-- Nav tabs -->
        <ul class="nav nav-tabs tabs-left">
          <li class="active"><a href="#incomingTab" data-toggle="tab"><i class="fa fa-inbox"></i> INCOMING DOCUMENTS <span class="pull-right badge bg-green">
            <?php 
              echo $objDocTransfer->count_get_doc_transfer_received_by_staff();
             ?>
          </span></a></li>
          <li><a href="#outgoingTab" data-toggle="tab"><i class="fa fa-send"></i> OUTGOING DOCUMENTS <span class="pull-right badge bg-red">
            <?php 
              echo $objDocTransfer->count_get_doc_transfer_by_staff();
             ?>
          </span></a></li>
        </ul>
      </div>

      <div class="col-md-10">
        <!-- Tab panes -->
        <div class="tab-content">
          <div id="incomingTab" class="tab-pane fade in active">
           <!--  -->
            <div class="x_panel">
              <div class="x_title bg-green">
                <h2><i class="fa fa-inbox"></i> INCOMING DOCUMENTS</h2>
                <!-- end new button -->
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <div class="table-responsive">
                    <table class="table table-striped jambo_table tableList">
                      <thead>
                          <tr>
                              <th>Select</th>
                              <th>Receiver Category</th>
                              <th>Date Received</th>
                              <th>Source</th>
                              <th>Type</th>
                              <th>Subject</th>
                              <th>Sender</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody id="receivedTransferDocs">
                        <?php
                          $details = $objDocTransfer->get_doc_transfer_received_by_staff(); 
                          if (!empty($details)) {
                            foreach ($details as $detail) {
                              print_r($detail);
                            }
                          }
                         ?>
                      </tbody>
                      <tfoot>
                        <tr>
                            <td colspan="8"><button type="button" class="btn bg-green sendSelectedDocsBtn" id="">Forward Selected Document <i class="fa fa-send"></i></button></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
              </div>
            </div>
           <!--  -->
          </div>
          <div id="outgoingTab" class="tab-pane fade">
            <!--  -->
            <div class="x_panel">
              <div class="x_title bg-red">
                <h2><i class="fa fa-send"></i> OUTGONE DOCUMENTS</h2>
                <!-- end new button -->
                <div class="clearfix"></div>

              </div>
              <div class="x_content">
                  <div class="table-responsive">
                    <table class="table table-striped jambo_table tableList">
                      <thead>
                          <tr>
                              <th>Date Received</th>
                              <th>Source</th>
                              <th>Type</th>
                              <th>Subject</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody id="sentTransferDocs">
                        <?php
                          $details = $objDocTransfer->get_doc_transfer_by_staff(); 
                          if (!empty($details)) {
                            foreach ($details as $detail) {
                              print_r($detail);
                            }
                          }

                         ?>
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
            <!--  -->
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- minutes added display log -->
<div class="modal fade" id="minuteLogsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title">Document Minutes Log</h4>
      </div>
      <div class="modal-body" id="bg">
          <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <button data-toggle="modal" data-target="#addMinuteModal" class="btn bg-dark-blue"  data-backdrop="static" data-keyboard="false">ADD NEW MINUTE <i class="fa fa-plus"></i></button>
                        <!-- end new button -->
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                          <div class="row">
                              <div class="col-md-12" id="viewAllMinutesDiv"></div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>

                 <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                 </div>
              </div>
          </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- add minutes Modal -->
<div class="modal fade" id="addMinuteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title addMinuteTitle">Add New Document Minute</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="addMinute_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                      <div class="col-md-3">
                        <label for="title" class="col-form-label">Lock Minute </label>
                      </div>
                      <div class="col-md-9">
                          <div class="form-group">
                            <input type="checkbox" id="addMinuteLock" data-width="150" checked />
                            <input type="hidden" name="addMinuteLock_log" id="addMinuteLock_log" value="OPEN"/>
                          </div>
                      </div>
                    </div>
                    <hr>
                    <!-- 2 -->
                    <div class="row">
                      <div class="col-md-3">
                        <label for="title" class="col-form-label"> Minute Details <span class="asterick"> *</span></label>
                      </div>
                      <div class="col-md-9">
                          <div class="form-group">
                            <textarea rows="8" name="addMinuteDetail" id="addMinuteDetail" class="form-control" placeholder="Add Minute Details" required autocomplete="off"></textarea>
                          </div>
                      </div>
                    </div>
                    <!-- for inserting document transfer id -->
                    <input type="hidden" name="documentReceivedId" id="documentReceivedId" value="">

                    <!-- for inserting the minute Id id -->
                    <input type="hidden" name="data_id" id="addMinuteModal_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="addMinuteModalMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="addMinuteModal_btn">Save Minute<i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="clearfix"></div>

<?php require_once('../includes/footer.php'); ?>
<!-- documents history and details modal include -->
<?php include_once('documents_transfer_history_include.php'); ?>
<!-- including modal for selecting documents and receivers -->
<?php include_once('document_selected_transfer_include.php'); ?>
<!-- for document transfer js -->
<script src="js/pageScript/docTransfer.js"></script>
<!-- for document add minute js -->
<script src="js/pageScript/docMinute.js"></script>
<!-- pdf viewer include -->
<?php include_once('pdfViewer.php'); ?>
