</div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © <?php echo date('Y');?> All Rights Reserved. <a href="dashboard.php" target="_blank">MINISTRY OF LANDS AND NATURAL RESOURCES</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- dropify -->
    <script src="../vendors/dropify/js/dropify.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <!-- bootstrap checkbox toggle -->
    <script src="../vendors/bootstrap-toggle/bootstrap2-toggle.min.js"></script>
    <!-- include datatable -->
    <script src="../vendors/datatables/dataTables.min.js"></script>
    <!-- incude select2 -->
    <script src="../vendors/select2/select2.min.js"></script>
    <!-- add parsley -->
    <script src="../vendors/parsley/parsley.min.js"></script>
    <!-- toastr -->
    <script src="../vendors/toastr/toastr.min.js"></script>
    <!-- date  -->
    <script src="../vendors/datetimepicker/datetimepicker.min.js"></script>
    <!-- fullcalendar -->
    <script src="../vendors/fullcalendar/lib/moment.min.js"></script>
    <script src="../vendors/fullcalendar/fullcalendar.min.js"></script>
    
    <!-- dashboard footer -->
    <!-- jQuery Sparklines -->
    <script src="../vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
  </body>
</html>

<script>
    $(document).ready(function(){
        $('.tableList').dataTable({ ordering: false});
        $('.dataTables_filter input[type="search"]').prop('placeholder','....').css({'width':'350px','height':'40px','display':'inline-block'});
        // date
        // for date time picker
        $(function() {
            $('[data-toggle="datepicker"]').datetimepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                minView: 2
            });

          });

    // <!-- changing password script -->
 
            $('#change_password_form').parsley();
            // for reset modal when close
            $('#changePassModal').on('hidden.bs.modal', function () {
                $('#change_password_form').parsley().reset();
                $("#change_password_form")[0].reset();

                $('#changePass_btn').text("Change Password").prop('disabled',false);  
            });

            // changing password
            $("#change_password_form").on("submit",function(e){
                e.preventDefault();
                $.ajax({
                url:"../controllers/usersController.php",
                method:"POST",
                data:$("#change_password_form").serialize(),
                beforeSend:function(){  
                    $('#changePass_btn').text("Please wait ...").prop('disabled',true);  
                 },
                success:function(data){  
                  // console.log(data);
                  $("#changePassModal").modal("hide");
                   $("#change_password_form")[0].reset();
                   if (data == "success") {
                    toastr.success('Password Changed Successfully');
                    // $.ajax({
                    //     url:"Script/log_out.php",
                    // });
                   window.location.href = "../controllers/logOut.php";
                   }
                   else if(data == "error"){
                    toastr.error('Sorry, There was a problem changing your password!');
                   }
                } 

                });  
            });
        });
    </script>
    <!-- //////////////////////////////////////////////////////////////// -->