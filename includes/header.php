<?php 
 
  require_once("Check.php");
  $objCheck = new Check();

  include_once('../Classes/Staffs.php'); 
  $objStaffs = new Staffs;
  // if ($_SESSION['account_type'] == "member") {
  //   $currentBalance = $objMembers->get_member_current_balance();
  // }
  include_once('../Classes/DocTransfer.php'); 
  $objDocTransfer = new DocTransfer;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MLNR | FILE TRACKING APPLICATION</title>
    <link rel="icon" type="image/jpg" sizes="96x96" href="../pages/images/logo.jpg">
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- add select2  -->
    <link href="../vendors/select2/select2.min.css" rel="stylesheet">
    <!-- add parsley --> 
    <link href="../vendors/parsley/parsley.css" rel="stylesheet">
    <!-- add bootstrap checkbox toggle --> 
    <link href="../vendors/bootstrap-toggle/bootstrap-toggle.min.css" rel="stylesheet">
     <!-- toastr -->
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet">
    <!-- date picker -->
    <link href="../vendors/datetimepicker/datetimepicker.min.css" rel="stylesheet">
    <!-- full calendar -->
    <link href="../vendors/fullcalendar/fullcalendar.min.css" rel="stylesheet">
    <!-- dropify -->
    <link href="../vendors/dropify/css/dropify.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
    <!-- ckeditor -->
    <script src="../vendors/ckeditor/ckeditor.js"></script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <!-- <a href="index.html" class="site_title"><i class="fa fa-globe"></i> <span>LISAG</span></a> -->
              <a href="dashboard.php"><img src="../pages/images/logo.jpg" class="img img-responsive img-circle header-logo" width="40%" height="40%"></a>
            </div>
            <div class="clearfix"></div>
            <br>
            <hr/>
            <!-- menu profile quick info -->
            <?php
              switch ($_SESSION['account_type']) {
                case 'staff':
                   echo '
                      <div class="profile clearfix">
                      <div class="profile_pic">
                        <img src="../pages/images/user.png" alt="..." class="img-circle profile_img">
                      </div>
                      <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>'.$_SESSION["fullname"].'</h2>
                        
                      </div>
                      <div class="clearfix"></div>
                    </div>';
                  break;
                 
                 default:
                   # code...
                   break;
               } 
              
            ?>
            <!-- /menu profile quick info -->

            
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3>General</h3> -->
                <hr>
                <ul class="nav side-menu">
                  <!-- menu links -->
                  <li class="menuLink"><a href="#" data-toggle="modal" data-target="#gettingStartedModal"><i class="fa fa-play"></i> Getting Started </a></li>
                  <!-- workin on pages -->
                  

                  <!-- ////////////////////////////////////////////////////////////////////////////////////////////////// -->
                    <?php 
                      switch ($_SESSION['account_type']) {
                        case 'staff':
                          echo '<li class="menuLink"><a href="../pages/dashboard.php"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>';
                          // <!-- get Pages per the group given -->
                          // getting the group id from session then getting the pages id from the database
                            require_once("../Classes/PagesGroup.php");
                            $objPagesGroup = new PagesGroup;
                            $pagesId = $objPagesGroup->menu_pages_id($_SESSION['group_id']);
                            if (!empty($pagesId)) {
                                // passing the pages id to  get the pages url
                                require_once("../Classes/Pages.php");
                              $objPages = new Pages;
                              foreach ($pagesId as $page_id) {
                                 $objPages->get_menu_pages($page_id);
                                }
                            }
                          break;
                        default:
                          session_destroy();
                          header("Location: ../../index.php");
                          break;
                      }
                    ?>
                  <!-- ////////////////////////////////////////////////////////////////////////////////////////////////// -->

                  
                  <!-- end menu links -->
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="">
                <span class="glyphicon glyphicon" aria-hidden="true"></span>
              </a>
              <a>
                <span class="glyphicon glyphicon" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="">
                <span class="glyphicon glyphicon" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="../controllers/logOut.php">
                <span class="asterick glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"  data-toggle="tooltip" data-placement="down" title="Show/Hide Menu"><i class="fa fa-bars"></i></a>
              </div>
              <div class="navbar-form navbar-left">
                <h4><b class="navbar-heading header_title">FILE TRACKING APPLICATION</b></h4>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php 
                      switch ($_SESSION['account_type']) {
                        case 'staff':
                         echo '<img src="../pages/images/user.png" alt="">'.$_SESSION['fullname'];
                          break;
                        case 'administator':
                         echo '<img src="../pages/images/user.png" alt="">ADMINISTRATOR';
                          break;
                        default:
                          echo '<img src="../pages/images/user.png" alt="">';
                          break;
                      }

                     ?>
                    <!-- image user name -->
                    <span class=" fa fa-chevron-circle-down"></span>
                  </a>

                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"></a>
                    <li><a data-toggle="modal" data-target="#changePassModal">Change Password <i class="fa fa-cog pull-right"></i></a></li>
                    <li><a href="../controllers/logOut.php"><i class="fa fa-sign-out pull-right asterick"></i> Log Out</a></li>
                  </ul>
                </li>
                <!-- notifications -->
                <li role="presentation">
                  <a href="../pages/documents_transfer.php" class="info-number">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-red"><?php echo $objDocTransfer->count_get_doc_transfer_received_by_staff();?></span>
                  </a>
                </li>
                <!--  -->
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <!-- <div class="clearfix"></div> -->

          
<!-- modal for changing password -->
     <div class="modal fade" id="changePassModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog ">
            <div class="modal-content">
              <div class="modal-header" id="bg">
                 <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default btnClose">&times;</span></button>
                <h4 class="modal-title"><b id="subject">Change Password</b></h4>
              </div>
              <div class="modal-body" id="bg">
                <form id="change_password_form" method="POST"> 
                    <!--  -->
                  <div class="row">
                    <div class="col-md-4"><label>New Password <span class="asterick"> *</span></label></div>
                    <div class="col-md-8">
                      <input type="password" class="form-control" id="newPasswd" name="newPasswd" minlength="4" autocomplete="off" placeholder="Enter New Password &hellip;" required>
                    </div>
                  </div>

                  <br>
                  <!--  -->
                  <div class="row">
                    <div class="col-md-4"><label>Retype Password <span class="asterick"> *</span></label></div>
                    <div class="col-md-8">
                      <input type="password" class="form-control" id="retypeNewPasswd" name="retypeNewPasswd" minlength="4" autocomplete="off" placeholder="Retype Password &hellip;" required>
                    </div>
                  </div>

                    <br>
                    <input type="hidden" name="mode" value="userChangePassword">

                    <div class=" modal-footer" id="bg">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="changePass_btn">Change Password <i class="fa fa-exchange"></i></button>
                    </div>        
                </form>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- ////////////////////////////////////////////////////////// Getting started modal ////////////////////////////////////////////////////////////////////// -->
        
        <div id="gettingStartedModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header gettingStarted" style="">
                <h4 class="modal-title"><center><b>Welcome to MINISTRY OF LANDS AND NATURAL RESOURCES File Tracking Application</b></center></h4>
              </div>
              <div class="modal-body">
                <p>Some text in the modal.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
              </div>
            </div>

          </div>
        </div>