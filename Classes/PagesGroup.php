<?php 
	date_default_timezone_set('Africa/Accra');
	class PagesGroup{
		// setting and getting variables
		private $id;
		private $pagesGroupName;
		private $pagesId;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "pages_group";

		function set_id($id) { $this->id = $id; }
		function set_pagesGroupName($pagesGroupName) { $this->pagesGroupName = $pagesGroupName; }
		function set_pagesId($pagesId) { $this->pagesId = $pagesId; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }

		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date =date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (pages_group_name,pages_id,added,record_hide,added_staff_id,last_updated_staff_id) VALUES (:pagesGroupName,:pagesId,:added,:recordHide,:addedStaffId,:lastUpdatedStaffId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":pagesGroupName",$this->pagesGroupName);
			$stmt->bindParam(":pagesId",$this->pagesId);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":addedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET pages_group_name=:pagesGroupName,pages_id=:pagesId,last_updated_staff_id=:lastUpdatedStaffId WHERE pages_group_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":pagesGroupName",$this->pagesGroupName);
				$stmt->bindParam(":pagesId",$this->pagesId);
				$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
				$stmt->bindParam(":Id",$this->id);
				
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE pages_group_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get users
		function get_pages_groups(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY pages_group_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

	// get all groups for admin
		function get_pages_groups_all(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY pages_group_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

	// get user
		function get_group_by_id(){
			$sql="SELECT * FROM $this->table WHERE pages_group_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
			}

	// get group name by id
		function get_groupName_by_id(){
			$sql="SELECT group_id,group_name FROM $this->table WHERE pages_group_id=:groupId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":groupId",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
			}

	// getting all pages for user login dashboard based on group number

			function menu_pages_id($groupId){
				
				$sql="SELECT pages_id FROM $this->table WHERE pages_group_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$groupId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					$pagesId = json_decode($results["pages_id"]);
					return $pagesId;
				}
				else{
					die();
					}

			}


	}

 ?>