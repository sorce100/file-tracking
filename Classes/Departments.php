<?php 
	date_default_timezone_set('Africa/Accra');
	class Departments{
		// setting and getting variables
		private $id;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "departments";
		private $departmentName;
		private $departmentHead;
		private $departmentSecretary;
		private $departmentNotes;

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_departmentName($departmentName) { $this->departmentName = $departmentName; }
		function set_departmentHead($departmentHead) { $this->departmentHead = $departmentHead; }
		function set_departmentSecretary($departmentSecretary) { $this->departmentSecretary = $departmentSecretary; }
		function set_departmentNotes($departmentNotes) { $this->departmentNotes = $departmentNotes; }

		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (department_name,departement_head,department_secretaries,department_notes,added,record_hide,last_updated,last_updated_staff_id,added_staff_id) 
			VALUES (:departmentName,:departmentHead,:departmentSecretary,:departmentNotes,:added,:recordHide,:lastUpdated,:lastUpdatedStaffId,:addedStaffId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":departmentName",$this->departmentName);
			$stmt->bindParam(":departmentHead",$this->departmentHead);
			$stmt->bindParam(":departmentSecretary",$this->departmentSecretary);
			$stmt->bindParam(":departmentNotes",$this->departmentNotes);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":lastUpdated",$date);
			$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":addedStaffId",$_SESSION['staff_id']);

			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$date=date("jS F Y \/ h:i:s A");
			$sql="UPDATE $this->table SET department_name=:departmentName,departement_head=:departmentHead,department_secretaries=:departmentSecretary,department_notes=:departmentNotes,last_updated=:lastUpdated,last_updated_staff_id=:lastUpdatedStaffId WHERE department_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":departmentName",$this->departmentName);
				$stmt->bindParam(":departmentHead",$this->departmentHead);
				$stmt->bindParam(":departmentSecretary",$this->departmentSecretary);
				$stmt->bindParam(":departmentNotes",$this->departmentNotes);
				$stmt->bindParam(":lastUpdated",$date);
				$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE department_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get users
		function get_departments(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY department_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
				}

		}
	// get all departments
		function get_departments_all(){
			$sql="SELECT d.department_name,d.departement_head,d.department_secretaries,d.added,d.department_id,s.staff_first_name,s.staff_last_name 
			FROM $this->table AS d
			RIGHT JOIN staffs AS s
			ON d.departement_head = s.staff_id
			WHERE d.record_hide=:recordHide 
			ORDER BY d.department_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
				}

		}

	// department list 
		function get_departments_list(){
			$returnRecords = '';
			$sql="SELECT D.department_name,D.departement_head,D.department_secretaries,D.added,D.department_id,
			CONCAT(SH.staff_first_name,' ',SH.staff_last_name) AS headname,
			CONCAT(SC.staff_first_name,' ',SC.staff_last_name) AS secretaryname
			FROM $this->table AS D
			LEFT JOIN staffs AS SH ON D.departement_head = SH.staff_id
			LEFT JOIN staffs AS SC ON D.department_secretaries = SC.staff_id
			
			WHERE D.record_hide=:recordHide 
			ORDER BY D.department_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$departments= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($departments as $department) {
					 $returnRecords .= '
				                        <tr>
				                          <td>'.trim($department["department_name"]).'</td>
				                          <td>'.trim($department["headname"]).'</td>
				                          <td>'.trim($department["secretaryname"]).'</td>
				                          <td>'.trim($department["added"]).'</td>
				                          <td>
				                            <button class="btn-primary update_department" id="'.$department["department_id"].'"><i class="fa fa-pencil"></i> UPDATE</button> 
				                            <button class="btn-danger del_department" id="'.$department["department_id"].'"><i class="fa fa-trash"></i> DELETE</button>
				                          </td>
				                        </tr>';
				}
				return $returnRecords;
			}
			else{
				return false;
				}

		}
	// get user
		function get_department_by_id(){
			$sql="SELECT * FROM $this->table WHERE department_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				return false;
				}
		}



	}

?>