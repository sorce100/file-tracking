<?php 
	date_default_timezone_set('Africa/Accra');
	class Staffs{
		// setting and getting variables
		private $id;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "staffs";
		private $staffFirstName;
		private $staffLastName;
		private $staffTelNo;
		private $staffEmail;
		private $staffEmployeeNum;
		private $staffEmployeeType;
		private $staffDepartmentId;
		private $staffUnitId;
		private $staffNotes;

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_staffFirstName($staffFirstName) { $this->staffFirstName = $staffFirstName; }
		function set_staffLastName($staffLastName) { $this->staffLastName = $staffLastName; }
		function set_staffTelNo($staffTelNo) { $this->staffTelNo = $staffTelNo; }
		function set_staffEmail($staffEmail) { $this->staffEmail = $staffEmail; }
		function set_staffEmployeeNum($staffEmployeeNum) { $this->staffEmployeeNum = $staffEmployeeNum; }
		function set_staffEmployeeType($staffEmployeeType) { $this->staffEmployeeType = $staffEmployeeType; }
		function set_staffDepartmentId($staffDepartmentId) { $this->staffDepartmentId = $staffDepartmentId; }
		function set_staffUnitId($staffUnitId) { $this->staffUnitId = $staffUnitId; }
		function set_staffNotes($staffNotes) { $this->staffNotes = $staffNotes; }


		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (staff_first_name, staff_last_name, staff_tel_num, staff_email, staff_employee_num, staff_employee_type, staff_unit_id, staff_department_id, staff_notes, added, record_hide, last_updated, last_updated_staff_id, added_staff_id) 
			VALUES (:staffFirstName,:staffLastName,:staffTelNo,:staffEmail,:staffEmployeeNum,:staffEmployeeType,:staffUnitId,:staffDepartmentId,:staffNotes,:added,:recordHide,:lastUpdated,:lastUpdatedStaffId,:addedStaffId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":staffFirstName",$this->staffFirstName);
			$stmt->bindParam(":staffLastName",$this->staffLastName);
			$stmt->bindParam(":staffTelNo",$this->staffTelNo);
			$stmt->bindParam(":staffEmail",$this->staffEmail);
			$stmt->bindParam(":staffEmployeeNum",$this->staffEmployeeNum);
			$stmt->bindParam(":staffEmployeeType",$this->staffEmployeeType);
			$stmt->bindParam(":staffDepartmentId",$this->staffDepartmentId);
			$stmt->bindParam(":staffUnitId",$this->staffUnitId);
			$stmt->bindParam(":staffNotes",$this->staffNotes);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":lastUpdated",$date);
			$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":addedStaffId",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET staff_first_name=:staffFirstName,staff_last_name=:staffLastName,staff_tel_num=:staffTelNo,staff_email=:staffEmail,staff_employee_num=:staffEmployeeNum,staff_employee_type=:staffEmployeeType,staff_unit_id=:staffUnitId,staff_department_id=:staffDepartmentId,staff_notes=:staffNotes,last_updated=:lastUpdated,last_updated_staff_id=:lastUpdatedStaffId WHERE staff_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":staffFirstName",$this->staffFirstName);
				$stmt->bindParam(":staffLastName",$this->staffLastName);
				$stmt->bindParam(":staffTelNo",$this->staffTelNo);
				$stmt->bindParam(":staffEmail",$this->staffEmail);
				$stmt->bindParam(":staffEmployeeNum",$this->staffEmployeeNum);
				$stmt->bindParam(":staffEmployeeType",$this->staffEmployeeType);
				$stmt->bindParam(":staffDepartmentId",$this->staffDepartmentId);
				$stmt->bindParam(":staffUnitId",$this->staffUnitId);
				$stmt->bindParam(":staffNotes",$this->staffNotes);
				$stmt->bindParam(":lastUpdated",$date);
				$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE staff_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}

	// get staff options
		function get_staff_options(){
			$returnRecords = '';
			$sql="SELECT staff_id, CONCAT(staff_first_name,' ',staff_last_name) AS staffname 
			FROM $this->table 
			ORDER BY staff_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$returnRecords .= '<option value="'.$result["staff_id"].'">'.trim($result["staffname"]).'</option>';
				}

				return $returnRecords;
			}
			else{
				return false;
				}
		}


	// get all staff
		function get_staffs_list(){
			$returnRecords = '';
			$sql="SELECT S.staff_id,S.staff_first_name,S.staff_last_name,S.staff_employee_type,S.staff_tel_num,S.staff_email,S.added,S.staff_unit_id,S.staff_department_id,S.staff_notes,U.unit_name,D.department_id,D.department_name
			FROM $this->table AS S
			INNER JOIN units AS U
			ON S.staff_unit_id = U.unit_id
			INNER JOIN departments AS D
			ON S.staff_department_id = D.department_id
			WHERE S.record_hide=:recordHide 
			ORDER BY S.staff_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$staffs= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($staffs as $staff) {
                          $returnRecords .= '
			                              <tr>
			                                <td>'.trim($staff["staff_first_name"]).' '.trim($staff["staff_last_name"]).'</td>
			                                <td>'.trim($staff["staff_employee_type"]).'</td>
			                                <td>'.trim($staff["staff_tel_num"]).'</td>
			                                <td>'.trim($staff["staff_email"]).'</td>
			                                <td>'.trim($staff["unit_name"]).'</td>
			                                <td>'.trim($staff["department_name"]).'</td>
			                                <td>'.trim($staff["added"]).'</td>
			                                <td>
			                                  <button class="btn-primary update_staff" id="'.$staff["staff_id"].'"><i class="fa fa-pencil"></i> UPDATE</button> 
			                                  <button class="btn-danger del_staff" id="'.$staff["staff_id"].'"><i class="fa fa-trash"></i> DELETE</button>
			                                </td>
			                              </tr>';
                }

				return $returnRecords;
			}
			else{
				return false;
				}

		}

		function get_staff_account_options(){
			$returnRecords = '';
			$sql="SELECT staff_id,staff_account_user_id, staff_first_name,staff_last_name FROM $this->table ORDER BY staff_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			if ($stmt->execute()) {
				$staffs= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($staffs as $staff) {
				  $fullname = trim($staff["staff_first_name"]).' '.trim($staff["staff_last_name"]);
                  $username = trim($staff["staff_first_name"]){0}.'.'.trim($staff["staff_last_name"]);
                   
                  if (!empty($staff['staff_account_user_id'])) {
                   $returnRecords .= '<option value="'.trim($staff["staff_id"]).'" id="'.$username.'" disabled>'. $fullname .'</option>';
                  }
                  elseif (empty($staff['staff_account_user_id'])) {
                    $returnRecords .= '<option value="'.trim($staff["staff_id"]).'" id="'.$username.'" >'.$fullname.'</option>';
                  }
	                  

	            }
				return $returnRecords;
			}
			else{
				return false;
			}
		}

	// get user
		function get_staff_by_id(){
			$sql="SELECT * FROM $this->table WHERE staff_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				return false;
				}
		}


	// get all staff by the departement
		function get_all_department_staff(){
			$sql="SELECT staff_id,staff_first_name,staff_last_name,staff_employee_type,staff_notes FROM $this->table WHERE staff_department_id=:staffDepartmentId ORDER BY staff_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":staffDepartmentId",$this->staffDepartmentId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
				}

		}

	// get all units satff
		function get_all_unit_staff(){
			$sql="SELECT staff_id,staff_first_name,staff_last_name,staff_employee_type,staff_notes FROM $this->table WHERE staff_unit_id=:staffUnitId ORDER BY staff_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":staffUnitId",$this->staffUnitId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
				}

		}

// //////////////////////////////////////////GET UNIT ASSOCIATED MEMBERS////////////////////////////////////////////////////////////
	// get all members details based on users unit Id
		function get_all_unit_staff_associated(){
			$returnRecords = '';
			$sql="SELECT CONCAT(staff_first_name,' ',staff_last_name) AS fullname,staff_tel_num,staff_email,staff_employee_type,staff_id
			FROM $this->table 
			WHERE staff_unit_id=:staffUnitId AND record_hide=:recordHide";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":staffUnitId",$_SESSION['unit_id']);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$staffs= $stmt->fetchAll(PDO::FETCH_ASSOC);
				// get the head and the secretary of unit
				$headSecArray = $this->get_staff_unit_head_secretary();
				// grab head
				$unitHead = $headSecArray['unit_head'];
				// grab Sec
				$unitSec = $headSecArray['unit_secretaries'];

				foreach ($staffs as $staff) {
					$returnRecords .= '
	                                    <tr>
	                                      <td>'.trim($staff["fullname"]).'</td>
	                                      <td>'.trim($staff["staff_tel_num"]).'</td>
	                                      <td>'.trim($staff["staff_email"]).'</td>
	                                      <td>'.trim($staff["staff_employee_type"]).'</td>';
	                                      if (trim($staff["staff_id"]) == $unitHead ) {
	                                      	$returnRecords .= '<td><i class="fa fa-user bg-red"></i> Head</td>';
	                                      }
	                                      elseif (trim($staff["staff_id"]) == $unitSec ) {
	                                      	$returnRecords .= '<td><i class="fa fa-user bg-red"></i> Secretary</td>';
	                                      }
	                                      elseif (trim($staff["staff_id"]) != $unitSec || trim($staff["staff_id"]) != $unitHead ) {
	                                      	$returnRecords .= '<td>Member</td>';
	                                      }
                    $returnRecords .= '</tr>';
				}

				return $returnRecords;
			}
			else{
				return false;
				}
		}


	// get unit head and secretary
		function get_staff_unit_head_secretary(){
			$sql="SELECT unit_head,unit_secretaries FROM units WHERE unit_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$_SESSION['unit_id']);
			if ($stmt->execute()) {
				$result= $stmt->fetch(PDO::FETCH_ASSOC);
				return $result;
			}
			else{
				return false;
				}
		}

		
// ////////////////////////////GET DEPARTMENT STAFF IN THE SAME DEPARTMENT AS CURRENT USER/////////////////////////////////////////////

		function get_all_department_staff_associated(){
			$returnRecords = '';
			$sql="SELECT CONCAT(staff_first_name,' ',staff_last_name) AS fullname,staff_tel_num,staff_email,staff_employee_type,staff_id
			FROM $this->table 
			WHERE staff_department_id=:staffDepartmentId AND record_hide=:recordHide";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":staffDepartmentId",$_SESSION['department_id']);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$staffs= $stmt->fetchAll(PDO::FETCH_ASSOC);
				// get the head and the secretary of unit
				$headSecArray = $this->get_staff_department_head_secretary();
				// grab head
				$departHead = $headSecArray['departement_head'];
				// grab Sec
				$departSec = $headSecArray['department_secretaries'];

				foreach ($staffs as $staff) {
					$returnRecords .= '
	                                    <tr>
	                                      <td>'.trim($staff["fullname"]).'</td>
	                                      <td>'.trim($staff["staff_tel_num"]).'</td>
	                                      <td>'.trim($staff["staff_email"]).'</td>
	                                      <td>'.trim($staff["staff_employee_type"]).'</td>';
	                                      if (trim($staff["staff_id"]) == $departHead ) {
	                                      	$returnRecords .= '<td><i class="fa fa-user bg-red"></i> Head</td>';
	                                      }
	                                      elseif (trim($staff["staff_id"]) == $departSec ) {
	                                      	$returnRecords .= '<td><i class="fa fa-user bg-red"></i> Secretary</td>';
	                                      }
	                                      elseif (trim($staff["staff_id"]) != $departSec || trim($staff["staff_id"]) != $departHead ) {
	                                      	$returnRecords .= '<td>Member</td>';
	                                      }
                    $returnRecords .= '</tr>';
				}

				return $returnRecords;
			}
			else{
				return false;
				}
		}


	// get department head and secretary
		function get_staff_department_head_secretary(){
			$sql="SELECT departement_head,department_secretaries FROM departments WHERE department_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$_SESSION['department_id']);
			if ($stmt->execute()) {
				$result= $stmt->fetch(PDO::FETCH_ASSOC);
				return $result;
			}
			else{
				return false;
				}
		}

// get staff profile details

		function get_staff_profile_details(){
			$sql="SELECT S.staff_first_name,S.staff_last_name,S.staff_email,S.staff_employee_type,
			D.department_name,
			U.unit_name
			FROM $this->table AS S
			LEFT JOIN departments AS D ON S.staff_department_id = D.department_id
			LEFT JOIN units AS U ON S.staff_unit_id = U.unit_id
			WHERE staff_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				$result= $stmt->fetch(PDO::FETCH_ASSOC);
				return '
						<tr>
			                <td class="bg-dark-blue">User First Name</td><td>'.$result["staff_first_name"].'</td>
			              </tr>
			              <tr>
			                <td class="bg-dark-blue">User Last Name</td><td>'.$result["staff_last_name"].'</td>
			              </tr>
			              <tr>
			                <td class="bg-dark-blue">User Email</td><td>'.$result["staff_email"].'</td>
			              </tr>
			              <tr>
			                <td class="bg-dark-blue">User Type</td><td>'.$result["staff_employee_type"].'</td>
			              </tr>
			              <tr>
			                <td class="bg-dark-blue">User Unit</td><td>'.$result["unit_name"].'</td>
			              </tr>
			              <tr>
			                <td class="bg-dark-blue">User Department</td><td>'.$result["department_name"].'</td>
			              </tr>';
			}
			else{
				return false;
				}
		}

}

?>