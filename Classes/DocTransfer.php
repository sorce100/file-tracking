<?php 
	date_default_timezone_set('Africa/Accra');
	class DocTransfer{
		// setting and getting variables
		private $id;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "documents_transfer";
		private $docReceivedId;
		private $docTransferSenderDepartmentId;
		private $docTransferSenderUnitId;
		private $docTransferSenderStaffId;
		private $docTransferSenderDate;
		private $docTransferReceiverDepartmentId;
		private $docTransferReceiverUnitId;
		private $docTransferReceiverStaffIds;
		private $docTransferReceiverAccepted = "NO";
		private $docTransferReceiverAcceptedYes = "YES";
		private $docTransferReceiverAcceptedDate;
		private $docTransferFeceiverAcceptStaffId;
		private $docTransferReceiverCategory;
		

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_docReceivedId($docReceivedId) { $this->docReceivedId = $docReceivedId; }
		function set_docTransferSenderDepartmentId($docTransferSenderDepartmentId) { $this->docTransferSenderDepartmentId = $docTransferSenderDepartmentId; }
		function set_docTransferSenderUnitId($docTransferSenderUnitId) { $this->docTransferSenderUnitId = $docTransferSenderUnitId; }
		function set_docTransferSenderStaffId($docTransferSenderStaffId) { $this->docTransferSenderStaffId = $docTransferSenderStaffId; }
		function set_docTransferSenderDate($docTransferSenderDate) { $this->docTransferSenderDate = $docTransferSenderDate; }
		function set_docTransferReceiverDepartmentId($docTransferReceiverDepartmentId) { $this->docTransferReceiverDepartmentId = $docTransferReceiverDepartmentId; }
		function set_docTransferReceiverUnitId($docTransferReceiverUnitId) { $this->docTransferReceiverUnitId = $docTransferReceiverUnitId; }
		function set_docTransferReceiverStaffIds($docTransferReceiverStaffIds) { $this->docTransferReceiverStaffIds = $docTransferReceiverStaffIds; }
		function set_docTransferReceiverAccepted($docTransferReceiverAccepted) { $this->docTransferReceiverAccepted = $docTransferReceiverAccepted; }
		function set_docTransferReceiverAcceptedDate($docTransferReceiverAcceptedDate) { $this->docTransferReceiverAcceptedDate = $docTransferReceiverAcceptedDate; }
		function set_docTransferFeceiverAcceptStaffId($docTransferFeceiverAcceptStaffId) { $this->docTransferFeceiverAcceptStaffId = $docTransferFeceiverAcceptStaffId; }
		function set_docTransferReceiverCategory($docTransferReceiverCategory) { $this->docTransferReceiverCategory = $docTransferReceiverCategory; }


		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){

			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (doc_received_id, doc_transfer_sender_department_id, doc_transfer_sender_unit_id, doc_transfer_sender_staff_id, doc_transfer_sender_date, doc_transfer_receiver_department_id, doc_transfer_receiver_unit_id, doc_transfer_receiver_staff_ids, doc_transfer_receiver_accepted, added, record_hide, last_updated_staff_id,last_updated_date, doc_transfer_receiver_category) 
			VALUES (:docReceivedId,:docTransferSenderDepartmentId,:docTransferSenderUnitId,:docTransferSenderStaffId,:docTransferSenderDate,:docTransferReceiverDepartmentId,:docTransferReceiverUnitId,:docTransferReceiverStaffIds,:docTransferReceiverAccepted,:added,:recordHide,:lastUpdatedStaffId,:lastUpdatedDate,:docTransferReceiverCategory)";
			$stmt = $this->dbConn->prepare($sql);

			$stmt->bindParam(":docReceivedId",$this->docReceivedId);
			$stmt->bindParam(":docTransferSenderDepartmentId",$_SESSION['department_id']);
			$stmt->bindParam(":docTransferSenderUnitId",$_SESSION['unit_id']);
			$stmt->bindParam(":docTransferSenderStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":docTransferSenderDate",$date);
			$stmt->bindParam(":docTransferReceiverDepartmentId",$this->docTransferReceiverDepartmentId);
			$stmt->bindParam(":docTransferReceiverUnitId",$this->docTransferReceiverUnitId);
			$stmt->bindParam(":docTransferReceiverStaffIds",$this->docTransferReceiverStaffIds);
			$stmt->bindParam(":docTransferReceiverAccepted",$this->docTransferReceiverAccepted);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":lastUpdatedDate",$date);
			$stmt->bindParam(":docTransferReceiverCategory",$this->docTransferReceiverCategory);

			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$date=date("jS F Y \/ h:i:s A");
			$sql="UPDATE $this->table SET department_name=:departmentName,departement_head=:departmentHead,department_secretaries=:departmentSecretary,department_notes=:departmentNotes,last_updated=:lastUpdated,last_updated_staff_id=:lastUpdatedStaffId WHERE department_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":departmentName",$this->departmentName);
				$stmt->bindParam(":departmentHead",$this->departmentHead);
				$stmt->bindParam(":departmentSecretary",$this->departmentSecretary);
				$stmt->bindParam(":departmentNotes",$this->departmentNotes);
				$stmt->bindParam(":lastUpdated",$date);
				$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE department_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get Sender doc tranfer history
		function get_doc_transfer_sender_by_department(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide AND doc_transfer_sender_department_id=:docTransferSenderDepartmentId  ORDER BY department_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":docTransferSenderDepartmentId",$_SESSION['department_id']);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					return $results;
				}
			}
			else{
				return false;
				}

		}

		function get_doc_transfer_sender_by_unit(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide AND doc_transfer_sender_unit_id=:docTransferSenderUnitId ORDER BY department_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":docTransferSenderUnitId",$_SESSION['unit_id']);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					return $results;
				}
				
			}
			else{
				return false;
				}

		}

		function get_doc_transfer_sender_by_staff(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide AND doc_transfer_sender_staff_id=:docTransferSenderStaffId ORDER BY department_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":docTransferSenderStaffId",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					return $results;
				}
			}
			else{
				return false;
				}

		}

		// count document received by staff
		function count_get_doc_transfer_received_by_staff(){
			$sql="SELECT *
			FROM $this->table
			WHERE record_hide=:recordHide 
			AND doc_transfer_receiver_accepted=:docTransferReceiverAcceptedNo 
			ORDER BY doc_transfer_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":docTransferReceiverAcceptedNo",$this->docTransferReceiverAccepted);
			if ($stmt->execute()) {
				return $stmt->rowCount();
			}

		}

		// get document receiver details
		function get_doc_transfer_received_by_staff(){
			$returnRecords=[];
			$sql="SELECT T.doc_transfer_id ,T.doc_transfer_receiver_category,T.doc_transfer_receiver_department_id,T.doc_transfer_sender_date,T.doc_received_id,T.doc_transfer_sender_staff_id,T.doc_transfer_receiver_accepted_date,T.doc_transfer_receiver_accepted_staff_id,T.doc_transfer_receiver_staff_ids,T.doc_transfer_receiver_unit_id,
			D.document_received_subject,D.document_received_source,D.document_received_type,
			CONCAT(S.staff_first_name,' ',S.staff_last_name) AS staffname 
			FROM $this->table AS T
			INNER JOIN documents_received AS D
			ON T.doc_received_id = D.document_received_id
			INNER JOIN staffs AS S
			ON T.doc_transfer_sender_staff_id = S.staff_id
			WHERE T.record_hide=:recordHide 
			AND T.doc_transfer_receiver_accepted=:docTransferReceiverAcceptedNo 
			OR  T.doc_transfer_receiver_accepted=:docTransferReceiverAcceptedYes
			ORDER BY doc_transfer_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":docTransferReceiverAcceptedNo",$this->docTransferReceiverAccepted);
			$stmt->bindParam(":docTransferReceiverAcceptedYes",$this->docTransferReceiverAcceptedYes);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					// loop through results and grab the category
					foreach ($results as $result) {
						// switch the category and pick the head and sectary then compare and add the records
						switch (trim($result['doc_transfer_receiver_category'])) {
							case 'Department':
								// get the head and secetary Array
								$headSecDetails = $this->get_department_head_secretary($result['doc_transfer_receiver_department_id']);
								// check if staff is head or secretary of department to generate row of data for 
								if (in_array($_SESSION['staff_id'], $headSecDetails)) {
									$returnRecords[] = '<tr>';
														if (!empty(trim($result["doc_transfer_receiver_accepted_date"])) ||  !empty(trim($result["doc_transfer_receiver_accepted_staff_id"])) ) {

															$returnRecords[] = '<td><input type="checkbox" name="'.trim($result["doc_received_id"]).'"class="docReceivedSelectedIds"></td>';
														}
														else{

															$returnRecords[] = '<td></td>';
														}

							        $returnRecords[] = 		'<td>'.trim($result["doc_transfer_receiver_category"]).'</td>
							                                <td>'.trim($result["doc_transfer_sender_date"]).'</td>
							                                <td>'.trim($result["document_received_source"]).'</td>
							                                <td>'.trim($result["document_received_type"]).'</td>
							                                <td>'.trim($result["document_received_subject"]).'</td>
							                                <td>'.trim($result["staffname"]).'</td>
							                                <td>';

							                            // check if the document has being accepted already or not

							                                if (empty($result["doc_transfer_receiver_accepted_date"]) || empty($result["doc_transfer_receiver_accepted_staff_id"]) ) {
							                                	$returnRecords[] = '<button class="btn-primary accept_received_doc_btn" id="'.$result["doc_transfer_id"].'"><i class="fa fa-check-circle"></i> ACCEPT DOCUMENT</button> 
										                                </td>
										                            </tr>'; 
							                                }
							                                elseif (!empty(trim($result["doc_transfer_receiver_accepted_date"])) ||  !empty(trim($result["doc_transfer_receiver_accepted_staff_id"])) ) {
							                                	$returnRecords[] = '
							                                	<button class="btn-info doc_history_btn" id="'.$result["doc_received_id"].'"><i class="fa fa-hourglass"></i> HISTORY</button>
							                                	<button class="bg-dark-blue add_minute_btn" id="'.$result["doc_received_id"].'"><i class="fa fa-pencil"></i> ADD MINUTE</button>
										                            </tr>';
							                                }
								}
								// now check if the staff is part of the document included staff
								$decodedReceiverStaffArray = json_decode($result["doc_transfer_receiver_staff_ids"],true);

								if (!empty($decodedReceiverStaffArray)) {
									// switch through the array by index then display based on read or unread
									switch ($decodedReceiverStaffArray[$_SESSION['staff_id']]) {
										// check to see if staff is a head or sectary to prevent double display
											case 'READ':
												if (!in_array($_SESSION['staff_id'], $headSecDetails)) {
													$returnRecords[] = '<tr>
											                                <td>'.trim($result["doc_transfer_sender_date"]).'</td>
											                                <td>'.trim($result["document_received_subject"]).'</td>
											                                <td>'.trim($result["staffname"]).'</td>
											                                <td>
											                                  <button class="btn-info doc_history_btn" id="'.$result["doc_received_id"].'"><i class="fa fa-hourglass"></i> HISTORY</button>
											                                </td>
											                            </tr>';
										        }
											break;
											case 'UNREAD':
												if (!in_array($_SESSION['staff_id'], $headSecDetails)) {
													$returnRecords[] = '<tr>
											                                <td>'.trim($result["doc_transfer_sender_date"]).'</td>
											                                <td>'.trim($result["document_received_subject"]).'</td>
											                                <td>'.trim($result["staffname"]).'</td>
											                                <td>
											                                  <button class="btn-info doc_history_btn" id="'.$result["doc_received_id"].'"><i class="fa fa-hourglass"></i> HISTORY</button>
											                                </td>
											                            </tr>';
										        }
											break;
											default:
										}
									// switch staff of receivers added
								}

							break;
							case 'Unit':
								
								// get the head and secetary Array
								$headSecDetails = $this->get_unit_head_secretary($result['doc_transfer_receiver_unit_id']);
								// check if staff is head or secretary of department to generate row of data for 
								if (in_array($_SESSION['staff_id'], $headSecDetails)) {
									$returnRecords[] = '<tr>';
														if (!empty(trim($result["doc_transfer_receiver_accepted_date"])) ||  !empty(trim($result["doc_transfer_receiver_accepted_staff_id"])) ) {

															$returnRecords[] = '<td><input type="checkbox" name="'.trim($result["doc_received_id"]).'"class="docReceivedSelectedIds"></td>';
														}
														else{

															$returnRecords[] = '<td></td>';
														}

									$returnRecords[] = '
						                                <td>'.trim($result["doc_transfer_receiver_category"]).'</td>
							                            <td>'.trim($result["doc_transfer_sender_date"]).'</td>
						                                <td>'.trim($result["document_received_source"]).'</td>
						                                <td>'.trim($result["document_received_type"]).'</td>
						                                <td>'.trim($result["document_received_subject"]).'</td>
						                                <td>'.trim($result["staffname"]).'</td>
						                                <td>';

							                            // check if the document has being accepted already or not

							                                if (empty($result["doc_transfer_receiver_accepted_date"]) || empty($result["doc_transfer_receiver_accepted_staff_id"]) ) {
							                                	$returnRecords[] = '<button class="btn-primary accept_received_doc_btn" id="'.$result["doc_transfer_id"].'"><i class="fa fa-check-circle"></i> ACCEPT DOCUMENT</button> 
										                                </td>
										                            </tr>'; 
							                                }
							                                elseif (!empty(trim($result["doc_transfer_receiver_accepted_date"])) ||  !empty(trim($result["doc_transfer_receiver_accepted_staff_id"])) ) {
							                                	$returnRecords[] = '
							                                	<button class="btn-info doc_history_btn" id="'.$result["doc_received_id"].'"><i class="fa fa-hourglass"></i> HISTORY</button>
							                                	<button class="bg-dark-blue add_minute_btn" id="'.$result["doc_received_id"].'"><i class="fa fa-pencil"></i> ADD MINUTE</button>
										                            </tr>';
							                                }
								}

								if (!isset($result["doc_transfer_receiver_staff_ids"])) {
									// now check if the staff is part of the document included staff
									$decodedReceiverStaffArray = json_decode($result["doc_transfer_receiver_staff_ids"],true);
									// switch through the array by index then display based on read or unread
									switch ($decodedReceiverStaffArray[$_SESSION['staff_id']]) {
										// check to see if staff is a head or sectary to prevent double display
											case 'READ':
												if (!in_array($_SESSION['staff_id'], $headSecDetails)) {
													$returnRecords[] = '<tr>
											                                <td>'.trim($result["doc_transfer_sender_date"]).'</td>
											                                <td>'.trim($result["document_received_subject"]).'</td>
											                                <td>'.trim($result["staffname"]).'</td>
											                                <td>
											                                  <button class="btn-info doc_history_btn" id="'.$result["doc_received_id"].'"><i class="fa fa-hourglass"></i> HISTORY</button>
											                                </td>
											                            </tr>';
										        }
											break;
											case 'UNREAD':
												if (!in_array($_SESSION['staff_id'], $headSecDetails)) {
													$returnRecords[] = '<tr>
											                                <td>'.trim($result["doc_transfer_sender_date"]).'</td>
											                                <td>'.trim($result["document_received_subject"]).'</td>
											                                <td>'.trim($result["staffname"]).'</td>
											                                <td>
											                                  <button class="btn-info doc_history_btn" id="'.$result["doc_received_id"].'"><i class="fa fa-hourglass"></i> HISTORY</button>
											                                </td>
											                            </tr>';
										        }
											break;
											default:
									}
									// switch staff of receivers added
								}
							break;
							case 'Staff':
								// if staff only then just get array of staff check and display
							break;
							
						}
						// switch 

					// }
					// foreach
				}

				return $returnRecords;
			}
			else{
				return false;
				}

			}

		}

		// get the head and sec of department
		function get_department_head_secretary($departmentId){
			$headSecArray=[];
			$sql="SELECT departement_head,department_secretaries FROM departments WHERE department_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$departmentId);
			if ($stmt->execute()) {
				$result= $stmt->fetch(PDO::FETCH_ASSOC);
				$headSecArray[]=$result['departement_head'];
				$headSecArray[]=$result['department_secretaries'];
				
				return $headSecArray;
			}
			else{
				return false;
				}
		}

		// get the head and sec of unit
		function get_unit_head_secretary($unitId){
			$headSecArray=[];
			$sql="SELECT unit_head,unit_secretaries FROM units WHERE unit_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$unitId);
			if ($stmt->execute()) {
				$result= $stmt->fetch(PDO::FETCH_ASSOC);
				$headSecArray[]=$result['unit_head'];
				$headSecArray[]=$result['unit_secretaries'];
				
				return $headSecArray;
			}
			else{
				return false;
				}
		}

	// count all documents transferd by staff
		function count_get_doc_transfer_by_staff(){
			$returnRecords=[];
			$sql="SELECT *
			FROM $this->table
			WHERE doc_transfer_sender_staff_id=:staffId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":staffId",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $stmt->rowCount();
			}
			else{
				return false;
				}
		}


	// get all documents details that was transfered by staff
		function get_doc_transfer_by_staff(){
			$returnRecords=[];
			$sql="SELECT T.doc_received_id,T.doc_transfer_sender_date,
			D.document_received_subject,D.document_received_source,D.document_received_type
			FROM $this->table AS T
			INNER JOIN documents_received AS D
			ON T.doc_received_id = D.document_received_id
			WHERE T.doc_transfer_sender_staff_id=:staffId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":staffId",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$returnRecords[] = '<tr>
			                                <td>'.trim($result["doc_transfer_sender_date"]).'</td>
			                                <td>'.trim($result["document_received_source"]).'</td>
			                                <td>'.trim($result["document_received_type"]).'</td>
			                                <td>'.trim($result["document_received_subject"]).'</td>
			                                <td>
			                                  <button class="btn-info doc_history_btn" id="'.$result["doc_received_id"].'"><i class="fa fa-hourglass"></i> HISTORY</button>
			                                </td>
			                            </tr>';
				}

				return $returnRecords;
			}
			else{
				return false;
				}
		}



		// get all document tranfer history
		function get_all_document_transfer_history(){
			$sql="SELECT * FROM $this->table WHERE doc_received_id =:docReceivedId ";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":docReceivedId",$this->docReceivedId);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
			}
			else{
				return false;
				}
		}


	// update document tranfer with staff data when accepted

		function staff_accepted_document_tranfer(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "UPDATE $this->table SET doc_transfer_receiver_accepted=:acceptedStatus,doc_transfer_receiver_accepted_date=:acceptedDate,doc_transfer_receiver_accepted_staff_id=:acceptedStaffId WHERE doc_transfer_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":acceptedStatus",$this->docTransferReceiverAcceptedYes);
			$stmt->bindParam(":acceptedDate",$date);
			$stmt->bindParam(":acceptedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				return 	'	<button class="btn-info doc_history_btn" id="'.$this->id.'"><i class="fa fa-hourglass"></i> HISTORY</button>
                        	<button class="bg-green add_minute_btn" id="'.$this->id.'"><i class="fa fa-pencil"></i> ADD MINUTE</button>';
			}
			else{
				return false;
				}
		}


		///////////////////////////////GET UNIT DOCS///////////////////////////////////////////////////////////////////////////////////////////

		function get_all_units_incoming_documents(){
			$returnRecords='';

			$sql="SELECT T.doc_transfer_id ,T.doc_transfer_receiver_category,T.doc_transfer_receiver_department_id,T.doc_transfer_sender_date,T.doc_received_id,T.doc_transfer_sender_staff_id,T.doc_transfer_receiver_accepted_date,T.doc_transfer_receiver_accepted_staff_id,T.doc_transfer_receiver_staff_ids,T.added,
			D.document_received_subject,D.document_received_source,D.document_received_type,D.document_received_from_company,
			CONCAT(Sender.staff_first_name,' ',Sender.staff_last_name) AS senderstaffname,
			CONCAT(Receiver.staff_first_name,' ',Receiver.staff_last_name) AS receiverstaffname,
			DPT.department_name,
			U.unit_name 

			FROM $this->table AS T
			LEFT JOIN documents_received AS D ON T.doc_received_id = D.document_received_id
			LEFT JOIN staffs AS Sender ON T.doc_transfer_sender_staff_id = Sender.staff_id
			LEFT JOIN staffs AS Receiver ON T.doc_transfer_receiver_accepted_staff_id = Receiver.staff_id
			LEFT JOIN departments AS DPT ON T.doc_transfer_sender_department_id = DPT.department_id
			LEFT JOIN units AS U ON T.doc_transfer_sender_unit_id = U.unit_id
			WHERE T.record_hide=:recordHide 
			AND doc_transfer_receiver_unit_id = :docTransferReceiverUnitId
			ORDER BY doc_transfer_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":docTransferReceiverUnitId",$_SESSION['unit_id']);
			
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					// loop through results and data
					foreach ($results as $result) {
						$returnRecords .= '<tr>
												<td>'.$result["added"].'</td>
												<td>'.$result["document_received_subject"].'</td>
												<td>'.$result["document_received_source"].'</td>
												<td>'.$result["document_received_type"].'</td>
												<td>'.$result["senderstaffname"].'</td>
												<td>'.$result["department_name"].'</td>
												<td>'.$result["unit_name"].'</td>
												<td>'.$result["doc_transfer_receiver_accepted_date"].'</td>
												<td>'.$result["receiverstaffname"].'</td>
											</tr>';
					}

				return $returnRecords;
			}
			else{
				return false;
				}

			}

		}

		// unit outgoing documents
		function get_all_units_outgoing_documents(){
			$returnRecords='';

			$sql="SELECT T.doc_transfer_id ,T.doc_transfer_receiver_category,T.doc_transfer_receiver_department_id,T.doc_transfer_sender_date,T.doc_received_id,T.doc_transfer_sender_staff_id,T.doc_transfer_receiver_accepted_date,T.doc_transfer_receiver_accepted_staff_id,T.doc_transfer_receiver_staff_ids,T.added,
			D.document_received_subject,D.document_received_source,D.document_received_type,D.document_received_from_company,
			CONCAT(Sender.staff_first_name,' ',Sender.staff_last_name) AS senderstaffname,
			CONCAT(Receiver.staff_first_name,' ',Receiver.staff_last_name) AS receiverstaffname

			FROM $this->table AS T
			LEFT JOIN documents_received AS D ON T.doc_received_id = D.document_received_id
			LEFT JOIN staffs AS Sender ON T.doc_transfer_sender_staff_id = Sender.staff_id
			LEFT JOIN staffs AS Receiver ON T.doc_transfer_receiver_accepted_staff_id = Receiver.staff_id
			WHERE T.record_hide=:recordHide 
			AND doc_transfer_sender_unit_id = :docTransferOutgoingUnitId
			ORDER BY doc_transfer_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":docTransferOutgoingUnitId",$_SESSION['unit_id']);
			
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					// loop through results and data
					foreach ($results as $result) {
						$returnRecords .= '<tr>
												<td>'.$result["added"].'</td>
												<td>'.$result["document_received_subject"].'</td>
												<td>'.$result["document_received_source"].'</td>
												<td>'.$result["document_received_type"].'</td>
												<td>'.$result["senderstaffname"].'</td>
												<td>'.$result["doc_transfer_receiver_category"].'</td>
												<td>'.$result["doc_transfer_receiver_accepted_date"].'</td>
												<td>'.$result["receiverstaffname"].'</td>
											</tr>';
					}

				return $returnRecords;
			}
			else{
				return false;
				}

			}

		}


		///////////////////////////////GET DEPARTMENT DOCS///////////////////////////////////////////////////////////////////////////////////

		function get_all_department_incoming_documents(){
			$returnRecords='';

			$sql="SELECT T.doc_transfer_id ,T.doc_transfer_receiver_category,T.doc_transfer_receiver_department_id,T.doc_transfer_sender_date,T.doc_received_id,T.doc_transfer_sender_staff_id,T.doc_transfer_receiver_accepted_date,T.doc_transfer_receiver_accepted_staff_id,T.doc_transfer_receiver_staff_ids,T.added,
			D.document_received_subject,D.document_received_source,D.document_received_type,D.document_received_from_company,
			CONCAT(Sender.staff_first_name,' ',Sender.staff_last_name) AS senderstaffname,
			CONCAT(Receiver.staff_first_name,' ',Receiver.staff_last_name) AS receiverstaffname,
			DPT.department_name,
			U.unit_name 

			FROM $this->table AS T
			LEFT JOIN documents_received AS D ON T.doc_received_id = D.document_received_id
			LEFT JOIN staffs AS Sender ON T.doc_transfer_sender_staff_id = Sender.staff_id
			LEFT JOIN staffs AS Receiver ON T.doc_transfer_receiver_accepted_staff_id = Receiver.staff_id
			LEFT JOIN departments AS DPT ON T.doc_transfer_sender_department_id = DPT.department_id
			LEFT JOIN units AS U ON T.doc_transfer_sender_unit_id = U.unit_id
			WHERE T.record_hide=:recordHide 
			AND doc_transfer_receiver_department_id = :docTransferReceiverDepartmentId
			ORDER BY doc_transfer_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":docTransferReceiverDepartmentId",$_SESSION['department_id']);
			
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					// loop through results and data
					foreach ($results as $result) {
						$returnRecords .= '<tr>
												<td>'.$result["added"].'</td>
												<td>'.$result["document_received_subject"].'</td>
												<td>'.$result["document_received_source"].'</td>
												<td>'.$result["document_received_type"].'</td>
												<td>'.$result["senderstaffname"].'</td>
												<td>'.$result["department_name"].'</td>
												<td>'.$result["unit_name"].'</td>
												<td>'.$result["doc_transfer_receiver_accepted_date"].'</td>
												<td>'.$result["receiverstaffname"].'</td>
											</tr>';
					}

				return $returnRecords;
			}
			else{
				return false;
				}

			}

		}

		// unit outgoing documents
		function get_all_department_outgoing_documents(){
			$returnRecords='';

			$sql="SELECT T.doc_transfer_id ,T.doc_transfer_receiver_category,T.doc_transfer_receiver_department_id,T.doc_transfer_sender_date,T.doc_received_id,T.doc_transfer_sender_staff_id,T.doc_transfer_receiver_accepted_date,T.doc_transfer_receiver_accepted_staff_id,T.doc_transfer_receiver_staff_ids,T.added,
			D.document_received_subject,D.document_received_source,D.document_received_type,D.document_received_from_company,
			CONCAT(Sender.staff_first_name,' ',Sender.staff_last_name) AS senderstaffname,
			CONCAT(Receiver.staff_first_name,' ',Receiver.staff_last_name) AS receiverstaffname

			FROM $this->table AS T
			LEFT JOIN documents_received AS D ON T.doc_received_id = D.document_received_id
			LEFT JOIN staffs AS Sender ON T.doc_transfer_sender_staff_id = Sender.staff_id
			LEFT JOIN staffs AS Receiver ON T.doc_transfer_receiver_accepted_staff_id = Receiver.staff_id
			WHERE T.record_hide=:recordHide 
			AND doc_transfer_sender_department_id = :docTransferOutgoingDepartmentId
			ORDER BY doc_transfer_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":docTransferOutgoingDepartmentId",$_SESSION['department_id']);
			
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					// loop through results and data
					foreach ($results as $result) {
						$returnRecords .= '<tr>
												<td>'.$result["added"].'</td>
												<td>'.$result["document_received_subject"].'</td>
												<td>'.$result["document_received_source"].'</td>
												<td>'.$result["document_received_type"].'</td>
												<td>'.$result["senderstaffname"].'</td>
												<td>'.$result["doc_transfer_receiver_category"].'</td>
												<td>'.$result["doc_transfer_receiver_accepted_date"].'</td>
												<td>'.$result["receiverstaffname"].'</td>
											</tr>';
					}

				return $returnRecords;
			}
			else{
				return false;
				}

			}

		}



























	}

?>