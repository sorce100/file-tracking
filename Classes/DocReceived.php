<?php 
	date_default_timezone_set('Africa/Accra');
	class DocReceived{
		// setting and getting variables
		private $id;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "documents_received";
		private $docReceivedFirstName;
		private $docReceivedLastName;
		private $docReceivedTelNum1;
		private $docReceivedTelNum2;
		private $docReceivedEmail;
		private $docReceivedCompany;
		private $docReceivedType;
		private $docReceivedDocCopies;
		private $docReceivedSubject;
		private $docReceivedNotes;
		private $scannedImage;
		private $folderName;
		private $docReceivedSource;

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_docReceivedFirstName($docReceivedFirstName) { $this->docReceivedFirstName = $docReceivedFirstName; }
		function set_docReceivedLastName($docReceivedLastName) { $this->docReceivedLastName = $docReceivedLastName; }
		function set_docReceivedTelNum1($docReceivedTelNum1) { $this->docReceivedTelNum1 = $docReceivedTelNum1; }
		function set_docReceivedTelNum2($docReceivedTelNum2) { $this->docReceivedTelNum2 = $docReceivedTelNum2; }
		function set_docReceivedEmail($docReceivedEmail) { $this->docReceivedEmail = $docReceivedEmail; }
		function set_docReceivedCompany($docReceivedCompany) { $this->docReceivedCompany = $docReceivedCompany; }
		function set_docReceivedType($docReceivedType) { $this->docReceivedType = $docReceivedType; }
		function set_docReceivedDocCopies($docReceivedDocCopies) { $this->docReceivedDocCopies = $docReceivedDocCopies; }
		function set_docReceivedSubject($docReceivedSubject) { $this->docReceivedSubject = $docReceivedSubject; }
		function set_docReceivedNotes($docReceivedNotes) { $this->docReceivedNotes = $docReceivedNotes; }
		function set_scannedImage($scannedImage) { $this->scannedImage = $scannedImage; }
		function set_folderName($folderName) { $this->folderName = $folderName; }
		function set_docReceivedSource($docReceivedSource) { $this->docReceivedSource = $docReceivedSource; }

		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (document_received_from_first_name,document_received_from_last_name,document_received_from_tel_num,document_received_from_tel_num_1,document_received_from_email,document_received_from_company,document_received_type,document_received_subject,document_received_copies_num,document_received_notes,document_received_folder_name,document_received_file_name,document_received_staff_id,document_received_by_unit,document_received_by_department, added, record_hide, last_updated, last_updated_staff_id, added_staff_id,document_received_source) 
			VALUES (:docReceivedFirstName,:docReceivedLastName,:docReceivedTelNum1,:docReceivedTelNum2,:docReceivedEmail,:docReceivedCompany,:docReceivedType,:docReceivedSubject,:docReceivedDocCopies,:docReceivedNotes,:folderName,:scannedImage,:docReceivedStaffId,:docReceivedUnitId,:docReceivedDepartmentId,:added,:recordHide,:lastUpdated,:lastUpdatedStaffId,:addedStaffId,:docReceivedSource)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":docReceivedFirstName",$this->docReceivedFirstName);
			$stmt->bindParam(":docReceivedLastName",$this->docReceivedLastName);
			$stmt->bindParam(":docReceivedTelNum1",$this->docReceivedTelNum1);
			$stmt->bindParam(":docReceivedTelNum2",$this->docReceivedTelNum2);
			$stmt->bindParam(":docReceivedEmail",$this->docReceivedEmail);
			$stmt->bindParam(":docReceivedCompany",$this->docReceivedCompany);
			$stmt->bindParam(":docReceivedType",$this->docReceivedType);
			$stmt->bindParam(":docReceivedSubject",$this->docReceivedSubject);
			$stmt->bindParam(":docReceivedDocCopies",$this->docReceivedDocCopies);
			$stmt->bindParam(":docReceivedNotes",$this->docReceivedNotes);
			$stmt->bindParam(":folderName",$this->folderName);
			$stmt->bindParam(":scannedImage",$this->scannedImage);
			$stmt->bindParam(":docReceivedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":docReceivedUnitId",$_SESSION['unit_id']);
			$stmt->bindParam(":docReceivedDepartmentId",$_SESSION['department_id']);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":lastUpdated",$date);
			$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":addedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":docReceivedSource",$this->docReceivedSource);

			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$date=date("jS F Y \/ h:i:s A");
			$sql="UPDATE $this->table SET staff_first_name=:staffFirstName,staff_last_name=:staffLastName,staff_tel_num=:staffTelNo,staff_email=:staffEmail,staff_employee_num=:staffEmployeeNum,staff_employee_type=:staffEmployeeType,staff_unit_id=:staffUnitId,staff_department_id=:staffDepartmentId,staff_notes=:staffNotes,last_updated=:lastUpdated,last_updated_staff_id=:lastUpdatedStaffId WHERE staff_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":staffFirstName",$this->staffFirstName);
				$stmt->bindParam(":staffLastName",$this->staffLastName);
				$stmt->bindParam(":staffTelNo",$this->staffTelNo);
				$stmt->bindParam(":staffEmail",$this->staffEmail);
				$stmt->bindParam(":staffEmployeeNum",$this->staffEmployeeNum);
				$stmt->bindParam(":staffEmployeeType",$this->staffEmployeeType);
				$stmt->bindParam(":staffDepartmentId",$this->staffDepartmentId);
				$stmt->bindParam(":staffUnitId",$this->staffUnitId);
				$stmt->bindParam(":staffNotes",$this->staffNotes);
				$stmt->bindParam(":lastUpdated",$date);
				$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
				$stmt->bindParam(":Id",$this->id);

				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE document_received_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get users
		function get_added_received_docs_list(){
			$returnRecords = '';
			$sql="SELECT D.document_received_id, CONCAT(D.document_received_from_first_name,' ',D.document_received_from_last_name) AS submitterfullname ,D.document_received_from_tel_num,D.document_received_from_company,D.document_received_type,D.document_received_subject,D.document_received_copies_num,D.added,D.document_received_source,C.company_name
			FROM $this->table AS D
			RIGHT JOIN companys AS C
			ON D.document_received_from_company = C.company_id
			WHERE D.record_hide=:recordHide AND D.document_received_staff_id = :receivedStaffId ORDER BY D.document_received_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":receivedStaffId",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				$details= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($details as $detail) {
                  $returnRecords .= '
				                      <tr>
				                        <td><input type="checkbox" name="'.trim($detail["document_received_id"]).'" class="docReceivedSelectedIds"></td>
				                        <td><b>'.trim($detail["document_received_source"]).'</b></td>
				                        <td>'.trim($detail["submitterfullname"]).'</td>
				                        <td>'.trim($detail["document_received_from_tel_num"]).'</td>
				                        <td>'.trim($detail["company_name"]).'</td>
				                        <td>'.trim($detail["document_received_type"]).'</td>
				                        <td>'.trim($detail["document_received_subject"]).'</td>
				                        <td>'.trim($detail["document_received_copies_num"]).'</td>
				                        <td>'.trim($detail["added"]).'</td>
				                        <td>
				                        <button class="btn-info doc_history_btn" id="'.$detail["document_received_id"].'"><i class="fa fa-hourglass"></i> HISTORY</button> 
				                        <button class="btn-primary update_docReceived" id="'.$detail["document_received_id"].'"><i class="fa fa-pencil"></i> UPDATE</button>
				                        <button class="btn-danger del_docReceived" id="'.$detail["document_received_id"].'"><i class="fa fa-trash"></i> DELETE</button></td>
				                      </tr>';
                  }
				return $returnRecords;
			}
			else{
				return false;
				}

		}

	// get document by id
		function get_received_docs_by_id(){
			$sql="SELECT * FROM $this->table WHERE document_received_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				return false;
				}
		}

	// get records of selected received documents
		function get_selected_documents($returnArrays){
			$returnArrays = json_decode($returnArrays);
			$returnRecords = [];

			foreach ($returnArrays as $returnArray) {
				$sql="SELECT D.document_received_id,CONCAT(D.document_received_from_first_name,' ',D.document_received_from_last_name) AS docreceivedfromname ,D.document_received_type,D.document_received_copies_num,D.document_received_subject,D.document_received_file_name,D.added,D.document_received_source,
				C.company_name
				FROM $this->table AS D
				RIGHT JOIN companys AS C
				ON D.document_received_from_company = C.company_id
				WHERE document_received_id=:Id LIMIT 1";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$returnArray);
				if ($stmt->execute()) {
					$result= $stmt->fetch(PDO::FETCH_ASSOC);
					// add to array 
					$returnRecords[] = $result;
				}
				else{
					return false;
					}
			}
			return $returnRecords;
			
		}


	// get documents details for documents history
		function get_document_detail_and_history_by_doc_id(){
			$returnRecords = [];
			$sql="SELECT D.document_received_id,D.document_received_source,CONCAT(D.document_received_from_first_name,' ',D.document_received_from_last_name) AS submitterfullname,D.document_received_from_tel_num,D.document_received_from_email,D.document_received_type,D.document_received_from_company,D.document_received_subject,D.document_received_copies_num,D.document_received_folder_name,D.document_received_file_name,D.document_received_staff_id,D.document_received_by_unit,D.document_received_by_department,D.added,D.last_updated,
				C.company_name,
				CONCAT(S.staff_first_name,' ',S.staff_last_name) AS stafffullname,
				U.unit_name,
				Dpt.department_name 
				FROM $this->table AS D
				LEFT JOIN companys AS C
				ON D.document_received_from_company = C.company_id
				LEFT JOIN staffs AS S
				ON D.document_received_staff_id = S.staff_id
				LEFT JOIN units AS U
				ON D.document_received_by_unit = U.unit_id
				LEFT JOIN departments AS Dpt
				ON D.document_received_by_department = Dpt.department_id

				WHERE D.document_received_id=:Id 
				AND D.record_hide = :recordHide LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				// add all results
				$returnRecords['allResults'] = $results;
				// add documents transfer history
				$histroyResults = $this->get_document_transfer_history();
				if ($histroyResults) {
					$returnRecords['transferHistory'] =$histroyResults;
				}
				// get document minutes and add
				$returnRecords['documentMinutes'] = $this->get_all_document_minutes();


				return json_encode($returnRecords);
			}
			else{
				return false;
				}
		}


	// get time ago function

		function timeago($date) {
		   $timestamp = strtotime($date);	
		   
		   $strTime = array("second", "minute", "hour", "day", "month", "year");
		   $length = array("60","60","24","30","12","10");

		   $currentTime = time();
		   if($currentTime >= $timestamp) {
				$diff     = time()- $timestamp;
				for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
				$diff = $diff / $length[$i];
				}

				$diff = round($diff);
				return $diff . " " . $strTime[$i] . "(s) ago ";
		   }
		}



// get all document tranfer history
		function get_document_transfer_history(){
			$returnRecords = '';
			$sql="SELECT DR.doc_transfer_sender_date,DR.doc_transfer_receiver_category,DR.doc_transfer_receiver_accepted_date,DR.doc_transfer_receiver_department_id,DR.doc_transfer_receiver_accepted_staff_id,
			CONCAT(S1.staff_first_name,' ',S1.staff_last_name) AS senderstaffname,
			CONCAT(S2.staff_first_name,' ',S2.staff_last_name) AS receiverstaffname,
			U1.unit_name AS senderunit,
			U2.unit_name AS receiverunit,
			DptSender.department_name AS senderdepartment,
			DptReceiver.department_name AS receiverdepartment

			FROM documents_transfer AS DR
			LEFT JOIN staffs AS S1 ON DR.doc_transfer_sender_staff_id = S1.staff_id
			LEFT JOIN staffs AS S2 ON DR.doc_transfer_receiver_accepted_staff_id = S2.staff_id
			LEFT JOIN units AS U1 ON DR.doc_transfer_sender_unit_id = U1.unit_id
			LEFT JOIN units AS U2 ON DR.doc_transfer_receiver_unit_id = U2.unit_id
			LEFT JOIN departments AS DptSender ON DR.doc_transfer_sender_department_id = DptSender.department_id
			LEFT JOIN departments AS DptReceiver ON DR.doc_transfer_receiver_department_id = DptReceiver.department_id

			WHERE DR.doc_received_id =:Id ";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$returnRecords .= '<tr>
										<td>'.$result["senderstaffname"].'</td>
										<td>'.$result["senderunit"].'</td>
										<td>'.$result["senderdepartment"].'</td>
										<td>'.$result["doc_transfer_sender_date"].'</td>
										<td>'.$result["doc_transfer_receiver_category"].'</td>';

										switch (trim($result["doc_transfer_receiver_category"])) {
										case 'Department':
											$returnRecords .='<td>'.$result["receiverdepartment"].'</td>';
											break;
										case 'Unit':
											$returnRecords .='<td>'.$result["receiverunit"].'</td>';
											break;
										case 'Staff':
											# Staff...
											break;
										default:
											# code...
											break;
										}
										
					$returnRecords .= '		<td>'.$result["receiverstaffname"].'</td>
											<td>'.$result["doc_transfer_receiver_accepted_date"].'</td>
										</tr>';
				}
				
				return $returnRecords;
			}
			else{
				return false;
				}
		}



// get all documnet minutes
		function get_all_document_minutes(){
			$returnRecords='';
			$sql="SELECT M.doc_minute_id,M.doc_minute_note,M.added,M.added_staff_id,M.doc_minute_lock,
			CONCAT(S.staff_first_name,' ',S.staff_last_name) as staffname 
			FROM documents_minutes AS M
			LEFT JOIN staffs AS S
			ON M.added_staff_id=S.staff_id
			WHERE M.record_hide=:recordHide 
			AND M.document_received_id=:documentReceivedId 
			ORDER BY M.doc_minute_id DESC";

			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":documentReceivedId",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$returnRecords .='<li>
					                    <div class="block">
					                        <div class="tags"><a href="#" class="tag"><span>'.$result["added"].'</span></a></div>
					                        <div class="block_content">';
					                        switch (trim($result['doc_minute_lock'])) {
												case 'OPEN':
												$returnRecords .= '<h2 class="title"><a>'.$result["doc_minute_note"].'</a></h2>';
												break;
												case 'LOCK':
												$returnRecords .= '<h2 class="title "><a class="bg-red">CONTENT IS LOCKED</a></h2>';
												break;
											}
					                        
					$returnRecords .=       '<div class="byline">
					                          <span>Uplaoded </span> By <a>'.$result["staffname"].'</a>
					                        </div>
					                      </div>
					                    </div>
					                  </li>';
				}
				return $returnRecords;
			}
			else{
				return false;
				}

		}

}

?>