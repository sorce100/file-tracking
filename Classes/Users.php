<?php 
	date_default_timezone_set('Africa/Accra');
	class Users{
		// setting and getting variables
		private $id;
		private $accountSelect;
		private $userName;
		private $userPassword;
		private $accPasswdReset;
		private $accGroup;
		private $accStatus;
		private $loginStatus = "NEVER";
		private $dbConn;
		private $recordHide = "NO";
		private $table = "users";
		private $accountType = "staff";

		function set_id($id) { $this->id = $id; }
		function set_accountSelect($accountSelect) { $this->accountSelect = $accountSelect; }
		function set_userName($userName) { $this->userName = $userName; }
		function set_userPassword($userPassword) { $this->userPassword = $userPassword; }
		function set_accPasswdReset($accPasswdReset) { $this->accPasswdReset = $accPasswdReset; }
		function set_accGroup($accGroup) { $this->accGroup = $accGroup; }
		function set_accStatus($accStatus) { $this->accStatus = $accStatus; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_accountType($accountType) { $this->accountType = $accountType; }

		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// for login
		function login(){
			$sql="SELECT * FROM $this->table WHERE user_name = :userName AND account_status = :accStatus AND record_hide=:recordHide";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":userName",$this->userName);
			$stmt->bindParam(":accStatus",$this->accStatus);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
				}

		}

		// insert pages
		function insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (staff_id,user_name,user_password,password_reset,group_id,account_status,account_login_status,added,record_hide,added_staff_id,last_updated,last_updated_staff_id,account_type) 
			VALUES (:staffId,:userName,:userPassword,:passwordReset,:groupId,:accStatus,:loginStatus,:added,:recordHide,:addedStaffId,:lastUpdated,:lastUpdatedStaffId,:accountType)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":staffId",$this->accountSelect);
			$stmt->bindParam(":userName",$this->userName);
			$stmt->bindParam(":userPassword",$this->userPassword);
			$stmt->bindParam(":passwordReset",$this->accPasswdReset);
			$stmt->bindParam(":groupId",$this->accGroup);
			$stmt->bindParam(":accStatus",$this->accStatus);
			$stmt->bindParam(":loginStatus",$this->loginStatus);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":addedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":lastUpdated",$date);
			$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":accountType",$this->accountType);

			if ($stmt->execute()) {
				// if successful, insert the user id into the member table
				$returnUserId =  trim($this->dbConn->lastInsertId());
				$sqlupdate = "UPDATE staffs SET staff_account_user_id=:returnUserId WHERE staff_id=:staffId";
				$stmt = $this->dbConn->prepare($sqlupdate);
				$stmt->bindParam(":returnUserId",$returnUserId);
				$stmt->bindParam(":staffId",$this->accountSelect);
				$stmt->execute();
				// return 
				return true;
			}
			else{
				return false;
				}
		}


		// admin insert user
		function insert_admin_account(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (member_id,user_name,user_password,password_reset,group_id,account_status,account_login_status,added,record_hide,user_id,account_type,account_profile) 
			VALUES (:memberId,:userName,:userPassword,:passwordReset,:groupId,:accStatus,:loginStatus,:added,:recordHide,:userId,:accountType,:accountProfile)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":memberId",$this->accountSelect);
			$stmt->bindParam(":userName",$this->userName);
			$stmt->bindParam(":userPassword",$this->userPassword);
			$stmt->bindParam(":passwordReset",$this->accPasswdReset);
			$stmt->bindParam(":groupId",$this->accGroup);
			$stmt->bindParam(":accStatus",$this->accStatus);
			$stmt->bindParam(":loginStatus",$this->loginStatus);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":accountType",$this->accountType);
			$stmt->bindParam(":accountProfile",$this->accountProfile);

			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}

		// for update
		function update(){
			$sql="UPDATE $this->table SET user_name=:userName,user_password=:userPassword,password_reset=:accPasswdReset,group_id=:accGroup,account_status=:accStatus,last_updated=:lastUpdated,last_updated_staff_id=:lastUpdatedStaffId WHERE user_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":userName",$this->userName);
				$stmt->bindParam(":userPassword",$this->userPassword);
				$stmt->bindParam(":accPasswdReset",$this->accPasswdReset);
				$stmt->bindParam(":accGroup",$this->accGroup);
				$stmt->bindParam(":accStatus",$this->accStatus);
				$stmt->bindParam(":lastUpdated",$date);
				$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE user_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		
		}

		// get all staff list
		function get_all_staff_list(){
			$returnRecords = '';
			$sql="SELECT  USR.user_id,USR.staff_id,USR.user_name,USR.user_password,USR.password_reset,USR.group_id,USR.account_status,USR.account_login_status,USR.added,USR.added_staff_id,USR.last_updated,
			CONCAT(S.staff_first_name,' ',S.staff_last_name) AS staffname,
			G.pages_group_name 
			FROM $this->table AS USR
			LEFT JOIN staffs AS S ON USR.staff_id = S.staff_id
			LEFT JOIN pages_group AS G ON USR.group_id = G.pages_group_id
			WHERE USR.record_hide=:recordHide 
			AND USR.account_type=:accountType 
			ORDER BY user_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":accountType",$this->accountType);

			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$returnRecords .= '<tr>
											<td>'.$result["staffname"].'</td>
											<td>'.$result["user_name"].'</td>
											<td>'.$result["pages_group_name"].'</td>
											<td>'.$result["password_reset"].'</td>
											<td>'.$result["account_status"].'</td>
											<td>'.$result["account_login_status"].'</td>
											<td>'.$result["added"].'</td>
											<td>'.$result["last_updated"].'</td>
											<td>
			                                  <button class="btn-primary update_user" id="'.$result["user_id"].'"><i class="fa fa-pencil"></i> UPDATE</button> 
			                                  <button class="btn-danger del_user" id="'.$result["user_id"].'"><i class="fa fa-trash"></i> DELETE</button>
			                                </td>
										</tr>';
				}
				return $returnRecords;
			}
			else{
				return false;
				}

		}

		// get administrator accounts
		function get_administator_users(){
			$this->accountType = "administator";
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide AND account_type=:accountType ORDER BY user_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":accountType",$this->accountType);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
				}

		}

		// get user by id
		function get_user_by_id(){
			$sql="SELECT * FROM $this->table WHERE user_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				return false;
				}
		}

	/// get password of the user
		function get_password(){
			$sql="SELECT user_password FROM $this->table WHERE user_id=:user_id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":user_id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return trim($results['user_password']);
			}
			else{
				return false;
				}
		}
	// change password
		function change_password(){
			$sql="UPDATE $this->table SET user_password = :userPassword,password_reset = :passwordReset WHERE user_name=:userName AND staff_id=:staffId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":userPassword",$this->userPassword);
			$stmt->bindParam(":passwordReset",$this->accPasswdReset);
			$stmt->bindParam(":userName",$this->userName);
			$stmt->bindParam(":staffId",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
//////////////////////////////////////////////////////////////////////////
//FOR UPDATING SESSION DETAILS
/////////////////////////////////////////////////////////////////////////

			function session_status_update($loginStatus){
				$sql="UPDATE $this->table SET account_login_status=:loginStatus WHERE user_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":loginStatus",$loginStatus);
				$stmt->bindParam(":Id",$_SESSION['user_id']);
				if ($stmt->execute()) {
					// if online successful then grab the 
					return true;
				}
				else{
					return false;
					}
				}

			function get_userName($userId){
				$sql="SELECT user_name FROM $this->table WHERE user_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$userId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return $results["user_name"];
				}
				else{
					return false;
					}
			}


			// get the department and unit ids and set to session logs
			function set_staff_details_into_session($staffId){
				$sql="SELECT CONCAT(staff_first_name,' ',staff_last_name) AS staffname, staff_unit_id,staff_department_id FROM staffs WHERE staff_id=:Id LIMIT 1";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$staffId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					$_SESSION['unit_id'] = trim($results["staff_unit_id"]);
					$_SESSION['department_id'] = trim($results["staff_department_id"]);
					$_SESSION['fullname'] = strtoupper(trim($results["staffname"]));
					return true;
				}
				else{
					return false;
					}
			}


	}

?>