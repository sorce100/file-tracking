<?php 
	class Counts{
		private $recordHide = "NO";

		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}


		// members count
		function members_count(){
			$sql="SELECT * FROM members WHERE record_hide=:recordHide";
			try{
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":recordHide",$this->recordHide);
				if ($stmt->execute()) {
					return $stmt->rowCount();
				}
				else{
					return false;
				}
				
			}catch(PDOException $e){
				echo '{"error":{"text": '.$e->getMessage().'}';
			}
		}

		// members count
		function member_agent_count(){
			$sql="SELECT * FROM agents WHERE record_hide=:recordHide AND agent_member_attach=:memberLicenseNum";
			try{
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":recordHide",$this->recordHide);
				$stmt->bindParam(":memberLicenseNum",$_SESSION['member_id']);
				if ($stmt->execute()) {
					return $stmt->rowCount();
				}
				else{
					return false;
				}
				
			}catch(PDOException $e){
				echo '{"error":{"text": '.$e->getMessage().'}';
			}
		}


	}


 ?>