<?php 
	date_default_timezone_set('Africa/Accra');
	class SessionLogs{
		// setting and getting variables
		private $id;
		private $table = "users_session_log";
		function set_id($id) { $this->id = $id; }
		function get_id() { return $this->id; }

		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}

			function session_log_start(){
				$date = date("jS F Y \/ h:i:s A");
				$sql = "INSERT INTO $this->table (user_id,session_start) VALUES (:userId,:sessionStart)";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":userId",$_SESSION['user_id']);
				$stmt->bindParam(":sessionStart",$date);
				if ($stmt->execute()) {
					$_SESSION['session_log_id'] = trim($this->dbConn->lastInsertId());
					return true;
				}
				else{
					die();
					}
			}

		// when the user logs out
			function session_log_end(){
				$date = $date=date("jS F Y \/ h:i:s A");
				$sql = "UPDATE $this->table SET  session_end=:sessionEnd WHERE users_session_log_id=:usersSessionLogId";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":sessionEnd",$date);
				$stmt->bindParam(":usersSessionLogId",$_SESSION['session_log_id']);
				if ($stmt->execute()) {
					return true;
				}
				else{
					die();
					}
			}

		// get all logs
			function get_session(){
				$sql = "SELECT s.user_id,s.session_start,s.session_end,u.user_name 
				FROM $this->table AS s
				RIGHT JOIN users AS u
				ON s.user_id = u.user_id
				ORDER BY s.users_session_log_id DESC";
				$stmt = $this->dbConn->prepare($sql);
				if ($stmt->execute()) {
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
					return $results;
				}
				else{
					die();
					}
			}

		// get session log for user
			function get_user_session(){
				$sql = "SELECT * FROM $this->table where user_id=:userId ORDER BY users_session_log_id DESC";
				$stmt = $this->dbConn->prepare($sql);
				if ($stmt->execute()) {
					$stmt->bindParam(":userId",$_SESSION['user_id']);
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
					return $results;
				}
				else{
					die();
					}
			}
		// get username of log
			function get_session_username($data){
				$sql = "SELECT user_name FROM users WHERE users_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$data);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return $results["user_name"];
				}
				else{
					die();
					}
			}
	}

 ?>