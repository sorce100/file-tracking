<?php 
	date_default_timezone_set('Africa/Accra');
	class Units{
		// setting and getting variables
		private $id;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "units";
		private $unitName;
		private $unitAlias;
		private $unitDepartment;
		private $unitHead;
		private $unitSecretary;
		private $unitNotes;

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_unitName($unitName) { $this->unitName = $unitName; }
		function set_unitAlias($unitAlias) { $this->unitAlias = $unitAlias; }
		function set_unitDepartment($unitDepartment) { $this->unitDepartment = $unitDepartment; }
		function set_unitHead($unitHead) { $this->unitHead = $unitHead; }
		function set_unitSecretary($unitSecretary) { $this->unitSecretary = $unitSecretary; }
		function set_unitNotes($unitNotes) { $this->unitNotes = $unitNotes; }


		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (unit_name,unit_alias,unit_department,unit_head,unit_secretaries,unit_notes,added,record_hide,last_updated,last_updated_staff_id,added_staff_id) 
			VALUES (:unitName,:unitAlias,:unitDepartment,:unitHead,:unitSecretary,:unitNotes,:added,:recordHide,:lastUpdated,:lastUpdatedStaffId,:addedStaffId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":unitName",$this->unitName);
			$stmt->bindParam(":unitAlias",$this->unitAlias);
			$stmt->bindParam(":unitDepartment",$this->unitDepartment);
			$stmt->bindParam(":unitHead",$this->unitHead);
			$stmt->bindParam(":unitSecretary",$this->unitSecretary);
			$stmt->bindParam(":unitNotes",$this->unitNotes);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":lastUpdated",$date);
			$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":addedStaffId",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$date=date("jS F Y \/ h:i:s A");
			$sql="UPDATE $this->table SET unit_name=:unitName,unit_alias=:unitAlias,unit_department=:unitDepartment,unit_head=:unitHead,unit_secretaries=:unitSecretary,unit_notes=:unitNotes,last_updated=:lastUpdated,last_updated_staff_id=:lastUpdatedStaffId WHERE unit_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":unitName",$this->unitName);
				$stmt->bindParam(":unitAlias",$this->unitAlias);
				$stmt->bindParam(":unitDepartment",$this->unitDepartment);
				$stmt->bindParam(":unitHead",$this->unitHead);
				$stmt->bindParam(":unitSecretary",$this->unitSecretary);
				$stmt->bindParam(":unitNotes",$this->unitNotes);
				$stmt->bindParam(":lastUpdated",$date);
				$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$date=date("jS F Y \/ h:i:s A");
			$sql="UPDATE $this->table SET record_hide=:recordHide,last_updated=:lastUpdated WHERE unit_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":lastUpdated",$date);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get units
		function get_units(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY unit_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
				}

		}

	// get all units list
		function get_units_list(){
			$returnRecords='';
			$sql="SELECT U.unit_name,U.unit_alias,U.unit_department,U.added,U.unit_id, 
			D.department_name,
			CONCAT(SH.staff_first_name,' ',SH.staff_last_name) AS headname,
			CONCAT(SC.staff_first_name,' ',SC.staff_last_name) AS secretaryname
			FROM $this->table AS U
			LEFT JOIN departments AS D ON U.unit_department = D.department_id
			LEFT JOIN staffs AS SH ON U.unit_head = SH.staff_id
			LEFT JOIN staffs AS SC ON U.unit_secretaries = SC.staff_id
			WHERE U.record_hide=:recordHide 
			ORDER BY U.unit_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$units= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($units as $unit) {
	                  $returnRecords .= '
					                      <tr>
					                        <td>'.trim($unit["unit_name"]).'</td>
					                        <td>'.trim($unit["unit_alias"]).'</td>
					                        <td>'.trim($unit["department_name"]).'</td>
					                        <td>'.trim($unit["headname"]).'</td>
					                        <td>'.trim($unit["secretaryname"]).'</td>
					                        <td>'.trim($unit["added"]).'</td>
					                        <td>
					                          <button class="btn-primary update_unit" id="'.$unit["unit_id"].'"><i class="fa fa-pencil"></i> UPDATE</button> 
					                          <button class="btn-danger del_unit" id="'.$unit["unit_id"].'"><i class="fa fa-trash"></i> DELETE</button>
					                        </td>
					                      </tr>';
	              }
				return $returnRecords;
			}
			else{
				return false;
				}

		}

	// get user
		function get_unit_by_id(){
			$sql="SELECT * FROM $this->table WHERE unit_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				return false;
				}
		}

	// get all units for departement
		function get_all_department_units(){
			$sql="SELECT unit_id,unit_name FROM $this->table WHERE unit_department=:unitDepartment ORDER BY unit_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":unitDepartment",$this->unitDepartment);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
				}

		}



	}

?>