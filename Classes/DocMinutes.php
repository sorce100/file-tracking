<?php 
	date_default_timezone_set('Africa/Accra');
	class DocMinutes{
		// setting and getting variables
		private $id;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "documents_minutes";
		private $addMinuteLock;
		private $addMinuteDetail;
		private $documentReceivedId;


		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_addMinuteLock($addMinuteLock) { $this->addMinuteLock = $addMinuteLock; }
		function set_addMinuteDetail($addMinuteDetail) { $this->addMinuteDetail = $addMinuteDetail; }
		function set_documentReceivedId($documentReceivedId) { $this->documentReceivedId = $documentReceivedId; }

		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (document_received_id,doc_minute_note, doc_minute_lock, doc_minute_unit_id, doc_minute_department_d, added, record_hide, last_updated, last_updated_staff_id, added_staff_id) 
			VALUES (:documentReceivedId,:addMinuteDetail,:addMinuteLock,:unitId,:departmentId,:added,:recordHide,:lastUpdated,:lastUpdatedStaffId,:addedStaffId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":documentReceivedId",$this->documentReceivedId);
			$stmt->bindParam(":addMinuteDetail",$this->addMinuteDetail);
			$stmt->bindParam(":addMinuteLock",$this->addMinuteLock);
			$stmt->bindParam(":unitId",$_SESSION['unit_id']);
			$stmt->bindParam(":departmentId",$_SESSION['department_id']);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":lastUpdated",$date);
			$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":addedStaffId",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$date=date("jS F Y \/ h:i:s A");
			$sql="UPDATE $this->table SET document_received_id=:documentReceivedId,doc_minute_note=:addMinuteDetail,doc_minute_lock=:addMinuteLock,last_updated=:lastUpdated,last_updated_staff_id=:lastUpdatedStaffId WHERE doc_minute_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":documentReceivedId",$this->documentReceivedId);
				$stmt->bindParam(":addMinuteDetail",$this->addMinuteDetail);
				$stmt->bindParam(":addMinuteLock",$this->addMinuteLock);
				$stmt->bindParam(":lastUpdated",$date);
				$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE doc_minute_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get all minutes
		function get_document_minutes(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide AND document_received_id=:documentReceivedId ORDER BY doc_minute_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":documentReceivedId",$this->documentReceivedId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
				}

		}
		// get minuts list
		function get_document_minutes_list(){
			$returnRecords='';
			$sql="SELECT M.doc_minute_id,M.doc_minute_note,M.added,M.added_staff_id,M.doc_minute_lock,
			CONCAT(S.staff_first_name,' ',S.staff_last_name) as staffname 
			FROM $this->table AS M
			LEFT JOIN staffs AS S
			ON M.added_staff_id=S.staff_id
			WHERE M.record_hide=:recordHide 
			AND M.document_received_id=:documentReceivedId 
			ORDER BY M.doc_minute_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":documentReceivedId",$this->documentReceivedId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$returnRecords .= '<div style="background-color:#f4f4f4;" class="well">';
					// check to see if its an open minute or closed on to lock the content
					switch (trim($result['doc_minute_lock'])) {
						case 'OPEN':
						$returnRecords .= '<h4><b>'.$result["doc_minute_note"].'</b></h4>';
						break;
						case 'LOCK':
						$returnRecords .= '<h4><b class="bg-red"> CONTENT IS LOCKED </b></h4>';
						break;
					}
	 									
	 				$returnRecords .= '<p><b>Added on :</b>'.$result["added"].'<b> BY </b>'.$result["staffname"].'</p>';

 					if ($_SESSION['staff_id'] == $result["added_staff_id"]) {
 						$returnRecords .= '<button class="btn-info updateMinute" id="'.$result["doc_minute_id"].'"><i class="fa fa-pencil"></i></button>
 											<button class="btn-danger deleteMinute" id="'.$result["doc_minute_id"].'"><i class="fa fa-trash"></i></button>';
 					}

 					$returnRecords .= '</div>';
				}

				return $returnRecords;
			}
			else{
				return false;
				}

		}

	// get minute by id
		function get_doc_minute_by_id(){
			$sql="SELECT * FROM $this->table WHERE doc_minute_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				return false;
				}
		}



	}

?>