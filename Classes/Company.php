<?php 
	date_default_timezone_set('Africa/Accra');
	class Company{
		// setting and getting variables
		private $id;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "companys";
		private $companyName;
		private $companyTelNo;
		private $companyEmail;
		private $companyAddress;
		private $companyRegion;
		private $companyCountry;
		private $companyNotes;

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_companyName($companyName) { $this->companyName = $companyName; }
		function set_companyTelNo($companyTelNo) { $this->companyTelNo = $companyTelNo; }
		function set_companyEmail($companyEmail) { $this->companyEmail = $companyEmail; }
		function set_companyAddress($companyAddress) { $this->companyAddress = $companyAddress; }
		function set_companyRegion($companyRegion) { $this->companyRegion = $companyRegion; }
		function set_companyCountry($companyCountry) { $this->companyCountry = $companyCountry; }
		function set_companyNotes($companyNotes) { $this->companyNotes = $companyNotes; }

		public function __construct(){
			require_once("db/db.php");
			$db = new db();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (company_name,company_tel_num,company_email,company_address,company_region,company_country,company_notes,added,record_hide,last_updated,last_updated_staff_id,added_staff_id) 
			VALUES (:companyName,:companyTelNo,:companyEmail,:companyAddress,:companyRegion,:companyCountry,:companyNotes,:added,:recordHide,:lastUpdated,:lastUpdatedStaffId,:addedStaffId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":companyName",$this->companyName);
			$stmt->bindParam(":companyTelNo",$this->companyTelNo);
			$stmt->bindParam(":companyEmail",$this->companyEmail);
			$stmt->bindParam(":companyAddress",$this->companyAddress);
			$stmt->bindParam(":companyRegion",$this->companyRegion);
			$stmt->bindParam(":companyCountry",$this->companyCountry);
			$stmt->bindParam(":companyNotes",$this->companyNotes);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":lastUpdated",$date);
			$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
			$stmt->bindParam(":addedStaffId",$_SESSION['staff_id']);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$date=date("jS F Y \/ h:i:s A");
			$sql="UPDATE $this->table SET company_name=:companyName,company_tel_num=:companyTelNo,company_email=:companyEmail,company_address=:companyAddress,company_region=:companyRegion,company_country=:companyCountry,company_notes=:companyNotes,last_updated=:lastUpdated,last_updated_staff_id=:lastUpdatedStaffId WHERE company_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":companyName",$this->companyName);
				$stmt->bindParam(":companyTelNo",$this->companyTelNo);
				$stmt->bindParam(":companyEmail",$this->companyEmail);
				$stmt->bindParam(":companyAddress",$this->companyAddress);
				$stmt->bindParam(":companyRegion",$this->companyRegion);
				$stmt->bindParam(":companyCountry",$this->companyCountry);
				$stmt->bindParam(":companyNotes",$this->companyNotes);
				$stmt->bindParam(":lastUpdated",$date);
				$stmt->bindParam(":lastUpdatedStaffId",$_SESSION['staff_id']);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$date=date("jS F Y \/ h:i:s A");
			$sql="UPDATE $this->table SET record_hide=:recordHide,last_updated=:lastUpdated WHERE company_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":lastUpdated",$date);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get companys
		function get_companys(){
			$returnRecords='';
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY company_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$details= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($details as $detail) {
                  
                  	$returnRecords.= '
				                      <tr>
				                        <td>'.trim($detail["company_name"]).'</td>
				                        <td>'.trim($detail["company_tel_num"]).'</td>
				                        <td>'.trim($detail["company_region"]).'</td>
				                        <td>'.trim($detail["company_country"]).'</td>
				                        <td>0</td>
				                        <td>'.trim($detail["added"]).'</td>
				                        <td>
					                        <button class="btn-info view_company" id="'.$detail["company_id"].'"><i class="fa fa-eye"></i> VIEW</button>';
					                    if ($detail['added_staff_id'] == $_SESSION['staff_id']) {     
						                    $returnRecords.= ' <button class="btn-primary update_company" id="'.$detail["company_id"].'"><i class="fa fa-pencil"></i> UPDATE</button> 
						                        <button class="btn-danger del_company" id="'.$detail["company_id"].'"><i class="fa fa-trash"></i> DELETE</button>';
						                    }

				    $returnRecords.=	' </td>
				                      </tr>';
	                
                  }
				return $returnRecords;
			}
			else{
				return false;
				}

		}

	// get companys list
		function get_companys_list(){
			$returnRecords = '';
			$sql="SELECT company_id,company_name FROM $this->table WHERE record_hide=:recordHide ORDER BY company_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
	              $returnRecords .= '<option value="'.trim($result["company_id"]).'">'.$result["company_name"].'</option>';
	            }
	            return $returnRecords;
			}
			else{
				return false;
				}

		}

	// get user
		function get_company_by_id(){
			$sql="SELECT * FROM $this->table WHERE company_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				return false;
				}
		}



	}

?>